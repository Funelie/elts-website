# Website

This repository manages the Extended LTS website:

https://deb.freexian.com/extended-lts/

# Where to file issues

Please file issues related to this website in the usual ELTS repository:

https://salsa.debian.org/groups/freexian-team/extended-lts/-/issues
