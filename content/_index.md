---
title: "Debian Extended LTS by Freexian"
date: 2018-05-30T11:07:07+02:00
description: "This service extends security support for old Debian releases up to 10 years, albeit only on the subset of packages used by the customers of this service. Debian 8 and Debian 9 are currently maintained."
---

## Introduction

This service is an extension of the regular [Debian LTS
support](https://www.freexian.com/services/debian-lts.html) already managed by
Debian contributors funded by Freexian with the money collected among many
sponsors.

To make Extended LTS possible and sustainable, there are supplementary
restrictions compared to regular LTS:

* we support only the packages used by the customers
* we provide updates for a limited set of architectures (at least amd64,
  others can be added on request)
* we support the Linux kernel only with a backport of a newer kernel
  version

Customers are invoiced semi-annually for the amount needed to support their
packages. 

## Releases supported

Each Debian release has some peculiarities in terms of support, open
the associated page to discover them:

* [Debian 8 Jessie](docs/debian-8-support/) can be supported until June 2025
* [Debian 9 Stretch](docs/debian-9-support/) can be supported until June 2027

## Request a quote

Organizations interested in getting security support for Debian 8 and/or
Debian 9 should [get in touch with us at
sales@freexian.com](mailto:sales@freexian.com) with the
[subscription form](debian-elts-subscription-form.pdf) filled (even if not
yet signed) and with the list of source packages that they want to see
supported in each of those Debian releases (see [instructions
here](docs/how-to-build-a-package-list/) for how to build those package
lists).

We will get back to you with a quote detailing the cost of support
for the upcoming semester and each of the following ones too. You will
also have a file highlighting the packages that we can't support and/or
that require special attention. More information about this on our
[ELTS cost estimation model](docs/cost-estimation/) page.
