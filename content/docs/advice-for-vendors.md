+++
title = "Advice for vendors of Debian appliances"
date = 2021-09-16T15:23:10-04:00
weight = 200
draft = true
bref = "Advice for getting the right long term support for your system"
toc = false
+++

## Selecting supported software 

## Getting security support for the Linux kernel.
