+++
title = "The ELTS cost estimation model"
date = 2022-04-20T11:58:57+02:00
weight = 121
draft = false
bref = "Understand the logic behind our cost estimations"
toc = false
+++

### Explanations about the ELTS cost estimation 

We have been doing security support of Debian packages since 2014 so we
have a long history of data that lets us estimate the workload (and
thus cost) required to keep each package secure.

We also have historical data of our customers so we know which packages
are the most popular among our customers. We use this data to define the
"per customer cost" of each package based on their expected popularity.
At the same time, we also know that the number of ELTS customers is
decreasing over time because some customers have finished migrating their
servers or reached the end-of-life of their own product.

We have combined all those data to build a cost estimation model so that
when a prospect submits us a package list to support, we can quickly
provide a cost estimation for the whole support period that they need.

#### What is included in the cost estimation

The cost estimation combines two parts:

* a fixed amount per semester that covers the cost of our infrastructure
  and of the maintenance of all the "base packages".
* a variable amount per semester that covers the cost of maintaining all
  the packages used by the customer

The part of the price that covers the maintenance of the packages will
grow each semester. During the first 2.5 years, the price increase will be
moderate (around 10% each time) but it will be more important in the last
2.5 years.

#### Examples of a cost estimation

The below examples have been made for Debian 9 Stretch. The figures and
package lists may vary for other Debian releases.

##### A small container with a web service

In this example, we ran debootstrap and then installed Apache 2, PostgreSQL
and PHP. This resulted in this [package list](sample1.txt).

The associated cost estimation looks like this:

> Your list of packages to support in Debian stretch contains 163 packages.
> 
> Among those there are 0 packages that Freexian will not support (see the
> warnings.csv file for details). Among the 163 packages that we can
> support, there are 80 that have an history of security vulnerabilities.
> 
> The price for each semester consists of a fixed sum (covering the cost of
> the base system and infrastructure costs) and of a variable amount
> depending on the cost of your packages (you can have some details about
> the price of each package in the initial period in the packages-cost.csv
> file) and of the time elapsed.
> 
> The price increase tries to model the loss of customers over time, it's a
> slow increase over the first half of the ELTS period and a steep increase
> afterwards because the bulk of the customers will have migrated to another
> Debian release at that time.
> 
> * 2022-H2: 3885 EUR
> * 2023-H1: 4305 EUR
> * 2023-H2: 4725 EUR
> * 2024-H1: 5145 EUR
> * 2024-H2: 5670 EUR
> * 2025-H1: 7455 EUR
> * 2025-H2: 9345 EUR
> * 2026-H1: 11130 EUR
> * 2026-H2: 12915 EUR
> * 2027-H1: 14700 EUR

##### An embedded product

In this example, we used debian installer in a virtual machine: we
installed the "standard" task, then manually installed curl, busybox, ntp
and openjdk-8-jre. This resulted in this [package list](sample2.txt).

The associated cost estimation looks like this:

> Your list of packages to support in Debian stretch contains 317 packages.
> 
> Among those there are 1 packages that Freexian will not support (see the
> warnings.csv file for details). Among the 316 packages that we can
> support, there are 122 that have an history of security vulnerabilities.
>
> [...]
> 
> *  2022-H2: 5250 EUR
> *  2023-H1: 5775 EUR
> *  2023-H2: 6300 EUR
> *  2024-H1: 6930 EUR
> *  2024-H2: 7560 EUR
> *  2025-H1: 10185 EUR
> *  2025-H2: 12810 EUR
> *  2026-H1: 15435 EUR
> *  2026-H2: 18060 EUR
> *  2027-H1: 20580 EUR

The associated [warnings.csv](sample2.warnings.csv) file only mentions that
"linux" is not supported and that one should use our backport of a newer
kernel.
