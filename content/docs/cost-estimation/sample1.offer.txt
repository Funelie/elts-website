Your list of packages to support in Debian stretch contains 163 packages.

Among those there are 0 packages that Freexian will not support (see the
warnings.csv file for details). Among the 163 packages that we can
support, there are 80 that have an history of security vulnerabilities.

The price for each semester consists of a fixed sum (covering the cost of
the base system and infrastructure costs) and of a variable amount
depending on the cost of your packages (you can have some details about
the price of each package in the initial period in the packages-cost.csv
file) and of the time elapsed.

The price increase tries to model the loss of customers over time, it's a
slow increase over the first half of the ELTS period and a steep increase
afterwards because the bulk of the customers will have migrated to another
Debian release at that time.

 2022-H2: 3885 EUR
 2023-H1: 4305 EUR
 2023-H2: 4725 EUR
 2024-H1: 5145 EUR
 2024-H2: 5670 EUR
 2025-H1: 7455 EUR
 2025-H2: 9345 EUR
 2026-H1: 11130 EUR
 2026-H2: 12915 EUR
 2027-H1: 14700 EUR

