+++
title = "About Debian 8 Jessie"
date = 2022-04-20T11:58:57+02:00
weight = 160
draft = false
bref = "Everything that you need to know about our support of Debian 8 Jessie"
toc = false
+++

## Extended LTS for Debian 8 Jessie

### Support period

* 2015-04-26: Publication of Debian 8 Jessie
* 2018-06-17: Security support handed over to the Debian LTS team.
* 2020-06-30: End of support in Debian. Security support now handled by
  Freexian's Extended LTS service.
* 2025-06-30: End of support.

### Architectures supported

* amd64
* i386
* armel

### Limitations of support

Not all packages can be supported by our Extended LTS for Debian 8 service:

* packages that have already been marked as unsupported by the Debian
  security/LTS teams are not supported:
  [see list in git repository](https://salsa.debian.org/debian/debian-security-support/-/blob/jessie/security-support-ended.deb8)
* linux: we don't provide security support for Linux 3.16 that was
  originally provided in Debian 8 Jessie. Instead we maintain a
  [backport](../kernel-backport/).
* libav will not be supported
* mariadb-10.0: it's EOL at the upstream level
* tomcat7: has reached its end-of-life (EOL) on March 2021. We continue to
  support it on a best effort basis but recommend to use Tomcat 8 instead
* openjdk-7 will be supported as long as it's maintained upstream,
  and we will also maintain an openjdk-8 backport.

Note that when you request a quote, we send you back a list of packages
that are not supported or that have limitations in their support so that
you can take an informed decision.

### List of base packages

The following packages are part of Debian's 8 base system and will thus
always be supported (as long as we have customers paying for Debian 8
support):

* acl
* adduser
* apt
* attr
* audit
* base-files
* base-passwd
* bash
* bind9
* boost1.55
* bsdmainutils
* bzip2
* ca-certificates
* cdebconf
* coreutils
* cpio
* cron
* cryptsetup
* dash
* db5.3
* debconf
* debian-archive-keyring
* debianutils
* diffutils
* dmidecode
* dpkg
* e2fsprogs
* findutils
* gcc-4.8
* gcc-4.9
* gdbm
* glibc
* gmp
* gnupg
* gnutls28
* grep
* groff
* gzip
* hostname
* icu
* ifupdown
* init-system-helpers
* insserv
* iproute2
* iptables
* iputils
* isc-dhcp
* json-c
* keyutils
* kmod
* krb5
* less
* libbsd
* libcap2
* libedit
* libestr
* libffi
* libgcrypt20
* libgpg-error
* libidn
* liblocale-gettext-perl
* liblogging
* liblognorm
* libmnl
* libnetfilter-acct
* libnfnetlink
* libpipeline
* libpsl
* libselinux
* libsemanage
* libsepol
* libsigc++-2.0
* libtasn1-6
* libtext-charwidth-perl
* libtext-iconv-perl
* libtext-wrapi18n-perl
* libusb
* libx11
* libxau
* libxcb
* libxdmcp
* libxext
* libxmu
* logrotate
* lsb
* lvm2
* man-db
* manpages
* mawk
* nano
* ncurses
* netbase
* netcat
* nettle
* net-tools
* newt
* nfacct
* openssh
* openssl
* p11-kit
* pam
* pcre3
* perl
* popt
* procps
* readline6
* rsyslog
* sed
* sensible-utils
* shadow
* slang2
* startpar
* sudo
* systemd
* sysvinit
* tar
* tasksel
* tcp-wrappers
* traceroute
* tzdata
* ustr
* util-linux
* vim
* wget
* xauth
* xz-utils
* zlib
