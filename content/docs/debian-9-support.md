+++
title = "About Debian 9 Stretch"
date = 2022-04-20T11:58:57+02:00
weight = 150
draft = false
bref = "Everything that you need to know about our support of Debian 9 Stretch"
toc = false
+++

## Extended LTS for Debian 9 Stretch

### Support period

* 2017-06-17: Publication of Debian 9 Stretch
* 2020-06-30: Security support handed over to the Debian LTS team.
* 2022-06-30: End of support in Debian. Security support now handled by
  Freexian's Extended LTS service.
* 2027-06-30: End of support.

### Architectures supported

* amd64
* i386
* armhf

If you need another architecture, please subscribe and let us know your
requirements.

### Limitations of support

Not all packages can be supported by our Extended LTS for Debian 9 service:

* packages that have already been marked as unsupported by the Debian
  security/LTS teams are not supported:
  [see list in git repository](https://salsa.debian.org/debian/debian-security-support/-/blob/master/security-support-ended.deb9)
* linux: we don't provide security support for Linux 4.9 that was
  originally provided in Debian 9 Stretch. Instead we maintain a
  [backport](../kernel-backport/).

Note that when you request a quote, we send you back a list of packages
that are not supported or that have limitations in their support so that
you can take an informed decision.

### List of base packages

The following packages are part of Debian's 9 base system and will thus
always be supported (as long as we have customers paying for Debian 9
support):

* acl
* adduser
* apparmor
* apt
* attr
* audit
* base-files
* base-passwd
* bash
* bind9
* bsdmainutils
* bzip2
* ca-certificates
* cdebconf
* coreutils
* cpio
* cron
* cryptsetup
* dash
* db5.3
* dbus
* debconf
* debian-archive-keyring
* debianutils
* diffutils
* dmidecode
* dpkg
* e2fsprogs
* elfutils
* expat
* findutils
* gcc-6
* gdbm
* glibc
* gmp
* gnupg2
* gnutls28
* grep
* gzip
* hostname
* ifupdown
* init-system-helpers
* iproute2
* iptables
* iputils
* isc-dhcp
* keyutils
* kmod
* krb5
* libassuan
* libbsd
* libcap2
* libcap-ng
* libedit
* libestr
* libfastjson
* libffi
* libgcrypt20
* libgpg-error
* libidn
* libidn2-0
* libksba
* liblocale-gettext-perl
* liblogging
* liblognorm
* libmnl
* libnetfilter-conntrack
* libnfnetlink
* libpipeline
* libpsl
* libseccomp
* libselinux
* libsemanage
* libsepol
* libtasn1-6
* libtext-charwidth-perl
* libtext-iconv-perl
* libtext-wrapi18n-perl
* libunistring
* libx11
* libxau
* libxcb
* libxdmcp
* libxext
* libxmu
* logrotate
* lsb
* lvm2
* lz4
* mawk
* nano
* ncurses
* netbase
* nettle
* newt
* npth
* openssh
* openssl
* openssl1.0
* p11-kit
* pam
* pcre3
* perl
* pinentry
* popt
* procps
* readline
* rsyslog
* sed
* sensible-utils
* shadow
* slang2
* sqlite3
* sudo
* systemd
* sysvinit
* tar
* tasksel
* tcp-wrappers
* tzdata
* ucf
* ustr
* util-linux
* vim
* wget
* xapian-core
* xauth
* xz-utils
* zlib
