+++
title = "How to follow extended LTS updates"
date = 2018-05-30T12:46:57+02:00
weight = 103
draft = false
bref = "You probably want to be notified when security updates are released, here's how"
toc = false
+++

## Follow with an RSS feed

The Extended LTS Advisories (ELA) are published in the
[Updates](../../updates/) section of this website. They can
be monitored through this [RSS feed](../../updates/index.xml).

## Follow by email

If you want to be notified by email, you can subscribe to
our [dedicated mailing list](http://eepurl.com/dChbTH).
[Click here](http://eepurl.com/dChbTH), fill the fields,
submit the form, confirm the captcha, and you are done.
