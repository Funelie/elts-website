+++
title = "How to build a package list"
date = 2022-04-20T12:46:57+02:00
weight = 120
draft = false
bref = "Package lists are required in a specific format for Freexian to be able to provide a good quote"
toc = false
+++

## One package list per Debian release

We need a list of packages to support for each Debian release that
you are using. The best way to build this list is to gather the list
of installed packages on all the Debian systems that you are running.

The dpkg-query command can be used for this:
```
$ dpkg-query -f'${source:Package},${Package},${Version}\n' -W
```

You need to analyze separately your Debian systems based on the version
of Debian that they are running.

## Format of the package list

The package list must follow the same format as what's produced by the
above `dpkg-query` command.

The result is a file with one line per binary package and 3
comma-separated fields on each line:

* the name of the source package
* the name of the binary package
* the version of the binary package

## Combining lists from multiple systems

If you have Debian systems with different list of installed packages,
you should run this command on each Debian system:

```
$ dpkg-query -f'${source:Package},${Package},${Version}\n' -W >$(hostname).pkglist
```

This produces a *hostname*.pkglist file. Collect all those `*.pkglist`
files in the same location and then merge them with this command:

```
$ sort -u *.pkglist >final.pkglist
```

This `final.pkglist` file is the file that you should submit to Freexian
at sales@freexian.com. Feel free to drop packages from the generated list
to only keep those that truly matter to you.

Note that you should provide us one package list for each Debian release
that you need support for. So only combine lists from systems running the
same Debian release. 
