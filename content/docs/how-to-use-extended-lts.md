+++
title = "How to use Extended LTS"
date = 2018-05-30T12:13:12+02:00
weight = 100
draft = false
bref = "To benefit from the security updates, you just have to configure APT to use our extended LTS repository"
toc = false
+++

### Adding extended LTS repositories to APT

#### Installing the freexian archive GPG key

The extended LTS repositories are signed with the following GPG key:
```
sec   rsa4096 2018-05-28 [SC] [expires: 2027-12-05]
      AB597C4F6F3380BD4B2BEBC2A07310D369055D5A
uid           [ultimate] Extended LTS Repository <sysadmin@freexian.com
```

To enable this key in your APT configuration, you have the following
choices:

* manually install the freexian-archive-keyring package with `wget https://deb.freexian.com/extended-lts/pool/main/f/freexian-archive-keyring/freexian-archive-keyring_2022.06.08_all.deb && sudo dpkg -i freexian-archive-keyring_2022.06.08_all.deb`
* manually fetch the key with `sudo wget https://deb.freexian.com/extended-lts/archive-key.gpg -O /etc/apt/trusted.gpg.d/freexian-archive-extended-lts.gpg`

You might want to double check that the key fingerprint displayed by
`apt-key finger` matches the one shown above.

#### sources.list entries for APT

##### For Debian 8 jessie

Here's what you should put in `/etc/apt/sources.list.d/extended-lts.list`:

```
deb http://deb.freexian.com/extended-lts jessie-lts main contrib non-free
```

Note that this repository only contains the security updates, not all
packages from Debian 8. If you want all packages from Debian 8, you
should keep another repository pointing to a Debian 8 mirror.

We do provide a repository combining all Debian 8 packages and our security
updates, but please use it only for small setups:

```
deb http://deb.freexian.com/extended-lts jessie main contrib non-free
```

##### For Debian 9 stretch

Here's what you should put in `/etc/apt/sources.list.d/extended-lts.list`:

```
deb http://deb.freexian.com/extended-lts stretch-lts main contrib non-free
```

Note that this repository only contains the security updates, not all
packages from Debian 9. If you want all packages from Debian 9, you
should keep another repository pointing to a Debian 9 mirror.

We do provide a repository combining all Debian 9 packages and our security
updates, but please use it only for small setups:

```
deb http://deb.freexian.com/extended-lts stretch main contrib non-free
```

### Be nice, use local mirrors/caches

There are currently no public mirrors of this service and it runs on a
single dedicated server. If you have many machines to keep secure, please
make a local mirror (or use some cache) and point your machines to your
local mirror (or cache) instead of pointing them to the 
repositories provided by Freexian.
