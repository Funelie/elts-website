+++
title = "How to install Linux Kernel backports"
date = 2018-07-10T11:58:57+02:00
weight = 102
draft = false
bref = "We maintain backports of newer kernels to keep old distributions secure. Here's how to use them"
toc = false
+++

### Installing Linux Kernel backports

#### For Debian 8 jessie

For Debian 8 (jessie), Linux kernels 3.16 and 4.9 are no longer maintained.
Instead, we provide a backport of Linux 4.19. The packages are available
in the regular jessie-lts repository and you can install it this way:

```
apt-get install linux-image-4.19-amd64
```
or for 32bit
```
apt-get install linux-image-4.19-686-pae
```

#### For Debian 9 stretch

For Debian 9 (stretch), Linux kernel 4.9 is no longer maintained.
Instead, we provide two kernel backports. Linux 4.19, which will be
supported until June 2024, and Linux 5.10, supported until June 2026.
The packages can be installed from the stretch-lts repository this way:

##### For Linux 4.19
```
apt-get install linux-image-4.19-amd64
```
or for 32bit
```
apt-get install linux-image-4.19-686-pae
```

##### For Linux 5.10
```
apt-get install linux-image-5.10-amd64
```
or for 32bit
```
apt-get install linux-image-5.10-686-pae
```
