---
title: "ELA-1-1 git security update"
package: git
version: 1:1.7.10.4-1+wheezy7
distribution: "Debian 7 Wheezy"
description: "arbitrary code execution vulnerability"
date: 2018-06-07T13:43:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-11235
---

Etienne Stalmans discovered that git, a fast, scalable, distributed revision
control system, is prone to an arbitrary code execution vulnerability
exploitable via specially crafted submodule names in a .gitmodules file.
