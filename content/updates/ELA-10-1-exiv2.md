---
title: "ELA-10-1 exiv2 security update"
package: exiv2
version: 0.23-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-06-28T04:47:48Z
draft: false
type: updates
cvelist:
  - CVE-2018-10958
  - CVE-2018-10998
  - CVE-2018-10999
  - CVE-2018-11531
  - CVE-2018-12264
  - CVE-2018-12265
---

Several vulnerabilities have been discovered in exiv2, a C++ library and
a command line utility to manage image metadata, resulting in denial of
service, heap-based buffer over-read/overflow, memory exhaustion, and
application crash.
