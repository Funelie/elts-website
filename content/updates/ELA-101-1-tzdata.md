---
title: "ELA-101-1 tzdata new upstream version"
package: tzdata
version: 2019a-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "new version update"
date: 2019-04-01T12:08:14+02:00
draft: false
type: updates
cvelist:

---

This update brings the timezone changes from the upstream 2019a release.
