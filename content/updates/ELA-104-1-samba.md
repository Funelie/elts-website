---
title: "ELA-104-1 samba security update"
package: samba
version: 2:3.6.6-6+deb7u19
distribution: "Debian 7 Wheezy"
description: "disable Windows registry service RPC API"
date: 2019-04-09T22:41:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-3880

---

A flaw was found in the way Samba implemented an RPC endpoint emulating
the Windows registry service API. An unprivileged attacker could have
used this flaw to create a new registry hive file anywhere they had unix
permissions which could have lead to creation of a new file in the Samba
share.
