---
title: "ELA-105-1 sqlalchemy security update"
package: sqlalchemy
version: 0.7.8-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "SQL Injection"
date: 2019-04-10T00:15:18+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-7164
  - CVE-2019-7548

---

Two vulnerabilities were discovered in SQLALchemy, a Python SQL Toolkit and
Object Relational Mapper.

CVE-2019-7164

    SQLAlchemy allows SQL Injection via the order_by parameter.

CVE-2019-7548

    SQLAlchemy allows SQL Injection when the group_by parameter can be controlled.

The SQLAlchemy project warns that these security fixes break the seldom-used
text coercion feature.
