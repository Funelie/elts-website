---
title: "ELA-106-1 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u16
distribution: "Debian 7 Wheezy"
description: "fixes buffer under-read, NULL pointer deref, missing bounds checks"
date: 2019-04-14T16:45:48+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-10899
  - CVE-2019-10901
  - CVE-2019-10903

---

Several vulnerabilities have been found in wireshark, a network traffic analyzer.

CVE-2019-10899: heap based buffer under-read in the SRVLOC dissector.

CVE-2019-10901: NULL pointer dereference in the LDSS dissector.

CVE-2019-10903: missing boundary checks causing Resource Management Errors in the DCERPC SPOOLSS dissector.

These vulnerabilities might be leveraged by remote attackers to cause denial of service (DoS) via a crafted packet or PCAP file.
