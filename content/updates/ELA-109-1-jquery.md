---
title: "ELA-109-1 jquery security update"
package: jquery
version: 1.7.2+dfsg-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "Prototype Pollution"
date: 2019-04-20T17:02:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11358

---

jQuery mishandles jQuery.extend(true, {}, ...) because of Object.prototype
pollution. If an unsanitized source object contained an enumerable __proto__
property, it could extend the native Object.prototype.
