---
title: "ELA-112-1 wget security update"
package: wget
version: 1.13.4-3+deb7u7
distribution: "Debian 7 Wheezy"
description: "Fix of a buffer overflow vulnerability."
date: 2019-04-23T23:16:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-5953

---

Kusano Kazuhiko discovered a buffer overflow vulnerability in the handling of Internationalized Resource Identifiers (IRI) in wget, a network utility to retrieve files from the web, which could result in the execution of arbitrary code or denial of service when recursively downloading from an untrusted server.

