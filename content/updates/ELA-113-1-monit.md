---
title: "ELA-113-1 monit security update"
package: monit
version: 1:5.4-2+deb7u4
distribution: "Debian 7 Wheezy"
description: "fix for several buffer overruns"
date: 2019-04-27T11:24:07+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11454
  - CVE-2019-11455

---

Zack Flack found several issues in monit, a utility for monitoring and managing daemons or similar programs.

CVE-2019-11454:
     An XSS vulnerabilitty has been reported that could be prevented by HTML escaping the log file content when viewed via Monit GUI.

CVE-2019-11455:
     A buffer overrun vulnerability has been reported in URL decoding.

