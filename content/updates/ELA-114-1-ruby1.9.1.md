---
title: "ELA-114-1 ruby1.9.1 security update"
package: ruby1.9.1
version: 1.9.3.194-8.1+deb7u9
distribution: "Debian 7 Wheezy"
description: "fix several escape sequence injections, directory traversal fix"
date: 2019-04-30T10:11:29+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-8320
  - CVE-2019-8322
  - CVE-2019-8323
  - CVE-2019-8325

---

Several vulnerabilities have been discovered in rubygems embedded in
ruby1.9.1, the interpreted scripting language.

CVE-2019-8320:
    A Directory Traversal issue was discovered in RubyGems. Before
    making new directories or touching files (which now include
    path-checking code for symlinks), it would delete the target
    destination.

CVE-2019-8322:
    The gem owner command outputs the contents of the API response
    directly to stdout. Therefore, if the response is crafted, escape
    sequence injection may occur.

CVE-2019-8323:
    Gem::GemcutterUtilities#with_response may output the API response to
    stdout as it is. Therefore, if the API side modifies the response,
    escape sequence injection may occur.

CVE-2019-8325:
    Since Gem::CommandManager#run calls alert_error without escaping,
    escape sequence injection is possible. (There are many ways to cause
    an error.)
