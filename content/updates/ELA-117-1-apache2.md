---
title: "ELA-117-1 apache2 security update"
package: apache2
version: 2.2.22-13+deb7u14
distribution: "Debian 7 Wheezy"
description: "access control bypass"
date: 2019-05-15T20:23:02+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-0217
  - CVE-2019-0220

---

CVE-2019-0217

    Simon Kappel discovered a race condition in mod_auth_digest when running in
    a threaded server which could allow a user with valid credentials to
    authenticate using another username, bypassing configured access control
    restrictions.

CVE-2019-0220

    Bernhard Lorenz of Alpha Strike Labs GmbH discovered a httpd URL
    normalization inconsistincy when the path component of a request URL
    contains multiple consecutive slashes ('/'), directives such as
    LocationMatch and RewriteRule must account for duplicates in regular
    expressions while other aspects of the servers processing will implicitly
    collapse them.
