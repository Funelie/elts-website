---
title: "ELA-118-1 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u17
distribution: "Debian 7 Wheezy"
description: "heap based out of bounds read and write issues"
date: 2019-05-21T10:01:32+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-10894
  - CVE-2019-10895

---

Several vulnerabilities have been found in wireshark, a network traffic analyzer.

CVE-2019-10894: assertion failure leading to crash.

CVE-2019-10895: large number of heap buffer overflows read and write in NetScaler trace handling.

These vulnerabilities might be leveraged by remote attackers to cause denial of service (DoS) via a crafted packet or PCAP file.
