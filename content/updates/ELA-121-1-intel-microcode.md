---
title: "ELA-121-1 intel-microcode security update"
package: intel-microcode
version: 3.20190514.1~deb7u1
distribution: "Debian 7 Wheezy"
description: "microarchitectural data sampling vulnerabilities"
date: 2019-05-27T16:32:18+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-12126
  - CVE-2018-12127
  - CVE-2018-12130
  - CVE-2019-11091

---

This update ships updated CPU microcode for most types of Intel CPUs. It
provides microcode support to implement mitigations for the MSBDS,
MFBDS, MLPDS and MDSUM hardware vulnerabilities.

To fully resolve these vulnerabilities it is also necessary to update
the Linux kernel packages. An update for that will follow soon.
