---
title: "ELA-121-2 intel-microcode security update"
package: intel-microcode
version: 3.20190618.1~deb7u1
distribution: "Debian 7 Wheezy"
description: "microarchitectural data sampling vulnerabilities"
date: 2019-06-21T11:40:18+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-12126
  - CVE-2018-12127
  - CVE-2018-12130
  - CVE-2019-11091

---

ELA-121-1 shipped updated CPU microcode for most types of Intel CPUs as
mitigations for the MSBDS, MFBDS, MLPDS and MDSUM hardware vulnerabilities.

This update provides additional support for some Sandybridge server
and Core-X CPUs which were not covered in the original May microcode
release. For a list of specific CPU models now supported please refer
to the entries listed under CPUID 206D6 and 206D7 at
https://www.intel.com/content/dam/www/public/us/en/documents/corporate-information/SA00233-microcode-update-guidance_05132019.pdf
