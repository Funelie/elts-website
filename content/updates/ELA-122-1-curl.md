---
title: "ELA-122-1 curl security update"
package: curl
version: 7.26.0-1+wheezy25+deb7u4
distribution: "Debian 7 Wheezy"
description: "fix for heap buffer overflow"
date: 2019-05-27T22:00:48+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-5436

---

cURL, an URL transfer library, contains a heap buffer overflow in the
function tftp_receive_packet() that receives data from a TFTP server.
It calls recvfrom() with the default size for the buffer rather than
with the size that was used to allocate it. Thus, the content that
might overwrite the heap memory is entirely controlled by the server.
