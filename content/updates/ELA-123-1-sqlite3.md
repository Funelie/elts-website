---
title: "ELA-123-1 sqlite3 security update"
package: sqlite3
version: 3.7.13-1+deb7u6
distribution: "Debian 7 Wheezy"
description: "heap out-of-bound vulnerability"
date: 2019-05-31T03:51:55+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-8457

---

SQLite3 was vulnerable to heap out-of-bound read in the rtreenode()
function when handling invalid rtree tables.
