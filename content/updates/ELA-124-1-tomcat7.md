---
title: "ELA-124-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u21
distribution: "Debian 7 Wheezy"
description: "XSS in SSI printenv"
date: 2019-05-31T10:14:31+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-0221

---

It was discovered that the SSI printenv command echoes user provided data
without escaping and is, therefore, vulnerable to XSS. SSI is disabled by
default. The printenv command is intended for debugging and is unlikely to be
present in a production website.
