---
title: "ELA-125-1 glib2.0 security update"
package: glib2.0
version: 2.33.12+really2.32.4-5+deb7u1
distribution: "Debian 7 Wheezy"
description: "unwanted file access during creation/copy of files"
date: 2019-06-01T01:28:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-12450

---

The file_copy_fallback() function/method in gio/gfile.c in GNOME GLib did
not properly restrict file permissions while a copy operation was in
progress. Instead, default permissions were used. A similar issue of the
need of tigher permissions was also spotted still unfixed in the keyfile
settings (gio/gkeyfilesettingsbackend.c).
