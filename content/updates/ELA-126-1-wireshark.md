---
title: "ELA-126-1 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u18
distribution: "Debian 7 Wheezy"
description: "multiple DoS vulnerabilities"
date: 2019-06-02T15:36:04+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-9345
  - CVE-2017-9352
  - CVE-2017-9617
  - CVE-2017-13767
  - CVE-2019-12295

---

Several vulnerabilities have been found in wireshark, a network traffic analyzer.

CVE-2017-9345: infinite loop in DNS dissector.

CVE-2017-9352: infinite loop in Bazaar dissector.

CVE-2017-9617: stack exhaustion in adissect_daap_one_tag (DAAP dissector).

CVE-2017-13767: infinite loop in MSDP dissector.

CVE-2019-12295: stack overflow in the dissection engine.

These vulnerabilities might be leveraged by remote attackers to cause denial of service (DoS) via a crafted packet or PCAP file.
