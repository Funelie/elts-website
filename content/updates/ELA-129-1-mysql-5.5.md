---
title: "ELA-129-1 mysql-5.5 end of life"
distribution: "Debian 7 Wheezy"
description: "end of life announcement"
date: 2019-06-06T14:42:31+02:00
draft: false

---

Upstream has ended the support for the MySQL 5.5 release series, and since
no information is available which would allow backports of isolated
security fixes, security support for MySQL in wheezy has to be ended as well.

Support will end on June 30th, 2019. Any serious flaws that are discovered
before then will be backported if feasible.
