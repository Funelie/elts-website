---
title: "ELA-13-1 ca-certificates security update"
package: ca-certificates
version: 20130119+deb7u3
distribution: "Debian 7 Wheezy"
description: "Certificate Authority update"
date: 2018-07-07T11:27:37+02:00
draft: false
---
There have been a number of updates to the set of Certificate Authority
(CA) certificates that are considered "valid" or otherwise should be
trusted.
