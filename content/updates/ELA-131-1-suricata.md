---
title: "ELA-131-1 suricata security update"
package: suricata
version: 1.2.1-2+deb7u4
distribution: "Debian 7 Wheezy"
description: "buffer overflow issue"
date: 2019-06-20T10:03:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-10053

---

It was discovered that suricata, the network threat detection engine, is
vulnerable to a buffer overflow issue when parsing SSH banners. This flaw might
be leveraged by remote attackers to cause unauthorized disclosure and
modification of information, or denial of service via a crafted SSH banner.
