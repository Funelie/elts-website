---
title: "ELA-137-1 libvirt security update"
package: libvirt
version: 0.9.12.3-1+deb7u4
distribution: "Debian 7 Wheezy"
description: "arbitrary file read and denial-of-service"
date: 2019-06-28T22:10:29+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-10161

---

A number of related vulnerabilities in the libvirt management API were recently
discovered and fixed by the libvirt maintainers. These vulnerabilities expose
unintended functionality to API clients with read-only permissions that could
be used by the client to perform operations outside their normal sphere of
permissions. An attacker could test for the existence of files on the host as
root. Libvirtd can be given an arbitrary path to read a saved state file,
which it will attempt to read. This may also be exploited for a
denial-of-service attack by choosing particular paths in /dev or /proc.
