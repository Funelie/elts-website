---
title: "ELA-138-1 ntfs-3g security update"
package: ntfs-3g
version: 1:2012.1.15AR.5-2.1+deb7u4
distribution: "Debian 7 Wheezy"
description: "fix for heap buffer overflow"
date: 2019-06-29T19:50:00+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-9755

---

A heap-based buffer overflow was discovered in NTFS-3G, a read-write NTFS driver for FUSE. A local user can take advantage of this flaw for local root privilege escalation.
