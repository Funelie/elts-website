---
title: "ELA-139-1 bash security update"
package: bash
version: 4.2+dfsg-0.1+deb7u6
distribution: "Debian 7 Wheezy"
description: "heap-based buffer overflow"
date: 2019-07-03T13:38:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2012-6711

---

A heap-based buffer overflow was discovered in bash caused by a wrong
handling of unsupported characters in the function u32cconv(). When
LC_CTYPE locale cannot correctly convert a wide character to a multibyte
sequence, through the wctomb() function, u32cconv() returns a negative
value that is used to update a pointer to a buffer in ansicstr(), resulting
in a write out of the buffer's bounds. A local attacker, who can provide
data to print through the `echo` builtin function, may use this flaw to
crash a script or execute code with the privileges of the bash process
(e.g. escape a restricted bash or elevate privileges if a setuid script is
vulnerable).
