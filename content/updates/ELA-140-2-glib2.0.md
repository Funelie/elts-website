---
title: "ELA-140-2 glib2.0 regression update"
package: glib2.0
version: 2.33.12+really2.32.4-5+deb7u3
distribution: "Debian 7 Wheezy"
description: "Memory leak regression fix"
date: 2019-08-06T00:27:19+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13012

---

Simon McVittie spotted a memory leak regression in the way CVE-2019-13012 had been resolved
for glib2.0 in Debian wheezy ELTS.
