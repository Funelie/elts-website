---
title: "ELA-141-1 unzip security update"
package: unzip
version: 6.0-8+deb7u7
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2019-07-09T23:23:20+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13232

---

David Fifield discovered a way to construct non-recursive "zip bombs" that
achieve a high compression ratio by overlapping files inside the zip
container. However the output size increases quadratically in the input
size, reaching a compression ratio of over 28 million (10 MB -> 281 TB) at
the limits of the zip format which can cause a denial-of-service. Mark
Adler provided a patch to detect and reject such zip files for the unzip
program.
