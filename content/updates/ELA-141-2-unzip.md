---
title: "ELA-141-2 unzip regression update"
package: unzip
version: 6.0-8+deb7u8
distribution: "Debian 7 Wheezy"
description: "regression update"
date: 2019-09-09T12:47:20+02:00
draft: false
type: updates
cvelist:
- CVE-2019-13232

---

The unzip security update issued as ELA 141-1 caused a regression when
extracting some non-standard zip files.

For instance there is a zip-like file in the Firefox distribution,
omni.ja, which is a zip container with the central directory placed at
the start of the file instead of after the local entries as required
by the zip standard.  This update now permits such containers to not
raise a zip bomb alert, where in fact there are no overlaps.
