---
title: "ELA-142-1 libxslt security update"
package: libxslt
version: 1.1.26-14.1+deb7u5
distribution: "Debian 7 Wheezy"
description: "uninitialized read"
date: 2019-07-11T20:11:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13117
  - CVE-2019-13118

---

Two flaws were discovered in libxslt, the XSLT processing library.

    CVE-2019-13117

    An xsl:number with certain format strings could lead to an uninitialized
    read in xsltNumberFormatInsertNumbers. This could allow an attacker to
    discern whether a byte on the stack contains the characters A, a, I, i, or
    0, or any other character.

    CVE-2019-13118

    A type holding grouping characters of an xsl:number instruction was too
    narrow and an invalid character/length combination could be passed to
    xsltNumberFormatDecimal, leading to a read of uninitialized stack data.
