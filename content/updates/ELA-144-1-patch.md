---
title: "ELA-144-1 patch security update"
package: patch
version: 2.6.1-3+deb7u2
distribution: "Debian 7 Wheezy"
description: "mishandling of symlinks"
date: 2019-07-19T23:40:48+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13636

---

The handling of symlinks while creating backup files had to be improved.
