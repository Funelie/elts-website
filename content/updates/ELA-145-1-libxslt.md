---
title: "ELA-145-1 libxslt security update"
package: libxslt
version: 1.1.26-14.1+deb7u6
distribution: "Debian 7 Wheezy"
description: "memory corruption"
date: 2019-07-20T17:53:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-4609
  - CVE-2016-4610

---

CVE-2016-4610

    Invalid memory access leading to DoS at exsltDynMapFunction. libxslt allows
    remote attackers to cause a denial of service (memory corruption) or
    possibly have unspecified other impact via unknown vectors.

CVE-2016-4609

    Out-of-bounds read at xmlGetLineNoInternal()
    libxslt allows remote attackers to cause a denial of service (memory
    corruption) or possibly have unspecified other impact via unknown vectors.
