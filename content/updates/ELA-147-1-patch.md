---
title: "ELA-147-1 patch security update"
package: patch
version: 2.6.1-3+deb7u3
distribution: "Debian 7 Wheezy"
description: "quoting vulnerability"
date: 2019-07-25T15:55:19+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13638

---

An issue with quoting has been found when invoking ed. In order to avoid this, ed is now directly started instead of calling a shell which starts ed.
