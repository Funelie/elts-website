---
title: "ELA-149-1 freetype security update"
package: freetype
version: 2.4.9-1.1+deb7u8
distribution: "Debian 7 Wheezy"
description: "Fix buffer over-read"
date: 2019-07-31T20:12:00+02:00
draft: false
type: updates
cvelist:
  - CVE-2015-9290

---

In FreeType a buffer over-read occured in type1/t1parse.c on function
T1_Get_Private_Dict. The fix assures that 'cur' in the parser code
doesn't point to the end of the file buffer.
