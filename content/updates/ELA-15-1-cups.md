---
title: "ELA-15-1 cups security update"
package: cups
version: 1.5.3-5+deb7u9
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-07-14T11:38:17+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-4180
  - CVE-2018-4181
  - CVE-2018-6553
---

Several vulnerabilities were discovered in CUPS, the Common UNIX Printing
System. These issues have been identified with the following CVE ids:

CVE-2018-4180

     Dan Bastone of Gotham Digital Science discovered that a local
     attacker with access to cupsctl could escalate privileges by setting
     an environment variable.

CVE-2018-4181

     Eric Rafaloff and John Dunlap of Gotham Digital Science discovered
     that a local attacker can perform limited reads of arbitrary files
     as root by manipulating cupsd.conf.

CVE-2018-6553

    Dan Bastone of Gotham Digital Science discovered that an attacker
    can bypass the AppArmor cupsd sandbox by invoking the dnssd backend
    using an alternate name that has been hard linked to dnssd.
