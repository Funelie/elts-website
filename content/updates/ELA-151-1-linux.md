---
title: "ELA-151-1 linux security update"
package: linux
version: 3.16.70-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "Linux Kernel update"
date: 2019-08-06T15:56:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-2101
  - CVE-2019-10639
  - CVE-2019-13272

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2019-2101

    Andrey Konovalov discovered that the USB Video Class driver
    (uvcvideo) did not consistently handle a type field in device
    descriptors, which could result in a heap buffer overflow.  This
    could be used for denial of service or possibly for privilege
    escalation.

CVE-2019-10639

    Amit Klein and Benny Pinkas discovered that the generation of IP
    packet IDs used a weak hash function that incorporated a kernel
    virtual address.  In Linux 3.16 this hash function is not used for
    IP IDs but is used for other purposes in the network stack.  In
    custom kernel configurations that enable kASLR, this might weaken
    kASLR.

CVE-2019-13272

    Jann Horn discovered that the ptrace subsystem in the Linux kernel
    mishandles the management of the credentials of a process that wants
    to create a ptrace relationship, allowing a local user to obtain root
    privileges under certain scenarios.

This update also fixes a regression introduced by the
original fix for CVE-2019-11478 (#930904), and includes other fixes
from upstream stable updates.

