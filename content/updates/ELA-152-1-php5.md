---
title: "ELA-152-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u24
distribution: "Debian 7 Wheezy"
description: "heap buffer overflows"
date: 2019-08-12T23:52:25+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11041
  - CVE-2019-11042

---

Two heap buffer overflows were found in the EXIF parsing code of PHP,
a widely-used open source general purpose scripting language.
