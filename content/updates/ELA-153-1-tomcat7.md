---
title: "ELA-153-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u22
distribution: "Debian 7 Wheezy"
description: "CGI outbound HTTP traffic redirection"
date: 2019-08-13T23:25:12+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-5388

---

An outbound HTTP traffic redirection issue was found in tomcat7, a
Java Servlet and JSP engine.

Apache Tomcat, when the CGI Servlet is enabled, follows RFC 3875
section 4.1.18 and therefore does not protect applications from the
presence of untrusted client data in the HTTP_PROXY environment
variable, which might allow remote attackers to redirect an
application's outbound HTTP traffic to an arbitrary proxy server via a
crafted Proxy header in an HTTP request, aka an "httpoxy" issue.

The 'cgi' servlet now has a 'envHttpHeaders' parameter to filter
environment variables.
