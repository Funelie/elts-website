---
title: "ELA-156-1 linux security update"
package: linux
version: 3.16.72-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "Linux Kernel update"
date: 2019-08-26T13:06:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-18509
  - CVE-2018-20836
  - CVE-2019-1125
  - CVE-2019-3900
  - CVE-2019-10207
  - CVE-2019-10638
  - CVE-2019-13631
  - CVE-2019-14283
  - CVE-2019-14284

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2017-18509

    Denis Andzakovic reported a missing type check in the IPv4 multicast
    routing implementation. A user with the CAP_NET_ADMIN capability (in
    any user namespace) could use this for denial-of-service (memory
    corruption or crash) or possibly for privilege escalation.

CVE-2018-20836

    chenxiang reported a race condition in libsas, the kernel
    subsystem supporting Serial Attached SCSI (SAS) devices, which
    could lead to a use-after-free.  It is not clear how this might be
    exploited.

CVE-2019-1125

    It was discovered that most x86 processors could speculatively
    skip a conditional SWAPGS instruction used when entering the
    kernel from user mode, and/or could speculatively execute it when
    it should be skipped.  This is a subtype of Spectre variant 1,
    which could allow local users to obtain sensitive information from
    the kernel or other processes.  It has been mitigated by using
    memory barriers to limit speculative execution.  Systems using an
    i386 kernel are not affected as the kernel does not use SWAPGS.

CVE-2019-3900

    It was discovered that vhost drivers did not properly control the
    amount of work done to service requests from guest VMs.  A
    malicious guest could use this to cause a denial-of-service
    (unbounded CPU usage) on the host.

CVE-2019-10207

    The syzkaller tool found a potential null dereference in various
    drivers for UART-attached Bluetooth adapters.  A local user with
    access to a pty device or other suitable tty device could use this
    for denial-of-service (BUG/oops).

CVE-2019-10638

    Amit Klein and Benny Pinkas discovered that the generation of IP
    packet IDs used a weak hash function, "jhash".  This could enable
    tracking individual computers as they communicate with different
    remote servers and from different networks.  The "siphash"
    function is now used instead.

CVE-2019-13631

    It was discovered that the gtco driver for USB input tablets could
    overrun a stack buffer with constant data while parsing the device's
    descriptor.  A physically present user with a specially
    constructed USB device could use this to cause a denial-of-service
    (BUG/oops), or possibly for privilege escalation.

CVE-2019-14283

    The syzkaller tool found a missing bounds check in the floppy disk
    driver.  A local user with access to a floppy disk device, with a
    disk present, could use this to read kernel memory beyond the
    I/O buffer, possibly obtaining sensitive information.

CVE-2019-14284

    The syzkaller tool found a potential division-by-zero in the
    floppy disk driver.  A local user with access to a floppy disk
    device could use this for denial-of-service (oops).

(CVE ID not yet assigned)

    Denis Andzakovic reported a possible use-after-free in the
    TCP sockets implementation.  A local user could use this for
    denial-of-service (memory corruption or crash) or possibly
    for privilege escalation.

(CVE ID not yet assigned)

    The netfilter conntrack subsystem used kernel addresses as
    user-visible IDs, which could make it easier to exploit other
    security vulnerabilities.

XSA-300

    Julien Grall reported that Linux does not limit the amount of memory
    which a domain will attempt to balloon out, nor limits the amount of
    "foreign / grant map" memory which any individual guest can consume,
    leading to denial of service conditions (for host or guests).

