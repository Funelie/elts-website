---
title: "ELA-158-1 python2.7 security update"
package: python2.7
version: 2.7.3-6+deb7u7
distribution: "Debian 7 Wheezy"
description: "bad cookie handling"
date: 2019-08-31T19:29:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-20852

---

A vulnerability has been discovered in Python, that is relevant for cookie handling. By using a malicious server an attacker might steal cookies that are meant for other domains


