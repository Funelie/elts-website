---
title: "ELA-159-1 freetype security update"
package: freetype
version: 2.4.9-1.1+deb7u9
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-09-04T14:53:45+02:00
draft: false
type: updates
cvelist:
  - CVE-2015-9381
  - CVE-2015-9382
  - CVE-2015-9383

---

Several newly-referenced issues have been fixed in the FreeType 2 font
engine.

* CVE-2015-9381

    heap-based buffer over-read in T1_Get_Private_Dict in
    type1/t1parse.c

* CVE-2015-9382

    buffer over-read in skip_comment in psaux/psobjs.c because
    ps_parser_skip_PS_token is mishandled in an FT_New_Memory_Face
    operation

* CVE-2015-9383

    a heap-based buffer over-read in tt_cmap14_validate in
    sfnt/ttcmap.c
