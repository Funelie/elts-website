---
title: "ELA-160-1 exim4 security update"
package: exim4
version: 4.80-7+deb7u7
distribution: "Debian 7 Wheezy"
description: "bad handling of backslash"
date: 2019-09-06T12:25:19+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-15846

---

Zerons and Qualys discovered that a buffer overflow triggerable in the
TLS negotiation code of the Exim mail transport agent could result in the
execution of arbitrary code with root privileges.

