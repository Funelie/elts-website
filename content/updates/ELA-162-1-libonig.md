---
title: "ELA-162-1 libonig security update"
package: libonig
version: 5.9.1-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "stack exhaustion"
date: 2019-09-12T11:32:11+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-16163

---

The Oniguruma regular expressions library, notably used in PHP
mbstring, is vulnerable to stack exhaustion.  A crafted regular
expression can crash the process.
