---
title: "ELA-163-1 curl security update"
package: curl
version: 7.26.0-1+wheezy25+deb7u5
distribution: "Debian 7 Wheezy"
description: "heap-based buffer overflow"
date: 2019-09-15T18:21:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-5482

---

Thomas Vegas discovered a heap-based buffer overflow in the function
tftp_receive_packet() that receives data from a TFTP server. It can call
recvfrom() with the default size for the buffer rather than with the size that
was used to allocate it. Thus, the content that might overwrite the heap memory
is controlled by the server. This issue was introduced by the add of the TFTP
BLKSIZE option handling. It was previously incompletely fixed by an almost
identical issue called CVE-2019-5436.
