---
title: "ELA-164-1 python2.7 security update"
package: python2.7
version: 2.7.3-6+deb7u8
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-09-16T20:59:05Z
draft: false
type: updates
cvelist:
  - CVE-2013-1753
  - CVE-2014-4616
  - CVE-2014-4650
  - CVE-2014-7185
  - CVE-2019-16056

---

Vulnerabilities have been discovered in Python, an interactive
high-level object-oriented language.

CVE-2019-16056
    
    The email module wrongly parses email addresses that contain
    multiple @ characters. An application that uses the email module and 
    implements some kind of checks on the From/To headers of a message
    could be tricked into accepting an email address that should be
    denied.
    
CVE-2013-1753
    
    A denial of service (resource exhaustion, excessive memory
    consumption) can be triggered in the xmlrpc library by a specially
    crafted HTTP request.

CVE-2014-4616
  
    An attacker is able to read arbitrary process memory by a specially
    crafted JSON string.

CVE-2014-4650

    Information disclosure or arbirtary code execution is possible via a
    specially crafted URL because of improper handling of URL-encoded
    path separators in the CGIHTTPServer module.
    
CVE-2014-7185
  
    A context-dependent attacker can take advantage of an integer
    overflow to obtain sensitive information from process memory via a
    large size and offset in a "buffer" function.     

