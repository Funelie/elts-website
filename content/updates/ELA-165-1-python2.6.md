---
title: "ELA-165-1 python2.6 security update"
package: python2.6
version: 2.6.8-1.1+deb7u4
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-09-16T20:59:05Z
draft: false
type: updates
cvelist:
  - CVE-2013-4238
  - CVE-2014-1912
  - CVE-2014-7185
  - CVE-2019-16056

---

Vulnerabilities have been discovered in Python, an interactive
high-level object-oriented language. 

CVE-2019-16056

    The email module wrongly parses email addresses that contain
    multiple @ characters. An application that uses the email module and
    implements some kind of checks on the From/To headers of a message
    could be tricked into accepting an email address that should be
    denied.
    
CVE-2013-4238

    A man-in-the-middle attack is possible by spoof of arbitrary SSL
    servers via a crafted certificate resulting from improper handling
    of '\0' characters in a domain name in the Subject Alternative Name
    field of an X.509 certificate.

CVE-2014-1912
  
    Arbitrary remote code execution is possible via a crafted string
    resulting from a buffer overflow in the socket.recvfrom_into
    function.

CVE-2014-7185
  
    A context-dependent attacker can take advantage of an integer
    overflow to obtain sensitive information from process memory via a
    large size and offset in a "buffer" function.

