---
title: "ELA-167-1 cups security update"
package: cups
version: 1.5.3-5+deb7u11
distribution: "Debian 7 Wheezy"
description: "session cookies with predictable seed"
date: 2019-09-21T19:33:58+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-4300

---

An issue was found in cups, the Common UNIX Printing System(tm). Linux session cookies used a predictable random number seed.

This CVE is sometimes referenced as CVE-2018-4700. Please only use CVE-2018-4300 for it.
