---
title: "ELA-168-1 netty security update"
package: netty
version: 3.2.6.Final-2+deb7u1 
distribution: "Debian 7 Wheezy"
description: "RFC7230-compliant header name handling"
date: 2019-09-27T16:05:20+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-16869

---

Netty mishandled whitespace before the colon in HTTP headers (such as a
"Transfer-Encoding : chunked" line), which lead to HTTP request
smuggling.
