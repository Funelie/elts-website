---
title: "ELA-169-1 openldap security update"
package: openldap
version: 2.4.31-2+deb7u4
distribution: "Debian 7 Wheezy"
description: "fix for authorization issues"
date: 2019-09-28T11:21:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-13057
  - CVE-2019-13565

---

Several security vulnerabilities were discovered in openldap, a server and tools to provide a standalone directory service.


CVE-2019-13057

    When the server administrator delegates rootDN (database admin) privileges for certain databases but wants to maintain isolation (e.g., for multi-tenant deployments), slapd does not properly stop a rootDN from requesting authorization as an identity from another database during a SASL bind or with a proxyAuthz (RFC 4370) control.  (It is not a common configuration to deploy a system where the server administrator and a DB administrator enjoy different levels of trust.)



CVE-2019-13565

    When using SASL authentication and session encryption, and relying on the SASL security layers in slapd access controls, it is possible to obtain access that would otherwise be denied via a simple bind for any identity covered in those ACLs. After the first SASL bind is completed, the sasl_ssf value is retained for all new non-SASL connections. Depending on the ACL configuration, this can affect different types of operations (searches, modifications, etc.). In other words, a successful authorization step completed by one user affects the authorization requirement for a different user.

