---
title: "ELA-170-1 e2fsprogs security update"
package: e2fsprogs
version: 1.42.5-1.1+deb7u2
distribution: "Debian 7 Wheezy"
description: "avoid buffer overruns with maliciously corrupted file systems"
date: 2019-09-28T11:32:32+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-5094

---

Lilith of Cisco Talos discovered a buffer overflow flaw in the quota
code used by e2fsck from the ext2/ext3/ext4 file system utilities.
Running e2fsck on a malformed file system can result in the execution of
arbitrary code.
