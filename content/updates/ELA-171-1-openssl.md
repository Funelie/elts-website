---
title: "ELA-171-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb7u9
distribution: "Debian 7 Wheezy"
description: "possible information disclosure"
date: 2019-09-29T23:04:45+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-1547
  - CVE-2019-1563

---

Two security vulnerabilities were found in OpenSSL, the Secure Sockets
Layer toolkit.

CVE-2019-1547

    Normally in OpenSSL EC groups always have a co-factor present and
    this is used in side channel resistant code paths. However, in some
    cases, it is possible to construct a group using explicit parameters
    (instead of using a named curve). In those cases it is possible that
    such a group does not have the cofactor present. This can occur even
    where all the parameters match a known named curve. If such a curve
    is used then OpenSSL falls back to non-side channel resistant code
    paths which may result in full key recovery during an ECDSA
    signature operation. In order to be vulnerable an attacker
    would have to have the ability to time the creation of a large
    number of signatures where explicit parameters with no co-factor
    present are in use by an application using libcrypto. For the
    avoidance of doubt libssl is not vulnerable because explicit
    parameters are never used.

CVE-2019-1563

    In situations where an attacker receives automated notification of
    the success or failure of a decryption attempt an attacker, after
    sending a very large number of messages to be decrypted, can recover
    a CMS/PKCS7 transported encryption key or decrypt any RSA encrypted
    message that was encrypted with the public RSA key, using a
    Bleichenbacher padding oracle attack. Applications are not affected
    if they use a certificate together with the private RSA key to the
    CMS_decrypt or PKCS7_decrypt functions to select the correct
    recipient info to decrypt.

