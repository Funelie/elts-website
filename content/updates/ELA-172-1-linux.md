---
title: "ELA-172-1 linux security update"
package: linux
version: 3.16.74-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "Linux Kernel update"
date: 2019-10-02T18:40:05+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-10905
  - CVE-2018-20976
  - CVE-2018-21008
  - CVE-2019-0136
  - CVE-2019-9506
  - CVE-2019-14814
  - CVE-2019-14815
  - CVE-2019-14816
  - CVE-2019-14821
  - CVE-2019-14835
  - CVE-2019-15117
  - CVE-2019-15118
  - CVE-2019-15211
  - CVE-2019-15212
  - CVE-2019-15215
  - CVE-2019-15218
  - CVE-2019-15219
  - CVE-2019-15220
  - CVE-2019-15221
  - CVE-2019-15292
  - CVE-2019-15807
  - CVE-2019-15917
  - CVE-2019-15926

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2016-10905

    A race condition was discovered in the GFS2 file-system
    implementation, which could lead to a use-after-free.  On a system
    using GFS2, a local attacker could use this for denial of service
    (memory corruption or crash) or possibly for privilege escalation.

CVE-2018-20976

    It was discovered that the XFS file-system implementation did not
    correctly handle some mount failure conditions, which could lead
    to a use-after-free.  The security impact of this is unclear.

CVE-2018-21008

    It was discovered that the rsi wifi driver did not correctly
    handle some failure conditions, which could lead to a use-after-
    free.  The security impact of this is unclear.

CVE-2019-0136

    It was discovered that the wifi soft-MAC implementation (mac80211)
    did not properly authenticate Tunneled Direct Link Setup (TDLS)
    messages.  A nearby attacker could use this for denial of service
    (loss of wifi connectivity).

CVE-2019-9506

    Daniele Antonioli, Nils Ole Tippenhauer, and Kasper Rasmussen
    discovered a weakness in the Bluetooth pairing protocols, dubbed
    the "KNOB attack".  An attacker that is nearby during pairing
    could use this to weaken the encryption used between the paired
    devices, and then to eavesdrop on and/or spoof communication
    between them.

    This update mitigates the attack by requiring a minimum encryption
    key length of 56 bits.

CVE-2019-14814, CVE-2019-14815, CVE-2019-14816

    Multiple bugs were discovered in the mwifiex wifi driver, which
    could lead to heap buffer overflows.  A local user permitted to
    configure a device handled by this driver could probably use this
    for privilege escalation.

CVE-2019-14821

    Matt Delco reported a race condition in KVM's coalesced MMIO
    facility, which could lead to out-of-bounds access in the kernel.
    A local attacker permitted to access /dev/kvm could use this to
    cause a denial of service (memory corruption or crash) or possibly
    for privilege escalation.

CVE-2019-14835

    Peter Pi of Tencent Blade Team discovered a missing bounds check
    in vhost_net, the network back-end driver for KVM hosts, leading
    to a buffer overflow when the host begins live migration of a VM.
    An attacker in control of a VM could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation on the host.

CVE-2019-15117

    Hui Peng and Mathias Payer reported a missing bounds check in the
    usb-audio driver's descriptor parsing code, leading to a buffer
    over-read.  An attacker able to add USB devices could possibly use
    this to cause a denial of service (crash).

CVE-2019-15118

    Hui Peng and Mathias Payer reported unbounded recursion in the
    usb-audio driver's descriptor parsing code, leading to a stack
    overflow.  An attacker able to add USB devices could use this to
    cause a denial of service (memory corruption or crash) or possibly
    for privilege escalation.

CVE-2019-15211

    The syzkaller tool found a bug in the radio-raremono driver that
    could lead to a use-after-free.  An attacker able to add and
    remove USB devices could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.

CVE-2019-15212

    The syzkaller tool found that the rio500 driver does not work
    correctly if more than one device is bound to it.  An attacker
    able to add USB devices could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.

CVE-2019-15215

    The syzkaller tool found a bug in the cpia2_usb driver that leads
    to a use-after-free.  An attacker able to add and remove USB
    devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.

CVE-2019-15218

    The syzkaller tool found that the smsusb driver did not validate
    that USB devices have the expected endpoints, potentially leading
    to a null pointer dereference.  An attacker able to add USB
    devices could use this to cause a denial of service (BUG/oops).

CVE-2019-15219

    The syzkaller tool found that a device initialisation error in the
    sisusbvga driver could lead to a null pointer dereference.  An
    attacker able to add USB devices could use this to cause a denial
    of service (BUG/oops).

CVE-2019-15220

    The syzkaller tool found a race condition in the p54usb driver
    which could lead to a use-after-free.  An attacker able to add and
    remove USB devices could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.

CVE-2019-15221

    The syzkaller tool found that the line6 driver did not validate
    USB devices' maximum packet sizes, which could lead to a heap
    buffer overrun.  An attacker able to add USB devices could use
    this to cause a denial of service (memory corruption or crash) or
    possibly for privilege escalation.

CVE-2019-15292

    The Hulk Robot tool found missing error checks in the Appletalk
    protocol implementation, which could lead to a use-after-free.
    The security impact of this is unclear.

CVE-2019-15807

    Jian Luo reported that the Serial Attached SCSI library (libsas)
    did not correctly handle failure to discover devices beyond a SAS
    expander.  This could lead to a resource leak and crash (BUG).
    The security impact of this is unclear.

CVE-2019-15917

    The syzkaller tool found a race condition in code supporting
    UART-attached Bluetooth adapters, which could lead to a use-
    after-free.  A local user with access to a pty device or other
    suitable tty device could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.

CVE-2019-15926

    It was found that the ath6kl wifi driver did not consistently
    validate traffic class numbers in received control packets,
    leading to out-of-bounds memory accesses.  A nearby attacker on
    the same wifi network could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.
