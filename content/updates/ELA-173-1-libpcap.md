---
title: "ELA-173-1 libpcap security update"
package: libpcap
version: 1.3.0-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "improper input validation"
date: 2019-10-10T17:15:53+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-15165

---

libpcap, a system interface for user-level packet capture, does not
properly validate the PHB header length in .pcapng files before
allocating memory.
