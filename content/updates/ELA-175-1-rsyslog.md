---
title: "ELA-175-1 rsyslog security update"
package: rsyslog
version: 5.8.11-3+deb7u3
distribution: "Debian 7 Wheezy"
description: "heap overflow"
date: 2019-10-11T16:22:28+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-17041
  - CVE-2019-17042

---

Two heap overflow vulnerabilities were discovered in rsyslog, a system
and kernel logging daemon, in the AIX and Cisco log messages parsers
(not loaded in the default configuration).
