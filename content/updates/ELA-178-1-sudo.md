---
title: "ELA-178-1 sudo security update"
package: sudo
version: 1.8.5p2-1+nmu3+deb7u5
distribution: "Debian 7 Wheezy"
description: "potential bypass of Runas user restrictions"
date: 2019-10-17T21:30:22+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-14287

---

In sudo, a program that provides limited super user privileges to
specific users, an attacker with access to a Runas ALL sudoer account
can bypass certain policy blacklists and session PAM modules, and can
cause incorrect logging, by invoking sudo with a crafted user ID. For
example, this allows bypass of (ALL,!root) configuration for a "sudo
-u#-1" command.

See https://www.sudo.ws/alerts/minus_1_uid.html for further
information.
