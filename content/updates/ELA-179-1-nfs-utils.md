---
title: "ELA-179-1 nfs-utils security update"
package: nfs-utils
version: 1.2.6-4+deb7u1
distribution: "Debian 7 Wheezy"
description: "insecure permissions"
date: 2019-10-19T16:32:35+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-3689

---

In the nfs-utils package, providing support files for Network File
System (NFS) including the rpc.statd daemon, the directory
/var/lib/nfs is owned by statd:nogroup.  This directory contains files
owned and managed by root.  If statd is compromised, it can therefore
trick processes running with root privileges into creating/overwriting
files anywhere on the system.

