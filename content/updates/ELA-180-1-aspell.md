---
title: "ELA-180-1 aspell security update"
package: aspell
version: 0.60.7~20110707-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "buffer over-read"
date: 2019-10-21T23:56:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-17544

---

GNU Aspell, a spell-checker, is vulnerable to a stack-based buffer
over-read via an isolated \ character when processing a configuration
file.