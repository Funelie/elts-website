---
title: "ELA-181-1 blktrace security update"
package: blktrace
version: 1.0.1-2.1+deb7u1
distribution: "Debian 7 Wheezy"
description: "buffer overflow"
date: 2019-10-23T19:23:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10689

---

An issue in blktrace, a package containing utilities for block layer IO tracing, has been found.
As some arrays are too small, one could create a buffer overflow in dev_map_read() when using a crafted file.
