---
title: "ELA-182-1 file security update"
package: file
version: 5.11-2+deb7u11
distribution: "Debian 7 Wheezy"
description: "heap buffer overflow"
date: 2019-10-23T19:36:08+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-18218

---

An issue has been found in file, a tool to determine file types by using magic numbers.

The number of CDF_VECTOR elements had to be restricted in order to prevent a heap-based buffer overflow (4-byte out-of-bounds write).

