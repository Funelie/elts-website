---
title: "ELA-185-1 libxslt security update"
package: libxslt
version: 1.1.26-14.1+deb7u7
distribution: "Debian 7 Wheezy"
description: "information disclosure"
date: 2019-10-27T23:33:30+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-18197

---

A security vulnerability was discovered in libxslt, a XSLT 1.0
processing library written in C.

In xsltCopyText in transform.c, a pointer variable is not reset under
certain circumstances. If the relevant memory area happened to be freed
and reused in a certain way, a bounds check could fail and memory
outside a buffer could be written to, or uninitialized data could be
disclosed.
