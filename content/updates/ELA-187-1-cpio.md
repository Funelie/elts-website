---
title: "ELA-187-1 cpio security update"
package: cpio
version: 2.11+dfsg-0.1+deb7u3
distribution: "Debian 7 Wheezy"
description: "privilege escalation"
date: 2019-11-06T01:47:35+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-14866

---

It is possible for an attacker to create a file so when backed up with cpio
can generate arbitrary files in the resulting tar archive. When the backup is
restored the file is then created with arbitrary permissions.
