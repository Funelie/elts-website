---
title: "ELA-19-1 tiff3 security update"
package: tiff3
version: 3.9.6-11+deb7u12
distribution: "Debian 7 Wheezy"
description: "DoS vulnerabilities"
date: 2018-07-21T14:11:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-11613
  - CVE-2018-5784
---

Two flaws were found in the tiff3 image library that may allow remote attackers
to cause a denial of service (memory exhaustion or application crash) or
possibly have unspecified other impact via a crafted image file.

CVE-2017-11613

    In tiff3 there is a denial of service vulnerability in the TIFFOpen
    function. A crafted input will lead to a denial of service attack. During
    the TIFFOpen process, td_imagelength is not checked. The value of
    td_imagelength can be directly controlled by an input file. In the
    ChopUpSingleUncompressedStrip function, the _TIFFCheckMalloc function is
    called based on td_imagelength.  If we set the value of td_imagelength
    close to the amount of system memory, it will hang the system or trigger
    the OOM killer.

CVE-2018-5784

    A flaw was found in tiff3, there is an uncontrolled resource consumption in
    the TIFFSetDirectory function of tif_dir.c. Remote attackers could leverage
    this vulnerability to cause a denial of service via a crafted tif file.
    This is possible because the declared number of directory entries is not
    validated against the actual number of directory entries.

