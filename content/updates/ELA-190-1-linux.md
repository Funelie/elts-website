---
title: "ELA-190-1 linux security update"
package: linux
version: 3.16.76-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "Linux Kernel update"
date: 2019-11-20T20:34:58+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-0154
  - CVE-2019-11135

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.

CVE-2019-0154

    Intel discovered that on their 8th and 9th generation GPUs,
    reading certain registers while the GPU is in a low-power state
    can cause a system hang.  A local user permitted to use the GPU
    can use this for denial of service.

    This update mitigates the issue through changes to the i915
    driver.

    The affected chips (gen8) are listed at
    <https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8>;.

CVE-2019-11135

    It was discovered that on Intel CPUs supporting transactional
    memory (TSX), a transaction that is going to be aborted may
    continue to execute speculatively, reading sensitive data from
    internal buffers and leaking it through dependent operations.
    Intel calls this "TSX Asynchronous Abort" (TAA).

    For CPUs affected by the previously published Microarchitectural
    Data Sampling (MDS) issues (CVE-2018-12126, CVE-2018-12127,
    CVE-2018-12130, CVE-2019-11091), the existing mitigation also
    mitigates this issue.

    For processors that are vulnerable to TAA but not MDS, this update
    disables TSX by default. This mitigation requires updated CPU
    microcode. An updated intel-microcode package (only available in
    Debian non-free) will be provided via a future ELA. The updated
    CPU microcode may also be available as part of a system firmware
    ("BIOS") update.

    Further information on the mitigation can be found at
    <https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html>
    or in the linux-doc-3.16 package.

    Intel's explanation of the issue can be found at
    <https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort>;.
