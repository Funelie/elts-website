---
title: "ELA-193-1 libxdmcp security update"
package: libxdmcp
version: 1:1.1.1-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "weak entropy used for keys"
date: 2019-11-25T19:59:53+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-2625

---

It has been found, that libxdmcp, an X11 Display Manager Control Protocol library, uses weak entropy to generate keys.
Using arc4random_buf() from libbsd should avoid this flaw.
