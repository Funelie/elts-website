---
title: "ELA-194-1 nss security update"
package: nss
version: 2:3.26-1+debu7u9
distribution: "Debian 7 Wheezy"
description: "out-of-bounds write"
date: 2019-11-25T18:29:59-05:00
draft: false
type: updates
cvelist:
  - CVE-2019-11745

---

A vulnerability has been discovered in nss, the Mozilla Network Security
Service library.  An out-of-bounds write can occur when passing an
output buffer smaller than the block size to NSC_EncryptUpdate.
