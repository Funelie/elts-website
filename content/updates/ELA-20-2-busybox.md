---
title: "ELA-20-2 busybox regression update"
package: busybox
version: 1:1.20.0-7+deb7u2
distribution: "Debian 7 Wheezy"
description: "regression update"
date: 2018-08-08T11:14:39+02:00
draft: false
type: updates
cvelist:
  - CVE-2011-5325
  - CVE-2015-9261
---

The security update of busybox announced as ELA-20-1 introduced a regression
due to an incomplete fix for CVE-2015-9261. It was no longer possible to
decompress gzip archives which exceeded a certain file size.

It was also found that the patch to fix CVE-2011-5325, a symlinking attack, was
too strict in case of cpio archives. This update restores the old behavior.

