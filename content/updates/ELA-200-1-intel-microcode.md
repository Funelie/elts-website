---
title: "ELA-200-1 intel-microcode security update"
package: intel-microcode
version: 3.20191112.1~deb7u1
distribution: "Debian 7 Wheezy"
description: "firmware update"
date: 2019-12-11T01:23:31+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-11135
  - CVE-2019-11139

---

This update ships updated CPU microcode for some types of Intel CPUs. In
particular it provides mitigations for the TAA (TSX Asynchronous Abort)
vulnerability. For affected CPUs, to fully mitigate the vulnerability it
is also necessary to update the Linux kernel packages as released in ELA-190-1.
