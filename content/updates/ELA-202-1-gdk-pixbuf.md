---
title: "ELA-202-1 gdk-pixbuf security update"
package: gdk-pixbuf
version: 2.26.1-1+deb7u9
distribution: "Debian 7 Wheezy"
description: "serveral underflow/overflow issues"
date: 2019-12-19T19:27:28+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-6352
  - CVE-2017-2870
  - CVE-2017-6312
  - CVE-2017-6313
  - CVE-2017-6314

---

Several issues in gdk-pixbuf, a library to handle pixbuf, have been found.

CVE-2016-6352
     fix for denial of service (out-of-bounds write and crash) via
     crafted dimensions in an ICO file

CVE-2017-2870
     Fix for an exploitable integer overflow vulnerability in the
     tiff_image_parse functionality. When software is compiled with
     clang, A specially crafted tiff file can cause a heap-overflow
     resulting in remote code execution. Debian package is compiled
     with gcc and is not affected, but probably some downstream is.

CVE-2017-6312
     Fix for an integer overflow in io-ico.c that allows attackers
     to cause a denial of service (segmentation fault and application
     crash) via a crafted image

CVE-2017-6313
     Fix for an integer underflow in the load_resources function in
     io-icns.c that allows attackers to cause a denial of service
     (out-of-bounds read and program crash) via a crafted image entry
     size in an ICO file

CVE-2017-6314
     Fix for an infinite loop in the make_available_at_least function
     in io-tiff.c that allows attackers to cause a denial of service
     via a large TIFF file.

