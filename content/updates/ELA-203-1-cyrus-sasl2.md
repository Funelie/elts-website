---
title: "ELA-203-1 cyrus-sasl2 security update"
package: cyrus-sasl2
version: 2.1.25.dfsg1-6+deb7u2
distribution: "Debian 7 Wheezy"
description: "fix off-by-one error affecting OpenLDAP"
date: 2019-12-20T14:28:56+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-19906

---

There has been an out-of-bounds write in Cyrus SASL leading to
unauthenticated remote denial-of-service in OpenLDAP via a malformed LDAP
packet. The OpenLDAP crash was ultimately caused by an off-by-one error
in _sasl_add_string in common.c in cyrus-sasl.
