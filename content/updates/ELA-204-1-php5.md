---
title: "ELA-204-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u26
distribution: "Debian 7 Wheezy"
description: "issues in several modules"
date: 2019-12-30T16:47:34+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-11045
  - CVE-2019-11046
  - CVE-2019-11047
  - CVE-2019-11050

---

Several security bugs have been identified and fixed in php5, a server-side, HTML-embedded scripting language.
The affected components include the exif module and handling of filenames with \0 embedded.

