---
title: "ELA-205-1 git security update"
package: git
version: 1:1.7.10.4-1+wheezy8
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2020-01-07T00:06:07-05:00
draft: false
type: updates
cvelist:
  - CVE-2019-1348
  - CVE-2019-1349
  - CVE-2019-1387

---

Several vulnerabilities have been discovered in git, a fast, scalable,
distributed revision control system.

CVE-2019-1348

    It was reported that the --export-marks option of git fast-import is
    exposed also via the in-stream command feature export-marks=...,
    allowing to overwrite arbitrary paths.

CVE-2019-1387

    It was discovered that submodule names are not validated strictly
    enough, allowing very targeted attacks via remote code execution
    when performing recursive clones.
