---
title: "ELA-206-1 apache-log4j1.2 security update"
package: apache-log4j1.2
version: 1.2.16-3+deb7u1
distribution: "Debian 7 Wheezy"
description: "remote code execution"
date: 2020-01-11T18:26:18+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-17571

---

Included in Log4j 1.2, a logging library for Java, is a SocketServer class that
is vulnerable to deserialization of untrusted data which can be exploited to
remotely execute arbitrary code when combined with a deserialization gadget
when listening to untrusted network traffic for log data.
