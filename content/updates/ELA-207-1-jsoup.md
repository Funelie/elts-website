---
title: "ELA-207-1 jsoup security update"
package: jsoup
version: 1.6.2-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "cross-site scripting (XSS) vulnerability"
date: 2020-01-26T19:51:42+01:00
draft: false
type: updates
cvelist:
  - CVE-2015-6748

---

An issue has been found in jsoup, a Java HTML parser that makes sense of real-world HTML soup.
Due to bad handling of missing '>' at EOF a cross-site scripting (XSS) vulnerability could appear.
