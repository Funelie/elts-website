---
title: "ELA-208-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u23
distribution: "Debian 7 Wheezy"
description: "local privilege escalation"
date: 2020-02-04T00:07:55+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-12418
  - CVE-2019-17563

---

Two security vulnerabilities have been found in the Tomcat
servlet and JSP engine.

CVE-2019-12418

    When Apache Tomcat is configured with the JMX Remote Lifecycle Listener, a
    local attacker without access to the Tomcat process or configuration files is
    able to manipulate the RMI registry to perform a man-in-the-middle attack to
    capture user names and passwords used to access the JMX interface. The attacker
    can then use these credentials to access the JMX interface and gain complete
    control over the Tomcat instance.

CVE-2019-17563

    When using FORM authentication with Apache Tomcat there was a narrow window
    where an attacker could perform a session fixation attack. The window was
    considered too narrow for an exploit to be practical but, erring on the side of
    caution, this issue has been treated as a security vulnerability.
