---
title: "ELA-209-1 linux security update"
package: linux
version: 3.16.81-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "Linux Kernel update"
date: 2020-01-27T00:07:18+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-2215
  - CVE-2019-10220
  - CVE-2019-14895
  - CVE-2019-14896
  - CVE-2019-14897
  - CVE-2019-14901
  - CVE-2019-15098
  - CVE-2019-15217
  - CVE-2019-15291
  - CVE-2019-15505
  - CVE-2019-16746
  - CVE-2019-17052
  - CVE-2019-17053
  - CVE-2019-17054
  - CVE-2019-17055
  - CVE-2019-17056
  - CVE-2019-17133
  - CVE-2019-17666
  - CVE-2019-19051
  - CVE-2019-19052
  - CVE-2019-19056
  - CVE-2019-19057
  - CVE-2019-19062
  - CVE-2019-19066
  - CVE-2019-19227
  - CVE-2019-19332
  - CVE-2019-19523
  - CVE-2019-19524
  - CVE-2019-19527
  - CVE-2019-19530
  - CVE-2019-19531
  - CVE-2019-19532
  - CVE-2019-19533
  - CVE-2019-19534
  - CVE-2019-19536
  - CVE-2019-19537
  - CVE-2019-19767
  - CVE-2019-19922
  - CVE-2019-19947
  - CVE-2019-19965
  - CVE-2019-19966

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.

CVE-2019-2215

    The syzkaller tool discovered a use-after-free vulnerability in
    the Android binder driver.  A local user on a system with this
    driver enabled could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.
    However, this driver is not enabled on Debian packaged kernels.

CVE-2019-10220

    Various developers and researchers found that if a crafted file-
    system or malicious file server presented a directory with
    filenames including a '/' character, this could confuse and
    possibly defeat security checks in applications that read the
    directory.

    The kernel will now return an error when reading such a directory,
    rather than passing the invalid filenames on to user-space.

CVE-2019-14895, CVE-2019-14901

    ADLab of Venustech discovered potential heap buffer overflows in
    the mwifiex wifi driver.  On systems using this driver, a
    malicious Wireless Access Point or adhoc/P2P peer could use these
    to cause a denial of service (memory corruption or crash) or
    possibly for remote code execution.

CVE-2019-14896, CVE-2019-14897

    ADLab of Venustech discovered potential heap and stack buffer
    overflows in the libertas wifi driver.  On systems using this
    driver, a malicious Wireless Access Point or adhoc/P2P peer could
    use these to cause a denial of service (memory corruption or
    crash) or possibly for remote code execution.

CVE-2019-15098

    Hui Peng and Mathias Payer reported that the ath6kl wifi driver
    did not properly validate USB descriptors, which could lead to a
    null pointer derefernce.  An attacker able to add USB devices
    could use this to cause a denial of service (BUG/oops).

CVE-2019-15217

    The syzkaller tool discovered that the zr364xx mdia driver did not
    correctly handle devices without a product name string, which
    could lead to a null pointer dereference.  An attacker able to add
    USB devices could use this to cause a denial of service
    (BUG/oops).

CVE-2019-15291

    The syzkaller tool discovered that the b2c2-flexcop-usb media
    driver did not properly validate USB descriptors, which could lead
    to a null pointer dereference.  An attacker able to add USB
    devices could use this to cause a denial of service (BUG/oops).

CVE-2019-15505

    The syzkaller tool discovered that the technisat-usb2 media driver
    did not properly validate incoming IR packets, which could lead to
    a heap buffer over-read.  An attacker able to add USB devices
    could use this to cause a denial of service (BUG/oops) or to read
    sensitive information from kernel memory.

CVE-2019-16746

    It was discovered that the wifi stack did not validate the content
    of beacon heads provided by user-space for use on a wifi interface
    in Access Point mode, which could lead to a heap buffer overflow.
    A local user permitted to configure a wifi interface could use
    this to cause a denial of service (memory corruption or crash) or
    possibly for privilege escalation.

CVE-2019-17052, CVE-2019-17053, CVE-2019-17054, CVE-2019-17055,
CVE-2019-17056

    Ori Nimron reported that various network protocol implementations
    - AX.25, IEEE 802.15.4, Appletalk, ISDN, and NFC - allowed all
    users to create raw sockets.  A local user could use this to send
    arbitrary packets on networks using those protocols.

CVE-2019-17133

    Nicholas Waisman reported that the wifi stack did not valdiate
    received SSID information before copying it, which could lead to a
    buffer overflow if it is not validated by the driver or firmware.
    A malicious Wireless Access Point might be able to use this to
    cause a denial of service (memory corruption or crash) or for
    remote code execution.

CVE-2019-17666

    Nicholas Waisman reported that the rtlwifi wifi drivers did not
    properly validate received P2P information, leading to a buffer
    overflow.  A malicious P2P peer could use this to cause a denial
    of service (memory corruption or crash) or for remote code
    execution.

CVE-2019-19051

    Navid Emamdoost discovered a potential memory leak in the i2400m
    wimax driver if the software rfkill operation fails.  The security
    impact of this is unclear.

CVE-2019-19052

    Navid Emamdoost discovered a potential memory leak in the gs_usb
    CAN driver if the open (interface-up) operation fails.  The
    security impact of this is unclear.

CVE-2019-19056, CVE-2019-19057

    Navid Emamdoost discovered potential memory leaks in the mwifiex
    wifi driver if the probe operation fails.  The security impact of
    this is unclear.

CVE-2019-19062

    Navid Emamdoost discovered a potential memory leak in the AF_ALG
    subsystem if the CRYPTO_MSG_GETALG operation fails.  A local user
    could possibly use this to cause a denial of service (memory
    exhaustion).

CVE-2019-19066

    Navid Emamdoost discovered a potential memory leak in the bfa SCSI
    driver if the get_fc_host_stats operation fails.  The security
    impact of this is unclear.

CVE-2019-19227

    Dan Carpenter reported missing error checks in the Appletalk
    protocol implementation that could lead to a null pointer
    dereference.  The security impact of this is unclear.

CVE-2019-19332

    The syzkaller tool discovered a missing bounds check in the KVM
    implementation for x86, which could lead to a heap buffer overflow.
    A local user permitted to use KVM could use this to cause a denial
    of service (memory corruption or crash) or possibly for privilege
    escalation.

CVE-2019-19523

    The syzkaller tool discovered a use-after-free bug in the adutux
    USB driver.  An attacker able to add and remove USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.

CVE-2019-19524

    The syzkaller tool discovered a race condition in the ff-memless
    library used by input drivers.  An attacker able to add and remove
    USB devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.

CVE-2019-19527

    The syzkaller tool discovered that the hiddev driver did not
    correctly handle races between a task opening the device and
    disconnection of the underlying hardware.  A local user permitted
    to access hiddev devices, and able to add and remove USB devices,
    could use this to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.

CVE-2019-19530

    The syzkaller tool discovered a potential use-after-free in the
    cdc-acm network driver.  An attacker able to add USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.

CVE-2019-19531

    The syzkaller tool discovered a use-after-free bug in the yurex
    USB driver.  An attacker able to add and remove USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.

CVE-2019-19532

    The syzkaller tool discovered a potential heap buffer overflow in
    the hid-gaff input driver, which was also found to exist in many
    other input drivers.  An attacker able to add USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.

CVE-2019-19533

    The syzkaller tool discovered that the ttusb-dec media driver was
    missing initialisation of a structure, which could leak sensitive
    information from kernel memory.

CVE-2019-19534, CVE-2019-19536

    The syzkaller tool discovered that the peak_usb CAN driver was
    missing initialisation of some structures, which could leak
    sensitive information from kernel memory.

CVE-2019-19537

    The syzkaller tool discovered race conditions in the USB stack,
    involving character device registration.  An attacker able to add
    USB devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.

CVE-2019-19767

    The syzkaller tool discovered that crafted ext4 volumes could
    trigger a buffer overflow in the ext4 filesystem driver.  An
    attacker able to mount such a volume could use this to cause a
    denial of service (memory corruption or crash) or possibly for
    privilege escalation.

CVE-2019-19922

    It was discovered that a change in Linux 3.16.61, "sched/fair: Fix
    bandwidth timer clock drift condition", could lead to tasks being
    throttled before using their full quota of CPU time.  A local
    user could use this bug to slow down other users' tasks.  This
    change has been reverted.

CVE-2019-19947

    It was discovered that the kvaser_usb CAN driver was missing
    initialisation of some structures, which could leak sensitive
    information from kernel memory.

CVE-2019-19965

    Gao Chuan reported a race condition in the libsas library used by
    SCSI host drivers, which could lead to a null pointer dereference.
    An attacker able to add and remove SCSI devices could use this to
    cause a denial of service (BUG/oops).

CVE-2019-19966

    The syzkaller tool discovered a missing error check in the cpia2
    media driver, which could lead to a use-after-free.  An attacker
    able to add USB devices could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.

