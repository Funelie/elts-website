---
title: "ELA-21-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb7u6
distribution: "Debian 7 Wheezy"
description: "cache timing side channel attack"
date: 2018-07-22T21:31:17+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-0737
---

Alejandro Cabrera Aldaya, Billy Brumley, Cesar Pereida Garcia and Luis Manuel
Alvarez Tapia discovered that the OpenSSL RSA Key generation algorithm has been
shown to be vulnerable to a cache timing side channel attack. An attacker with
sufficient access to mount cache timing attacks during the RSA key generation
process could recover the private key.
