---
title: "ELA-210-1 unzip security update"
package: unzip
version: 6.0-8+deb7u9
distribution: "Debian 7 Wheezy"
description: "buffer overflow"
date: 2020-01-28T20:03:13+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-1000035

---

An issue has been found in unzip, a de-archiver for .zip files.
While processing a password protected archive, a heap-based buffer overflow could happen, that allows an attacker to perform a denial of service or to possibly achieve code execution.

