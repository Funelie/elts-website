---
title: "ELA-212-1 zlib security update"
package: zlib
version: 1.2.7.dfsg-13+deb7u1
distribution: "Debian 7 Wheezy"
description: "bad CRC calculation, integer or pointer arithmetic"
date: 2020-01-30T14:14:19+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-9840
  - CVE-2016-9841
  - CVE-2016-9842
  - CVE-2016-9843

---

Several issues have been found in zlib, a compression library.
They are basically about improper big-endian CRC calculation, improper
left shift of negative integers and improper pointer arithmetic.

