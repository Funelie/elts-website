---
title: "ELA-215-2 openjdk-7 regression update"
package: openjdk-7
version: 7u251-2.6.21-1~deb7u2
distribution: "Debian 7 Wheezy"
description: "SunEC regression update"
date: 2020-03-05T20:21:14+01:00
draft: false
type: updates
cvelist:

---

The latest security update of openjdk-7 caused a regression by disabling
the build of the SunEC provider. This has been fixed in this version by
re-enabling the build of SunEC.
