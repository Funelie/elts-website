---
title: "ELA-216-1 php5 security update"
package: php5
version: 
distribution: "Debian 7 Wheezy"
description: "information disclosure or crash by crafted data"
date: 2020-02-29T19:47:53+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-7059
  - CVE-2020-7060

---

Two issues have been found in php5, a server-side, HTML-embedded scripting language. Both issues are related to crafted data that could lead to reading after an allocated buffer and result in information disclosure or crash. 
