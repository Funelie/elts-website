---
title: "ELA-217-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u24
distribution: "Debian 7 Wheezy"
description: "HTTP Request Smuggling"
date: 2020-03-09T11:16:49+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-1935

---

The HTTP header parsing code used an approach to end-of-line (EOL)
parsing that allowed some invalid HTTP headers to be parsed as
valid. This led to a possibility of HTTP Request Smuggling if Tomcat
was located behind a reverse proxy that incorrectly handled the
invalid Transfer-Encoding header in a particular manner.
