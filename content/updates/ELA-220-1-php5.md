---
title: "ELA-220-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u28
distribution: "Debian 7 Wheezy"
description: "fix for null pointer dereference and lax access permissions in tar file"
date: 2020-03-27T17:01:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-7062
  - CVE-2020-7063

---

Two security issues have been identified and fixed in php5, a
server-side, HTML-embedded scripting language.

CVE-2020-7062 is about a possible null pointer derefernce, which would
likely lead to a crash, during a failed upload with progress tracking.
CVE-2020-7063 is about wrong file permissions of files added to tar with
Phar::buildFromIterator when extracting them again.

