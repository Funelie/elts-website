---
title: "ELA-221-1 nss security update"
package: nss
version: 2:3.26-1+debu7u11
distribution: "Debian 7 Wheezy"
description: "heap buffer overflow"
date: 2020-03-30T16:13:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-17006

---

An issue has been found in package nss, which consists of a couple of Network Security Service libraries.
Certain cryptographic primitives in nss did not check the length of the input text. This could result in a potential heap-based buffer overflow.

