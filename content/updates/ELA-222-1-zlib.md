---
title: "ELA-222-1 zlib security update"
package: zlib
version: 1:1.2.7.dfsg-13+deb7u2
distribution: "Debian 7 Wheezy"
description: "directory traversal vulnerability"
date: 2020-04-14T22:09:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2014-9485

---

Jakub Wilk discovered that miniunzip in zlib-bin was affected by a directory
traversal security vulnerability. An attacker could use this flaw to extract
the contents of a specially crafted zip file to arbitrary locations.
