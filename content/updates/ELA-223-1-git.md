---
title: "ELA-223-1 git security update"
package: git
version: 1:1.7.10.4-1+wheezy9
distribution: "Debian 7 Wheezy"
description: "information disclosure"
date: 2020-04-15T16:10:14Z
draft: false
type: updates
cvelist:
  - CVE-2020-5260

---

Felix Wilhelm of Google Project Zero discovered a flaw in git, a fast,
scalable, distributed revision control system. With a crafted URL that
contains a newline, the credential helper machinery can be fooled to
return credential information for a wrong host.
