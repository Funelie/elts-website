---
title: "ELA-226-1 openjdk-7 security update"
package: openjdk-7
version: 7u261-2.6.22-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2020-04-29T12:51:00Z
draft: false
type: updates
cvelist:
  - CVE-2020-2756
  - CVE-2020-2757
  - CVE-2020-2773
  - CVE-2020-2781
  - CVE-2020-2800
  - CVE-2020-2803
  - CVE-2020-2805
  - CVE-2020-2830
---

Several vulnerabilities have been discovered in the OpenJDK Java
runtime, resulting in denial of service, insecure TLS handshakes, bypass
of sandbox restrictions or HTTP response splitting attacks.
