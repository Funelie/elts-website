---
title: "ELA-229-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u25
distribution: "Debian 7 Wheezy"
description: "Remote code execution vulnerability"
date: 2020-05-24T16:21:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-9484
---

It was discovered that there was a potential remote code execution via
deserialization of local files on the filesystem within `tomcat7`, a server for
HTTP and Java "servlets".
