---
title: "ELA-23 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u12
distribution: "Debian 7 Wheezy"
description: "fix problems in different dissectors"
date: 2018-07-29T16:17:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-14339
  - CVE-2018-14340
  - CVE-2018-14341
  - CVE-2018-14342
  - CVE-2018-14343
  - CVE-2018-14368
  - CVE-2018-14369
---

Due to several flaws different dissectors could go in infinite loop or could be crashed by malicious packets.
