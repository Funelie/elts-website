---
title: "ELA-231-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u30
distribution: "Debian 7 Wheezy"
description: "avoid exhausted disk space"
date: 2020-05-31T11:27:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-11048

---

When using overly long filenames or field names, a memory limit could
be hit which results in stopping the upload but not cleaning up behind.
This could lead to exhausted disk space on the server.

