---
title: "ELA-234-1 mysql-connector-java security update"
package: mysql-connector-java
version: 5.1.49-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2020-06-08T10:16:46+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-2875
  - CVE-2020-2933
  - CVE-2020-2934

---

Several issues were discovered in mysql-connector-java that allow
attackers to update, insert or delete access to some of MySQL
Connectors accessible data, unauthorized read access to a subset of
the data, and partial denial of service.
