---
title: "ELA-236-1 wheezy-elts end of life"
package: wheezy-elts
version: 
distribution: "Debian 7 Wheezy"
description: "Wheezy end of life"
date: 2020-07-02T11:47:50+02:00
draft: false
type: updates
cvelist:

---

The Extended Long Term Support (ELTS) Team hereby announces that Debian 7
"Wheezy" support has reached its end-of-life on June 30, 2020,
seven years after its initial release on May 4, 2013.

We highly encourage any remaining Wheezy users to upgrade to a supported
Debian version. The ELTS Team will continue to provide support for
Debian 8 "Jessie", while Debian 9 "Stretch" and Debian 10 "Buster" are
still supported by the LTS and Debian security teams, respectively.

Freexian and the ELTS Team would like to thank all the users and sponsors
that made this initiative possible, and encourage any interested parties to
contribute to the extended support of Jessie.
