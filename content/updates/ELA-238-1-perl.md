---
title: "ELA-238-1 perl security update"
package: perl
version: 5.20.2-3+deb8u13
distribution: "Debian 8 jessie"
description: "buffer overflow and code injection via crafted regexes"
date: 2020-07-03T12:41:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-10543
  - CVE-2020-10878
  - CVE-2020-12723

---

Several vulnerabilities where found in Perl's regular expression compiler.
An application that compiles untrusted regular expressions could be exploited
to cause denial of service or code injection.

It is discouraged to allow untrusted regular expressions to be compiled by Perl.
