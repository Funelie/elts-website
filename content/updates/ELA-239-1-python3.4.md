---
title: "ELA-239-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u8
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-07-04T14:34:03+02:00
draft: false
type: updates
cvelist:
  - CVE-2013-1753
  - CVE-2016-1000110
  - CVE-2019-16935
  - CVE-2019-18348
  - CVE-2020-8492
  - CVE-2020-14422

---

Several issues were discovered in Python 3.4, an interactive
high-level object-oriented language, that allow an attacker to cause
denial of service, trafic redirection, header injection and cross-site
scripting.

* CVE-2013-1753

    The gzip_decode function in the xmlrpc client library allows
    remote attackers to cause a denial of service (memory consumption)
    via a crafted HTTP request.

* CVE-2016-1000110

    The CGIHandler class does not protect against the HTTP_PROXY
    variable name clash in a CGI script, which could allow a remote
    attacker to redirect HTTP requests.

* CVE-2019-16935

    The documentation XML-RPC server has XSS via the server_title
    field. This occurs in Lib/xmlrpc/server.py. If set_server_title is
    called with untrusted input, arbitrary JavaScript can be delivered
    to clients that visit the http URL for this server.

* CVE-2019-18348

    In urllib2, CRLF injection is possible if the attacker controls a
    url parameter, as demonstrated by the first argument to
    urllib.request.urlopen with \r\n (specifically in the host
    component of a URL) followed by an HTTP header.

* CVE-2020-8492

    Python allows an HTTP server to conduct Regular Expression Denial
    of Service (ReDoS) attacks against a client because of
    urllib.request.AbstractBasicAuthHandler catastrophic backtracking.

* CVE-2020-14422

    Lib/ipaddress.py improperly computes hash values in the
    IPv4Interface and IPv6Interface classes, which might allow a
    remote attacker to cause a denial of service if an application is
    affected by the performance of a dictionary containing
    IPv4Interface or IPv6Interface objects, and this attacker can
    cause many dictionary entries to be created.
