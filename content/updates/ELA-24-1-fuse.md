---
title: "ELA-24 fuse security update"
package: fuse
version: 2.9.0-2+deb7u3
distribution: "Debian 7 Wheezy"
description: "fix restriction bypass"
date: 2018-07-31T16:50:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10906
---

This upload fixes a restriction bypass of the "allow_other" option when SELinux is active.
