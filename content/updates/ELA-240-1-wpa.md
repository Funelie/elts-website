---
title: "ELA-240-1 wpa security update"
package: wpa
version: 2.3-1+deb8u11
distribution: "Debian 8 jessie"
description: "the CallStranger issue in wpa"
date: 2020-07-13T14:57:10+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-12695

---

The Open Connectivity Foundation UPnP specification before 2020-04-17
does not forbid the acceptance of a subscription request with a delivery
URL on a different network segment than the fully qualified
event-subscription URL, aka the CallStranger issue.
