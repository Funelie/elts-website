---
title: "ELA-241-1 openjpeg2 security update"
package: openjpeg2
version: 2.1.0-2+deb8u11
distribution: "Debian 8 jessie"
description: "use-after-free and double-free in openjpeg2"
date: 2020-07-13T17:24:41+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-15389

---

jp2/opj_decompress.c in OpenJPEG through 2.3.1 has a use-after-free that can
be triggered if there is a mix of valid and invalid files in a directory
operated on by the decompressor.  
Triggering a double-free may also be possible. This is related to calling
opj_image_destroy twice.

