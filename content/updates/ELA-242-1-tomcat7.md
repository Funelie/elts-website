---
title: "ELA-242-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.100-1+deb8u2
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-07-15T20:12:19+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-13935

---

The payload length in a WebSocket frame was not correctly validated. Invalid
payload lengths could trigger an infinite loop. Multiple requests with invalid
payload lengths could lead to a denial of service.
