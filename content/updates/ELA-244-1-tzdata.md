---
title: "ELA-244-1 tzdata new upstream version"
package: tzdata
version: 2020a-0+deb8u1
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2020-07-20T09:44:18+02:00
draft: false
type: updates
cvelist:

---

This update brings the timezone changes from the upstream 2020a release.
