---
title: "ELA-245-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.75-2+2020a
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2020-07-20T09:47:25+02:00
draft: false
type: updates
cvelist:

---

This update brings the Olson database changes from the 2020a version to
the Perl bindings.
