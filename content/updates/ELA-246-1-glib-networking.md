---
title: "ELA-246-1 glib-networking security update"
package: glib-networking
version: 2.42.0-2+deb8u1
distribution: "Debian 8 jessie"
description: "insuficient certificate validation"
date: 2020-07-20T09:53:05+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-13645

---

GLib networking, the network extensions for GLib, failed to verify
certificates' hostnames if the application didn't specify the server
identity.

Applications must provide a server identity or GLib will fail with
G_TLS_CERTIFICATE_BAD_IDENTITY.
