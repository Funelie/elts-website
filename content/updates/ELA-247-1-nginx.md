---
title: "ELA-247-1 nginx security update"
package: nginx
version: 1.6.2-5+deb8u7
distribution: "Debian 8 jessie"
description: "HTTP request smuggling"
date: 2020-07-20T15:25:39+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-20372
  - CVE-2020-11724

---

Two HTTP request smuggling issues were discovered in nginx, a
high-performance web and reverse proxy server, as well as in its
ngx_lua plugin.