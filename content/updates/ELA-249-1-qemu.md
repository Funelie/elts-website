---
title: "ELA-249-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u16
distribution: "Debian 8 jessie"
description: "NULL pointer dereference & stack overflow in QEMU"
date: 2020-07-26T00:27:11+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-13659
  - CVE-2020-15863

---

There were two following CVE(s) reported against `src:qemu`.

- `CVE-2020-13659`: `address_space_map` in `exec.c` in QEMU 4.2.0
  can trigger a NULL pointer dereference related to `BounceBuffer`.

- `CVE-2020-15863`: stack-based overflow in `xgmac_enet_send()` in
  `hw/net/xgmac.c`.
