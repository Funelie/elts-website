---
title: "ELA-251-1 curl security update"
package: curl
version: 7.38.0-4+deb8u17
distribution: "Debian 8 jessie"
description: "possible overwrite of local files"
date: 2020-07-27T22:36:11+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-8177

---

A vulnerability was found in curl, a command line tool for transferring
data with URL syntax.

When using when using -J (--remote-header-name) and -i (--include) in
the same command line, a malicious server could force curl to overwrite
the contents of local files with incoming HTTP headers.
