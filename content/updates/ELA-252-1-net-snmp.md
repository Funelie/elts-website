---
title: "ELA-252-1 net-snmp security update"
package: net-snmp
version: 5.7.2.1+dfsg-1+deb8u3
distribution: "Debian 8 Jessie"
description: "Privilege escalation vulnerability"
date: 2020-07-29T12:39:32+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-15862

---

A privilege escalation vulnerability was discovered in
[Net-SNMP](http://net-snmp.sourceforge.net/), a set of tools for collecting and
organising information about devices on computer networks.

Upstream notes that:

* It is still possible to enable this MIB via the `--with-mib-modules`
  configure option.

* Another MIB that provides similar functionality, namely
  `ucd-snmp/extensible`, is disabled by default.

* The security risk of `ucd-snmp/pass` and `ucd-snmp/pass_persist` is lower
  since these modules only introduce a security risk if the invoked scripts are
  exploitable.
