---
title: "ELA-253-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u20
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-07-30T15:18:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-12805
  - CVE-2017-17681
  - CVE-2017-18252
  - CVE-2018-7443
  - CVE-2018-8804
  - CVE-2018-8960
  - CVE-2018-9133
  - CVE-2018-10177
  - CVE-2018-18024
  - CVE-2018-20467
  - CVE-2019-10131
  - CVE-2019-11472
  - CVE-2019-12977
  - CVE-2019-12978
  - CVE-2019-12979
  - CVE-2019-13300
  - CVE-2019-13307
  - CVE-2019-13454

---

Multiple security vulnerabilities were fixed in Imagemagick. Various memory
handling problems and cases of missing or incomplete input sanitising may
result in denial of service and memory or CPU exhaustion.
