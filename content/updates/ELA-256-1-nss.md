---
title: "ELA-256-1 nss security update"
package: nss
version: 2:3.26-1+debu8u12
distribution: "Debian 8 jessie"
description: "ECDSA timing and side channel attacks"
date: 2020-08-02T23:54:40Z
draft: false
type: updates
cvelist:
  - CVE-2020-6829
  - CVE-2020-12400
  - CVE-2020-12401
---

Multiple security vulnerabilities were fixed in nss, the Network Security
Services library.  The ECDSA signature generation in P-384 and P-521 was
found to be vulnerable to a side channel attack in the modular inversion
function implementation.  The ECDSA implementation was also found to be
vulnerable to a timing attack mitigation bypass.
