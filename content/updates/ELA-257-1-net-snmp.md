---
title: "ELA-257-1 net-snmp security update"
package: net-snmp
version: 5.7.2.1+dfsg-1+deb8u4
distribution: "Debian 8 Jessie"
description: "privilege escalation vulnerability"
date: 2020-08-04T15:52:07+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-15861
  - CVE-2020-15862
---

A privilege escalation vulnerability was discovered in
[Net-SNMP](http://www.net-snmp.org/) due to incorrect symlink handling
(`CVE-2020-15861`).

This security update also applies an upstream fix to their previous handling of
`CVE-2020-15862` as part of
[ELA-252-1](https://deb.freexian.com/extended-lts/updates/ela-252-1-net-snmp/).
