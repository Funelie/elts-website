---
title: "ELA-258-1 gupnp security update"
package: gupnp
version: 0.20.12-1+deb8u1
distribution: "Debian 8 jessie"
description: "DDoS and data exfiltration in UPnP"
date: 2020-08-06T18:38:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-12695

---

Yunus Çadırcı found an issue in the SUBSCRIBE method of UPnP, a
network protocol for devices to automatically discover and communicate
with each other. Insufficient checks on this method allowed attackers
to use vulnerable UPnP services for DoS attacks or possibly to bypass
firewalls.
