---
title: "ELA-26 libxcursor security update"
package: libxcursor
version: 1:1.1.13-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "insufficient memory allocation"
date: 2018-08-05T17:02:58+02:00
draft: false
type: updates
cvelist:
  - CVE-2015-9262
---

Insufficient memory allocation for terminating null character in string.
