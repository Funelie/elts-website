---
title: "ELA-264-1 openjdk-8 new package"
package: openjdk-8
version: 8u265-b01-0+deb8u1
distribution: "Debian 8 jessie"
description: "OpenJDK 8 backported to ELTS"
date: 2020-08-21T19:25:45+02:00
draft: false
type: updates
cvelist:

---

This update brings OpenJDK 8 to Debian 8 jessie. This will become the
default Java version in a separate update.
