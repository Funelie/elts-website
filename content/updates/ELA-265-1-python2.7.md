---
title: "ELA-265-1 python2.7 security update"
package: python2.7
version: 2.7.9-2-ds1+deb8u6
distribution: "Debian 8 jessie"
description: "fix for infinite loop"
date: 2020-08-22T15:54:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-20907

---

An issue has been found in python2.7, an interactive high-level object-oriented language.

Opening a crafted tar file could result in an infinite loop due to missing header validation.
