---
title: "ELA-266-1 software-properties security update"
package: software-properties
version: 0.92.25debian1+deb8u1
distribution: "Debian 8 jessie"
description: "ansi escape sequence injection"
date: 2020-08-22T18:13:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-15709

---

Jason A. Donenfeld found an ansi escape sequence injection into
software-properties, a manager for apt repository sources. An attacker
could manipulate the screen of a user prompted to install an
additional repository (PPA).
