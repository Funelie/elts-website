---
title: "ELA-267-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u3
distribution: "Debian 8 jessie"
description: "fix for privilege escalation"
date: 2020-08-26T15:15:43Z
draft: false
type: updates
cvelist:
  - CVE-2020-14350
---

Andres Freund found an issue in the PostgreSQL database system where an
uncontrolled search path could allow users to run arbitrary SQL functions
with elevated priviledges when a superuser runs certain `CREATE EXTENSION'
statements.
