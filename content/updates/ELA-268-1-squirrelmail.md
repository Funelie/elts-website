---
title: "ELA-268-1 squirrelmail security update"
package: squirrelmail
version: 2:1.4.23~svn20120406-2+deb8u5
distribution: "Debian 8 Jessie"
description: "unsafe serialisation vulnerabilities"
date: 2020-08-28T14:26:56+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-14932
  - CVE-2020-14933
---

Two unsafe serialisation vulnerabilities were discovered in the PHP-based
`squirrelmail` webmail client.

Unsafe data was accepted to the `mailto.php` script which opened an email
compose screen with the passed email address.
