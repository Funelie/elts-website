---
title: "ELA-269-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u17
distribution: "Debian 8 jessie"
description: "IP address spoofing"
date: 2020-08-30T20:43:07+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-11985

---

apache2 was vulnerable to IP address spoofing when proxying using mod_remoteip
and mod_rewrite.

For configurations using proxying with mod_remoteip and certain mod_rewrite rules,
an attacker could spoof their IP address for logging and PHP scripts. 
