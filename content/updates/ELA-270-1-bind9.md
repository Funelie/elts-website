---
title: "ELA-270-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u20
distribution: "Debian 8 jessie"
description: "denial of service with TSIG-signed requests"
date: 2020-08-30T17:31:03+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-8622

---

Crafted responses to TSIG-signed requests could lead to an assertion
failure, causing named, a Domain Name Server, to exit. This could be
done by malicious server operators or guessing attackers.
