---
title: "ELA-271-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u1
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-08-30T22:28:51+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-19132
  - CVE-2019-12519
  - CVE-2019-12520
  - CVE-2019-12521
  - CVE-2019-12523
  - CVE-2019-12524
  - CVE-2019-12525
  - CVE-2019-12526
  - CVE-2019-12528
  - CVE-2019-12529
  - CVE-2019-13345
  - CVE-2019-18676
  - CVE-2019-18677
  - CVE-2019-18678
  - CVE-2019-18679
  - CVE-2019-18860
  - CVE-2020-8449
  - CVE-2020-8450
  - CVE-2020-11945

---

It was found that Squid, a high-performance proxy caching server for
web clients, has been affected by multiple security vulnerabilities.
Due to incorrect input validation and URL request handling it was
possible to bypass access restrictions for restricted HTTP servers
and to cause a denial-of-service.
