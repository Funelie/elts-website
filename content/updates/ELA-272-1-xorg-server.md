---
title: "ELA-272-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u3
distribution: "Debian 8 jessie"
description: "fix for integer underflow, out-of-bounds read and memory leak"
date: 2020-08-30T23:43:08+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-14345
  - CVE-2020-14346
  - CVE-2020-14347
  - CVE-2020-14361
  - CVE-2020-14362

---

Basically all issues in xorg-server, the X server from xorg, are
out-of-bounds access or integer underflows in different request
handlers. One CVE is about a leak of uninitialize heap memory
to clients.
