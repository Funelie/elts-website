---
title: "ELA-273-1 nss security update"
package: nss
version: 2:3.26-1+debu8u13
distribution: "Debian 8 jessie"
description: "out-of-bounds read"
date: 2020-08-31T20:33:17+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-12403

---

The ChaCha20 symmetric key cipher algorithm did not correctly enforce the tag
length which may have led to an out-of-bounds read and a lack of
confidentiality.
