---
title: "ELA-274-1 uwsgi security update"
package: uwsgi
version: 2.0.7-1+deb8u3
distribution: "Debian 8 jessie"
description: "resource exhaustion & DOS"
date: 2020-09-03T04:09:15+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-11984

---

Apache HTTP Server versions before 2.4.32 uses src:uwsgi where a flaw
was discovered. The uwsgi protocol does not let us serialize more
than 16K of HTTP header leading to resource exhaustion and denial of
service.
