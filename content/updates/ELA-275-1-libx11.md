---
title: "ELA-275-1 libx11 security update"
package: libx11
version: 2:1.6.2-3+deb8u4
distribution: "Debian 8 jessie"
description: "double free"
date: 2020-09-03T04:43:06+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-14363

---

Jayden Rivers found an integer overflow in the init_om function of
libX11, the X11 client-side library, which could lead to a double
free.
