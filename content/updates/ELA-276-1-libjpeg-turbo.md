---
title: "ELA-276-1 libjpeg-turbo security update"
package: libjpeg-turbo
version: 1:1.3.1-12+deb8u3
distribution: "Debian 8 jessie"
description: "buffer over-read"
date: 2020-09-04T22:08:14+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-13790
  - CVE-2020-14152

---

Two security vulnerabilities were discovered in libjpeg-turbo, a library for
handling JPEG image files.

 CVE-2020-13790

    Heap-based buffer over-read via a PPM input file.

 CVE-2020-14152

    Improper handling of max_memory_to_use setting can lead to excessive memory
    consumption.

