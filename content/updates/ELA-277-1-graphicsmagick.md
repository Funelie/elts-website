---
title: "ELA-277-1 graphicsmagick security update"
package: graphicsmagick
version: 1.3.20-3+deb8u12
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-09-04T23:07:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-10800

---

When GraphicsMagick processes a MATLAB image, it can lead to a denial of
service, if the size specified for a MAT Object is larger than the actual
amount of data.
