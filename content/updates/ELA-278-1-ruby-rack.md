---
title: "ELA-278-1 ruby-rack security update"
package: ruby-rack
version: 1.5.2-3+deb8u4
distribution: "Debian 8 jessie"
description: "forge a secure or host-only cookie prefix"
date: 2020-09-05T12:30:32+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-8184

---

A reliance on cookies without validation/integrity check security
vulnerability exists in rack < 2.2.3, rack < 2.1.4 that makes it
is possible for an attacker to forge a secure or host-only cookie
prefix.
