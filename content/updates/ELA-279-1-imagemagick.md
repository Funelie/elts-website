---
title: "ELA-279-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u21
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2020-09-05T19:35:33+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-12806
  - CVE-2019-13308
  - CVE-2019-13391

---

Several security vulnerabilities have been addressed in imagemagick, an image processing
toolkit.

CVE-2017-12806

    A memory exhaustion vulnerability was found in the function format8BIM, which allows
    attackers to cause a denial of service.

CVE-2019-13308, CVE-2019-13391

    Heap-based buffer overflow in MagickCore/fourier.c in ComplexImages may
    cause a denial-of-service or other unspecified results.

