---
title: "ELA-28-1 tomcat-native security update"
package: tomcat-native
version: 1.1.24-1+deb7u2
distribution: "Debian 7 Wheezy"
description: "authentication bypass"
date: 2018-08-19T20:11:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-8019
  - CVE-2018-8020
---

When using an OCSP responder Tomcat Native did not correctly handle invalid
responses. This allowed for revoked client certificates to be incorrectly
identified. It was therefore possible for users to authenticate with revoked
certificates when using mutual TLS. Users not using OCSP checks are not
affected by this vulnerability.
