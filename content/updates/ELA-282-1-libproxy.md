---
title: "ELA-282-1 libproxy security update"
package: libproxy
version: 0.4.11-4+deb8u1
distribution: "Debian 8 Jessie"
description: "denial of service vulnerability"
date: 2020-09-12T10:48:26+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-25219
---

It was discovered that there was a denial of service attack in `libproxy`, a
library to make applications HTTP proxy aware. A remote server could cause an
infinite stack recursion.
