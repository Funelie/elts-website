---
title: "ELA-283-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u17
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-09-14T16:50:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-20382
  - CVE-2020-13253
  - CVE-2020-13754
  - CVE-2020-14364
  - CVE-2020-16092

---

Several vulnerabilities were discovered in QEMU, a fast processor
emulator (notably used in KVM and Xen HVM virtualization). An attacker
could trigger a denial-of-service (DoS) and possibly execute arbitrary
code with the privileges of the QEMU process on the host.

* CVE-2019-20382

    Memory leak in zrle_compress_data in ui/vnc-enc-zrle.c during a
    VNC disconnect operation because libz is misused, resulting in a
    situation where memory allocated in deflateInit2 is not freed in
    deflateEnd.

* CVE-2020-13253

    sd_wp_addr in hw/sd/sd.c uses an unvalidated address, which leads
    to an out-of-bounds read during sdhci_write() operations. A guest
    OS user can crash the QEMU process.

* CVE-2020-13754

    hw/pci/msix.c allows guest OS users to trigger an out-of-bounds
    access via a crafted address in an msi-x mmio operation.

* CVE-2020-14364

    An out-of-bounds read/write access flaw was found in the USB
    emulator. This issue occurs while processing USB packets from a
    guest when USBDevice 'setup_len' exceeds its 'data_buf[4096]' in
    the do_token_in, do_token_out routines. This flaw allows a guest
    user to crash the QEMU process, resulting in a denial of service,
    or the potential execution of arbitrary code with the privileges
    of the QEMU process on the host.

* CVE-2020-16092

    An assertion failure can occur in the network packet
    processing. This issue affects the vmxnet3 network devices. A
    malicious guest user/process could use this flaw to abort the QEMU
    process on the host, resulting in a denial of service condition in
    net_tx_pkt_add_raw_fragment in hw/net/net_tx_pkt.c.
