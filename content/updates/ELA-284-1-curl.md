---
title: "ELA-284-1 curl security update"
package: curl
version: 7.38.0-4+deb8u18
distribution: "Debian 8 jessie"
description: "multi API problem with connection"
date: 2020-09-27T23:25:10+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-8231

---

An issue has been found in curl, a command line tool for transferring data
with URL syntax.
In rare circumstances, when using the multi API of curl in combination
with CURLOPT_CONNECT_ONLY, the wrong connection  might be used when
transfering data later.

