---
title: "ELA-286-1 clamav security update"
package: clamav
version: 0.102.4+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2020-09-29T15:14:46+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-3350
  - CVE-2020-3481

---

Several vulnerabilities have been found in the ClamAV antivirus toolkit:

CVE-2020-3350

    A malicious user could trick clamscan, clamdscan or clamonacc into
    moving or removing a different file than intended when those are
    used with one of the --move or --remove options. This could be used
    to get rid of special system files.

CVE-2020-3481

    The EGG archive module was vulnerable to denial of service via NULL
    pointer dereference due to improper error handling. The official
    signature database avoided this problem because the signatures there
    avoided the use of the EGG archive parser.
