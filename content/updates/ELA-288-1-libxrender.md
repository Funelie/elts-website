---
title: "ELA-288-1 libxrender security update"
package: libxrender
version: 1:0.9.8-1+deb8u1
distribution: "Debian 8 jessie"
description: "improved handling of responses from the server"
date: 2020-09-30T15:42:23+02:00
draft: false
type: updates
cvelist:
  - CVE-2016-7949
  - CVE-2016-7950

---

Two issues have been found in libxrender, a X Rendering Extension client library. 

Tobias Stoeckmann from the OpenBSD project has discovered issues in the
way various X client libraries handle the responses they receive from
servers. Insufficient validation of data from the X server could cause
out of boundary memory writes in the libXrender library potentially
allowing the user to escalate their privileges.
