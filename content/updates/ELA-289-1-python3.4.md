---
title: "ELA-289-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u9
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-09-30T16:14:27+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-20907
  - CVE-2020-26116

---

Two issues have been found in Python 3.4, an interactive high-level
object-oriented language.

CVE-2019-20907
     Avoid infinite loop with crafted tar file by improving header validation.

CVE-2020-26116
     Avoid injection of HTTP headers via the HTTP method without rejecting newline characters.

