---
title: "ELA-289-2 python3.4 regression update"
package: python3.4
version: 3.4.2-1+deb8u10
distribution: "Debian 8 jessie"
description: "build again on armel"
date: 2020-10-27T22:56:51+01:00
draft: false
type: updates
cvelist:

---

In contrast to packages built for architectures amd64 and i386, which
are built by sbuild in native mode, the packages for architecture armel
are built in cross build mode.
The changes for version 3.4.2-1+deb8u9 did not consider this and only
the packages for amd64 and i386 could be successfully created, whereas
the build of the armel version failed.
This has been fixed with the current upload, which builds for every
architecture now.
