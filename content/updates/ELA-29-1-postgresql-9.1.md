---
title: "ELA-29-1 postgresql-9.1 security update"
package: postgresql-9.1
version: 9.1.24lts2-0+deb7u3
distribution: "Debian 7 Wheezy"
description: "privilege escalation,SQL injection"
date: 2018-08-26T23:28:28+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10915
---

Andrew Krasichkov discovered that an unprivileged user of dblink or
postgres_fdw could bypass the checks intended to prevent use of server-side
credentials, such as a ~/.pgpass file owned by the operating-system user
running the server. Servers allowing peer authentication on local connections
are particularly vulnerable. Other attacks such as SQL injection into a
postgres_fdw session are also possible. Attacking postgres_fdw in this way
requires the ability to create a foreign server object with selected connection
parameters, but any user with access to dblink could exploit the problem. In
general, an attacker with the ability to select the connection parameters for a
libpq-using application could cause mischief, though other plausible attack
scenarios are harder to think of.
