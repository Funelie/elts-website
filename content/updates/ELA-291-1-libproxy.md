---
title: "ELA-291-1 libproxy security update"
package: libproxy
version: 0.4.11-4+deb8u2
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2020-10-02T01:13:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-26154

---

Li Fei discovered a possible buffer overflow vulnerability in libroxy when a
server serving a PAC file sends more than 102400 bytes without a Content-Length
present.
