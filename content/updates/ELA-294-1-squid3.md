---
title: "ELA-294-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u2
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-10-08T23:55:42+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-15049
  - CVE-2020-15810
  - CVE-2020-15811
  - CVE-2020-24606

---

Several security vulnerabilities have been discovered in Squid, a high-
performance proxy caching server for web clients.

CVE-2020-15049

    An issue was discovered in http/ContentLengthInterpreter.cc in
    Squid. A Request Smuggling and Poisoning attack can succeed against
    the HTTP cache. The client sends an HTTP request with a Content-
    Length header containing "+\ "-" or an uncommon shell whitespace
    character prefix to the length field-value.
    This update also includes several other improvements to the
    HttpHeader parsing code.

CVE-2020-15810 and CVE-2020-15811

    Due to incorrect data validation, HTTP Request Smuggling attacks may
    succeed against HTTP and HTTPS traffic. This leads to cache
    poisoning and allows any client, including browser scripts, to
    bypass local security and poison the proxy cache and any downstream
    caches with content from an arbitrary source. When configured for
    relaxed header parsing (the default), Squid relays headers
    containing whitespace characters to upstream servers. When this
    occurs as a prefix to a Content-Length header, the frame length
    specified will be ignored by Squid (allowing for a conflicting
    length to be used from another Content-Length header) but relayed
    upstream.

CVE-2020-24606

    Squid allows a trusted peer to perform Denial of Service by
    consuming all available CPU cycles during handling of a crafted
    Cache Digest response message. This only occurs when cache_peer is
    used with the cache digests feature. The problem exists because
    peerDigestHandleReply() livelocking in peer_digest.cc mishandles
    EOF.
