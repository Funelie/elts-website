---
title: "ELA-295-1 rails security update"
package: rails
version: 2:4.1.8-1+deb8u8
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2020-10-09T20:00:19+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-8163
  - CVE-2020-15169

---

CVE-2020-8163

    A code injection vulnerability in Rails would allow an attacker who
    controlled the `locals` argument of a `render` call to perform a possible
    remote code execution.

CVE-2020-15169

    There is a potential Cross-Site Scripting (XSS) vulnerability in Action
    View's translation helpers. Views that allow the user to control the
    default (not found) value of the `t` and `translate` helpers could be
    susceptible to XSS attacks. When an HTML-unsafe string is passed as the
    default for a missing translation key named html or ending in _html, the
    default string is incorrectly marked as HTML-safe and not escaped.
