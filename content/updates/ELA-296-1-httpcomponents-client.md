---
title: "ELA-296-1 httpcomponents-client security update"
package: httpcomponents-client
version: 4.3.5-2+deb8u1
distribution: "Debian 8 jessie"
description: "improper handling of input"
date: 2020-10-11T00:04:49+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-13956

---

Oleg Kalnichevski discovered that httpcomponents-client, a Java library
for building HTTP-aware applications, can misinterpret a malformed
authority component in request URIs passed to the library as
java.net.URI object and pick the wrong target host for request
execution.
