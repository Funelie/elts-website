---
title: "ELA-300-2 openjdk-8 regression update"
package: openjdk-8
version: 8u275-b01-1~deb8u1
distribution: "Debian 8 jessie"
description: "regression update"
date: 2020-12-22T09:54:18+01:00
draft: false
type: updates
cvelist:

---

A few issues have been found in the OpenJDK 8u272 update, including
LDAP connection failures and application crash.
