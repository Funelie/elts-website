---
title: "ELA-301-1 bluez security update"
package: bluez
version: 5.43-2+deb9u2~deb8u2
distribution: "Debian 8 Jessie"
description: "Double-free vulnerability"
date: 2020-10-24T11:02:18+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-27153
---

It was discovered that there was a double-free vulnerability in
[BlueZ](http://www.bluez.org/), a suite of Bluetooth tools, utilities and
daemons.

A remote attacker could potentially cause a denial of service or code
execution during service discovery, due to a redundant disconnect management
(`MGMT`) event.
