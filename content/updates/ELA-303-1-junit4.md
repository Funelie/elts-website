---
title: "ELA-303-1 junit4 security update"
package: junit4
version: 4.11-3+deb8u1
distribution: "Debian 8 jessie"
description: "information disclosure"
date: 2020-10-29T14:01:25+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-15250

---

The test rule TemporaryFolder contains a local information disclosure
vulnerability. On Unix like systems, the system's temporary directory is shared
between all users on that system. Because of this, when files and directories
are written into this directory they are, by default, readable by other users
on that same system. This vulnerability does not allow other users to overwrite
the contents of these directories or files. This is purely an information
disclosure vulnerability. This vulnerability impacts you if the JUnit tests
write sensitive information, like API keys or passwords, into the temporary
folder, and the JUnit tests execute in an environment where the OS has other
untrusted users.
