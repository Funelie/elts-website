---
title: "ELA-304-1 freetype security update"
package: freetype
version: 2.5.2-3+deb8u5
distribution: "Debian 8 jessie"
description: "heap-based buffer overflow"
date: 2020-10-26T16:21:49+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-15999

---

Sergei Glazunov discovered a heap-based buffer overflow vulnerability in
the handling of embedded PNG bitmaps in FreeType. Opening malformed fonts
may result in denial of service or the execution of arbitrary code.

