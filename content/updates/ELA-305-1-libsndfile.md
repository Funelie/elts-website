---
title: "ELA-305-1 libsndfile security update"
package: libsndfile
version: 1.0.25-9.1+deb8u5
distribution: "Debian 8 jessie"
description: "fix for divide by zero and buffer overflow errors"
date: 2020-10-29T17:05:27+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-6892
  - CVE-2017-7585
  - CVE-2017-7586
  - CVE-2017-7741
  - CVE-2017-7742
  - CVE-2017-12562
  - CVE-2017-16942

---

Several issues have been found in libsndfile, a library for
reading/writing audio files.
All issues are basically divide by zero errors, heap read overflows or
other buffer overlow errors.
