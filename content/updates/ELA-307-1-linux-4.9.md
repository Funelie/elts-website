---
title: "ELA-307-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.240-2~deb8u1
distribution: "Debian 8 jessie"
description: "Linux Kernel update"
date: 2020-11-03T10:25:25+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-9445
  - CVE-2019-19073
  - CVE-2019-19074
  - CVE-2019-19448
  - CVE-2020-12351
  - CVE-2020-12352
  - CVE-2020-12655
  - CVE-2020-12771
  - CVE-2020-12888
  - CVE-2020-14305
  - CVE-2020-14314
  - CVE-2020-14331
  - CVE-2020-14356
  - CVE-2020-14386
  - CVE-2020-14390
  - CVE-2020-15393
  - CVE-2020-16166
  - CVE-2020-24490
  - CVE-2020-25211
  - CVE-2020-25212
  - CVE-2020-25220
  - CVE-2020-25284
  - CVE-2020-25285
  - CVE-2020-25641
  - CVE-2020-25643
  - CVE-2020-26088

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to the execution of arbitrary code, privilege escalation,
denial of service or information leaks.

CVE-2019-9445

    A potential out-of-bounds read was discovered in the F2FS
    implementation.  A user permitted to mount and access arbitrary
    filesystems could potentially use this to cause a denial of
    service (crash) or to read sensitive information.

CVE-2019-19073, CVE-2019-19074

    Navid Emamdoost discovered potential memory leaks in the ath9k and
    ath9k_htc drivers.  The security impact of these is unclear.

CVE-2019-19448

    "Team bobfuzzer" reported a bug in Btrfs that could lead to a
    use-after-free, and could be triggered by crafted filesystem
    images.  A user permitted to mount and access arbitrary
    filesystems could use this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.

CVE-2020-12351

    Andy Nguyen discovered a flaw in the Bluetooth implementation in
    the way L2CAP packets with A2MP CID are handled.  A remote attacker
    within a short distance, knowing the victim's Bluetooth device
    address, can send a malicious l2cap packet and cause a denial of
    service or possibly arbitrary code execution with kernel
    privileges.

CVE-2020-12352

    Andy Nguyen discovered a flaw in the Bluetooth implementation.
    Stack memory is not properly initialised when handling certain AMP
    packets.  A remote attacker within a short distance, knowing the
    victim's Bluetooth device address address, can retrieve kernel
    stack information.

CVE-2020-12655

    Zheng Bin reported that crafted XFS volumes could trigger a system
    hang.  An attacker able to mount such a volume could use this to
    cause a denial of service.

CVE-2020-12771

    Zhiqiang Liu reported a bug in the bcache block driver that could
    lead to a system hang.  The security impact of this is unclear.

CVE-2020-12888

    It was discovered that the PCIe Virtual Function I/O (vfio-pci)
    driver allowed users to disable a device's memory space while it
    was still mapped into a process.  On some hardware platforms,
    local users or guest virtual machines permitted to access PCIe
    Virtual Functions could use this to cause a denial of service
    (hardware error and crash).

CVE-2020-14305

    Vasily Averin of Virtuozzo discovered a potential heap buffer
    overflow in the netfilter nf_contrack_h323 module.  When this
    module is used to perform connection tracking for TCP/IPv6, a
    remote attacker could use this to cause a denial of service (crash
    or memory corruption) or possibly for remote code execution with
    kernel privilege.

CVE-2020-14314

    A bug was discovered in the ext4 filesystem that could lead to an
    out-of-bound read.  A local user permitted to mount and access
    arbitrary filesystem images could use this to cause a denial of
    service (crash).

CVE-2020-14331

    A bug was discovered in the VGA console driver's soft-scrollback
    feature that could lead to a heap buffer overflow.  On a system
    with a custom kernel that has CONFIG_VGACON_SOFT_SCROLLBACK
    enabled, a local user with access to a console could use this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.

CVE-2020-14356, CVE-2020-25220

    A bug was discovered in the cgroup subsystem's handling of socket
    references to cgroups.  In some cgroup configurations, this could
    lead to a use-after-free.  A local user might be able to use this
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

    The original fix for this bug introudced a new security issue,
    which is also addressed in this update.

CVE-2020-14386

    Or Cohen discovered a bug in the packet socket (AF_PACKET)
    implementation which could lead to a heap buffer overflow.  A
    local user with the CAP_NET_RAW capability (in any user namespace)
    could use this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.

CVE-2020-14390

    Minh Yuan discovered a bug in the framebuffer console driver's
    scrollback feature that could lead to a heap buffer overflow.  On
    a system using framebuffer consoles, a local user with access to a
    console could use this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.

    The scrollback feature has been disabled for now, as no other fix
    was available for this issue.

CVE-2020-15393

    Kyungtae Kim reported a memory leak in the usbtest driver.  The
    security impact of this is unclear.

CVE-2020-16166

    Amit Klein reported that the random number generator used by the
    network stack might not be re-seeded for long periods of time,
    making e.g. client port number allocations more predictable.  This
    made it easier for remote attackers to carry out some network-
    based attacks such as DNS cache poisoning or device tracking.

CVE-2020-24490

    Andy Nguyen discovered a flaw in the Bluetooth implementation that
    can lead to a heap buffer overflow.  On systems with a Bluetooth 5
    hardware interface, a remote attacker within a short distance can
    use this to cause a denial of service (crash or memory corruption)
    or possibly for remote code execution with kernel privilege.

CVE-2020-25211

    A flaw was discovered in netfilter subsystem.  A local attacker
    able to inject conntrack Netlink configuration can cause a denial
    of service.

CVE-2020-25212

    A bug was discovered in the NFSv4 client implementation that could
    lead to a heap buffer overflow.  A malicious NFS server could use
    this to cause a denial of service (crash or memory corruption) or
    possibly to execute arbitrary code on the client.

CVE-2020-25284

    It was discovered that the Rados block device (rbd) driver allowed
    tasks running as uid 0 to add and remove rbd devices, even if they
    dropped capabilities.  On a system with the rbd driver loaded,
    this might allow privilege escalation from a container with a task
    running as root.

CVE-2020-25285

    A race condition was discovered in the hugetlb filesystem's sysctl
    handlers, that could lead to stack corruption.  A local user
    permitted to write to hugepages sysctls could use this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.  By default only the root user can do this.

CVE-2020-25641

    The syzbot tool found a bug in the block layer that could lead to
    an infinite loop.  A local user with access to a raw block device
    could use this to cause a denial of service (unbounded CPU use and
    possible system hang).

CVE-2020-25643

    ChenNan Of Chaitin Security Research Lab discovered a flaw in the
    hdlc_ppp module.  Improper input validation in the ppp_cp_parse_cr()
    function may lead to memory corruption and information disclosure.

CVE-2020-26088

    It was discovered that the NFC (Near Field Communication) socket
    implementation allowed any user to create raw sockets.  On a
    system with an NFC interface, this allowed local users to evade
    local network security policy.
