---
title: "ELA-308-1 krb5 security update"
package: krb5
version: 1.12.1+dfsg-19+deb8u6
distribution: "Debian 8 Jessie"
description: "denial-of-service vulnerability"
date: 2020-11-07T12:42:26+00:00
draft: false
type: updates
cvelist:
  - CVE-2020-28196
---

It was discovered that there was a denial of service vulnerability in the MIT
Kerberos network authentication system, `krb5`. The lack of a limit in the
"ASN.1" decoder could lead to infinite recursion and allow an attacker to
overrun the stack and cause the process to crash.
