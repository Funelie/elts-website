---
title: "ELA-309-1 raptor2 security update"
package: raptor2
version: 2.0.14-1+deb8u1
distribution: "Debian 8 Jessie"
description: "heap-overflow vulnerabilities"
date: 2020-11-07T13:03:18+00:00
draft: false
type: updates
cvelist:
  - CVE-2017-18926
---

It was discovered that there were two heap overflow vulnerabilities in
`raptor2`, a set of parsers for RDF files that is used (amongst others) in
LibreOffice.
