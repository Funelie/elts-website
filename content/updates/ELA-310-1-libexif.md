---
title: "ELA-310-1 libexif security update"
package: libexif
version: 0.6.21-2+deb8u5
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2020-11-07T19:07:39+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-0452

---

In libexif/exif-entry.c, through libexif 0.6.21-2+deb8u4, compiler
optimization could remove a buffer overflow check, making a
buffer overflow possible with some EXIF tags.
