---
title: "ELA-311-1 tcpdump security update"
package: tcpdump
version: 4.9.3-1~deb8u2
distribution: "Debian 8 jessie"
description: "untrusted input issue"
date: 2020-11-09T15:36:30+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-8037

---

The ppp de-capsulator in tcpdump 4.9.3 can be convinced to allocate
a large amount of memory. 

The buffer should be big enough to hold the captured data, but it
doesn't need to be big enough to hold the entire on-the-network packet,
if we haven't captured all of it.
