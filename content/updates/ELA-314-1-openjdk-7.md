---
title: "ELA-314-1 openjdk-7 security update"
package: openjdk-7
version: 7u281-2.6.24-0+deb8u1
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2020-11-11T13:14:41+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-14779
  - CVE-2020-14781
  - CVE-2020-14782
  - CVE-2020-14792
  - CVE-2020-14796
  - CVE-2020-14797
  - CVE-2020-14798
  - CVE-2020-14803

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in denial of service, bypass of sandbox restrictions or
information disclosure.
