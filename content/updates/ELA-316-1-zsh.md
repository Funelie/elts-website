---
title: "ELA-316-1 zsh security update"
package: zsh
version: 5.0.7-5+deb8u3
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2020-11-19T16:21:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-10714
  - CVE-2017-18206
  - CVE-2018-0502
  - CVE-2018-1071
  - CVE-2018-1083
  - CVE-2018-1100
  - CVE-2018-13259

---

Several security vulnerabilities were found and corrected in zsh, a powerful
shell and scripting language. Off-by-one errors, wrong parsing of shebang lines
and buffer overflows may lead to unexpected behavior. A local, unprivileged
user can create a specially crafted message file or directory path. If the
receiving user is privileged or traverses the aforementioned path, this leads
to privilege escalation.
