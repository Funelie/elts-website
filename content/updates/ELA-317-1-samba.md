---
title: "ELA-317-1 samba security update"
package: samba
version: 2:4.2.14+dfsg-0+deb8u14
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-11-22T22:08:25-05:00
draft: false
type: updates
cvelist:
  - CVE-2020-1472
  - CVE-2020-10704
  - CVE-2020-10745
  - CVE-2020-14303
  - CVE-2020-14318
  - CVE-2020-14323
  - CVE-2020-14383

---

Multiple vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix.

CVE-2020-1472

    Unauthenticated domain controller compromise by subverting Netlogon
    cryptography.  This vulnerability includes both ZeroLogon and
    non-ZeroLogon variations.

CVE-2020-10704

    An unauthorized user can trigger a denial of service via a stack
    overflow in the AD DC LDAP server.

CVE-2020-10745

    Denial of service resulting from abuse of compression of replies to
    NetBIOS over TCP/IP name resolution and DNS packets causing excessive
    CPU load on the Samba AD DC.

CVE-2020-14303

    Denial of service resulting from CPU spin and and inability to
    process further requests once the AD DC NBT server receives an empty
    (zero-length) UDP packet to port 137.

CVE-2020-14318

    Missing handle permissions check in ChangeNotify

CVE-2020-14323

    Unprivileged user can crash winbind via invalid lookupsids DoS

CVE-2020-14383

    DNS server crash via invalid records resulting from uninitialized
    variables
