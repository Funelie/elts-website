---
title: "ELA-318-1 python-werkzeug security update"
package: python-werkzeug
version: 0.9.6+dfsg-1+deb8u2
distribution: "Debian 8 jessie"
description: "open redirect"
date: 2020-11-24T12:47:34+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-28724

---

It was found that the WSGI server included in Werkzeug would redirect requests
if the URL path started with a double slash.
