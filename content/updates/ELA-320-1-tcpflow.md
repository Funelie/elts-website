---
title: "ELA-320-1 tcpflow security update"
package: tcpflow
version: 1.4.4+repack1-3+deb8u1
distribution: "Debian 8 jessie"
description: "fir for overflow vulnerability"
date: 2020-11-29T00:05:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-14938

---

An issue has been found in tcpflow, a TCP flow recorder.

Due to an overflow vulnerability in function handle_80211, an out-of-bounds read with access to sensitive memory or a denial of service might happen.

