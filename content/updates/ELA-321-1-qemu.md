---
title: "ELA-321-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u18
distribution: "Debian 8 jessie"
description: "several issues"
date: 2020-11-29T23:12:56+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-25085
  - CVE-2020-25624
  - CVE-2020-25625
  - CVE-2020-25723
  - CVE-2020-27617

---

Some issues have been found in qemu, a fast processor emulator.

All issues are related to assertion failures, out-of-bounds access failures or bad handling of return codes.

