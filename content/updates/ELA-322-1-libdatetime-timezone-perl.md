---
title: "ELA-322-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.75-2+2020d
distribution: "Debian 8 jessie"
description: "Update to tzdata 2020d"
date: 2020-11-30T20:25:05+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2020d for the
Perl bindings.
