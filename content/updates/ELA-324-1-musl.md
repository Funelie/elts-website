---
title: "ELA-324-1 musl security update"
package: musl
version: 1.1.5-2+deb8u2
distribution: "Debian 8 jessie"
description: "wcsnrtombs destination buffer overflow"
date: 2020-12-01T03:47:18+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-28928

---

The wcsnrtombs function in all musl libc versions up through 1.2.1 has
been found to have multiple bugs in handling of destination buffer
size when limiting the input character count, which can lead to
infinite loop with no forward progress (no overflow) or writing past
the end of the destination buffers.
