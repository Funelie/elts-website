---
title: "ELA-326-1 libxstream-java security update"
package: libxstream-java
version: 1.4.7-2+deb8u3
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2020-12-02T00:37:56+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-26217

---

It was found that XStream is vulnerable to Remote Code Execution. The
vulnerability may allow a remote attacker to run arbitrary shell commands only
by manipulating the processed input stream. Users who rely on blocklists
are affected (the default in Debian). We strongly recommend to use the
whitelist approach of XStream's Security Framework because there are likely
more class combinations the blacklist approach may not address.

