---
title: "ELA-327-1 openldap security update"
package: openldap
version: 2.4.40+dfsg-1+deb8u8
distribution: "Debian 8 jessie"
description: "denial-of-service vulnerability"
date: 2020-12-04T21:17:32+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-25709
  - CVE-2020-25710

---

Two vulnerabilities in the certificate list syntax verification and
in the handling of CSN normalization were discovered in OpenLDAP, a
free implementation of the Lightweight Directory Access Protocol.
An unauthenticated remote attacker can take advantage of these
flaws to cause a denial of service (slapd daemon crash) via
specially crafted packets.
