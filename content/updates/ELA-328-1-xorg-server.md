---
title: "ELA-328-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u4
distribution: "Debian 8 jessie"
description: "privilege escalation"
date: 2020-12-09T11:30:06+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-14360
  - CVE-2020-25712

---

Jan-Niklas Sohn discovered that the XKB extension of the Xorg X server
performed incomplete input validation, which could result in privilege
escalation.
