---
title: "ELA-329-1 jasper security update"
package: jasper
version: 1.900.1-debian1-2.4+deb8u7
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2020-12-11T23:07:34+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-9782
  - CVE-2018-19139
  - CVE-2018-19543
  - CVE-2020-27828

---

Several security vulnerabilities were found and corrected in jasper, a JPEG
2000 image library, which could lead to denial-of-service or have other unspecified
impact.

 CVE-2018-19139: Fix memory leaks by registering jpc_unk_destroyparms.

 CVE-2020-27828: Avoid maxrlvls more than upper bound to cause heap-buffer-overflow.

 CVE-2018-19543 and CVE-2017-9782: There is a heap-based buffer over-read of size 8
 in the function jp2_decode in libjasper/jp2/jp2_dec.c.
