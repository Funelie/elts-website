---
title: "ELA-33-1 libtirpc security update"
package: libtirpc
version: 0.2.2-5+deb7u2
distribution: "Debian 7 Wheezy"
description: "Segmentation fault due to pointer becoming NULL"
date: 2018-08-31T19:55:24+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-14622
---

This version fixes a segmentation fault due to pointer becoming NULL.
