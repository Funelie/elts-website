---
title: "ELA-330-1 xerces-c security update"
package: xerces-c
version: 3.1.1-5.1+deb8u5
distribution: "Debian 8 jessie"
description: "use-after-free vulnerability"
date: 2020-12-11T19:37:58+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-1311

---

The UK's National Cyber Security Centre (NCSC) discovered that
Xerces-C, a validating XML parser library for C++, contains a
use-after-free error triggered during the scanning of external
DTDs. An attacker could cause a Denial of Service (DoS) and possibly
achieve remote code execution. This flaw has not been addressed in the
maintained version of the library and has no complete mitigation. The
first is provided by this update which fixes the use-after-free
vulnerability at the expense of a memory leak. The other is to disable
DTD processing, which can be accomplished via the DOM using a standard
parser feature, or via SAX using the XERCES_DISABLE_DTD environment
variable.
