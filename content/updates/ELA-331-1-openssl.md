---
title: "ELA-331-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u13
distribution: "Debian 8 jessie"
description: "denial of service and timing side channel attack"
date: 2020-12-14T10:08:09+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-0734
  - CVE-2020-1971

---

Two security vulnerabilities were found in OpenSSL, the Secure Sockets
Layer toolkit.

CVE-2018-0734

    A minor timing side channel attack was found in the OpenSSL DSA
    signature algorithm. The fix for that introduced a more severe
    regression that could also be exploited as a timing side channel
    attack. This update fixes both the original problem and the
    subsequent issue.

CVE-2020-1971

    David Benjamin discovered a flaw in the GENERAL_NAME_cmp() function
    which could cause a NULL dereference, resulting in denial of service.
