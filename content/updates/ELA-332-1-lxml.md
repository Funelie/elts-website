---
title: "ELA-332-1 lxml security update"
package: lxml
version: 3.4.0-1+deb8u3
distribution: "Debian 8 jessie"
description: "cross-site scripting"
date: 2020-12-15T11:04:38+01:00
draft: false
type: updates
cvelist:

---

It was discovered that the `clean_html()` function of lxml, a Python library
for HTML and XML processing, performed insufficient sanitisation for embedded
Javascript code. This could lead to cross-site scripting or possibly the
execution of arbitrary code.
