---
title: "ELA-334-1 curl security update"
package: curl
version: 7.38.0-4+deb8u19
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2020-12-18T09:07:18-05:00
draft: false
type: updates
cvelist:
  - CVE-2020-8284
  - CVE-2020-8285

---

Two vulnerabilities were discovered in curl, a command line tool for
transferring data with URL syntax and an easy-to-use client-side URL
transfer library.

CVE-2020-8284

    When curl performs a passive FTP transfer, it first tries the EPSV
    command and if that is not supported, it falls back to using PASV.
    Passive mode is what curl uses by default.  A server response to a
    PASV command includes the (IPv4) address and port number for the
    client to connect back to in order to perform the actual data
    transfer.  This is how the FTP protocol is designed to work.  A
    malicious server can use the PASV response to trick curl into
    connecting back to a given IP address and port, and this way
    potentially make curl extract information about services that are
    otherwise private and not disclosed, for example doing port scanning
    and service banner extractions.

    The IP address part of the response is now ignored by default, by
    making CURLOPT_FTP_SKIP_PASV_IP default to 1L instead of previously
    being 0L.  This has the minor drawback that a small fraction of use
    cases might break, when a server truly needs the client to connect
    back to a different IP address than what the control connection uses
    and for those CURLOPT_FTP_SKIP_PASV_IP can be set to 0L.  The same
    goes for the command line tool, which then might need
    --no-ftp-skip-pasv-ip set to prevent curl from ignoring the address
    in the server response.

CVE-2020-8285

    libcurl offers a wildcard matching functionality, which allows a
    callback (set with CURLOPT_CHUNK_BGN_FUNCTION) to return information
    back to libcurl on how to handle a specific entry in a directory
    when libcurl iterates over a list of all available entries.  When
    this callback returns CURL_CHUNK_BGN_FUNC_SKIP, to tell libcurl to
    not deal with that file, the internal function in libcurl then calls
    itself recursively to handle the next directory entry.  If there's a
    sufficient amount of file entries and if the callback returns "skip"
    enough number of times, libcurl runs out of stack space.  The exact
    amount will of course vary with platforms, compilers and other
    environmental factors.  The content of the remote directory is not
    kept on the stack, so it seems hard for the attacker to control
    exactly what data that overwrites the stack - however it remains a
    Denial-Of-Service vector as a malicious user who controls a server
    that a libcurl-using application works with under these premises can
    trigger a crash.

    The internal function is rewritten to instead and more appropriately
    use an ordinary loop instead of the recursive approach. This way,
    the stack use will remain the same no matter how many files that are
    skipped.
