---
title: "ELA-336-1 libxstream-java security update"
package: libxstream-java
version: 1.4.11.1-1+deb8u1
distribution: "Debian 8 jessie"
description: "arbitrary file deletion"
date: 2020-12-31T19:51:30+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-26258
  - CVE-2020-26259

---

Several security vulnerabilities were discovered in XStream, a Java library to
serialize objects to XML and back again.

CVE-2020-26258

    XStream is vulnerable to a Server-Side Forgery Request which can be
    activated when unmarshalling. The vulnerability may allow a remote attacker
    to request data from internal resources that are not publicly available
    only by manipulating the processed input stream.

CVE-2020-26259

    Xstream is vulnerable to an Arbitrary File Deletion on the local host when
    unmarshalling. The vulnerability may allow a remote attacker to delete
    arbitrary known files on the host as long as the executing process has
    sufficient rights only by manipulating the processed input stream.
