---
title: "ELA-337-1 tzdata new upstream version"
package: tzdata
version: 2020e-0+deb8u1
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2020-12-29T20:46:02+01:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2020e. Notable
changes are:

- Volgograd switched to Moscow time on 2020-12-27 at 02:00.

