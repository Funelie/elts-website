---
title: "ELA-339-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.246-2~deb8u1
distribution: "Debian 8 jessie"
description: "linux kernel update"
date: 2020-12-30T08:46:23+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-0427
  - CVE-2020-8694
  - CVE-2020-14351
  - CVE-2020-25645
  - CVE-2020-25656
  - CVE-2020-25668
  - CVE-2020-25669
  - CVE-2020-25704
  - CVE-2020-25705
  - CVE-2020-27673
  - CVE-2020-27675
  - CVE-2020-28974

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to the execution of arbitrary code, privilege escalation,
denial of service or information leaks.

CVE-2020-0427

    Elena Petrova reported a bug in the pinctrl subsystem that can
    lead to a use-after-free after a device is renamed.  The security
    impact of this is unclear.

CVE-2020-8694

    Multiple researchers discovered that the powercap subsystem
    allowed all users to read CPU energy meters, by default.  On
    systems using Intel CPUs, this provided a side channel that could
    leak sensitive information between user processes, or from the
    kernel to user processes.  The energy meters are now readable only
    by root, by default.

    This issue can be mitigated by running:

        chmod go-r /sys/devices/virtual/powercap/*/*/energy_uj

    This needs to be repeated each time the system is booted with
    an unfixed kernel version.

CVE-2020-14351

    A race condition was discovered in the performance events
    subsystem, which could lead to a use-after-free.  A local user
    permitted to access performance events could use this to cause a
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.

    Debian's kernel configuration does not allow unprivileged users to
    access peformance events by default, which fully mitigates this
    issue.

CVE-2020-25645

    A flaw was discovered in the interface driver for GENEVE
    encapsulated traffic when combined with IPsec. If IPsec is
    configured to encrypt traffic for the specific UDP port used by the
    GENEVE tunnel, tunneled data isn't correctly routed over the
    encrypted link and sent unencrypted instead.

CVE-2020-25656

    Yuan Ming and Bodong Zhao discovered a race condition in the
    virtual terminal (vt) driver that could lead to a use-after-free.
    A local user with the CAP_SYS_TTY_CONFIG capability could use this
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2020-25668

    Yuan Ming and Bodong Zhao discovered a race condition in the
    virtual terminal (vt) driver that could lead to a use-after-free.
    A local user with access to a virtual terminal, or with the
    CAP_SYS_TTY_CONFIG capability, could use this to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.

CVE-2020-25669

    Bodong Zhao discovered a bug in the Sun keyboard driver (sunkbd)
    that could lead to a use-after-free.  On a system using this
    driver, a local user could use this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.

CVE-2020-25704

    kiyin(尹亮) discovered a potential memory leak in the performance
    events subsystem.  A local user permitted to access performance
    events could use this to cause a denial of service (memory
    exhaustion).

    Debian's kernel configuration does not allow unprivileged users to
    access peformance events by default, which fully mitigates this
    issue.

CVE-2020-25705

    Keyu Man reported that strict rate-limiting of ICMP packet
    transmission provided a side-channel that could help networked
    attackers to carry out packet spoofing.  In particular, this made
    it practical for off-path networked attackers to "poison" DNS
    caches with spoofed responses ("SAD DNS" attack).

    This issue has been mitigated by randomising whether packets are
    counted against the rate limit.

CVE-2020-27673 / XSA-332

    Julien Grall from Arm discovered a bug in the Xen event handling
    code.  Where Linux was used in a Xen dom0, unprivileged (domU)
    guests could cause a denial of service (excessive CPU usage or
    hang) in dom0.

CVE-2020-27675 / XSA-331

    Jinoh Kang of Theori discovered a race condition in the Xen event
    handling code.  Where Linux was used in a Xen dom0, unprivileged
    (domU) guests could cause a denial of service (crash) in dom0.

CVE-2020-28974

    Yuan Ming discovered a bug in the virtual terminal (vt) driver
    that could lead to an out-of-bounds read.  A local user with
    access to a virtual terminal, or with the CAP_SYS_TTY_CONFIG
    capability, could possibly use this to obtain sensitive
    information from the kernel or to cause a denial of service
    (crash).

    The specific ioctl operation affected by this bug
    (KD_FONT_OP_COPY) has been disabled, as it is not believed that
    any programs depended on it.
