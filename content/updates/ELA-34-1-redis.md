---
title: "ELA-34-1 redis security update"
package: redis
version: 2:2.4.14-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "Buffer overflow vulnerability"
date: 2018-09-05T11:12:04+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-12326
---

A buffer overflow vulnerability was discovered in the the redis key-value
database. The "redis-cli" tool could have allowed an attacker to achieve code
execution and/or escalate to higher privileges via a specially-crafted command
line.
