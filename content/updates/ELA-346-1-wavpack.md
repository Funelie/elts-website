---
title: "ELA-346-1 wavpack security update"
package: wavpack
version: 4.70.0-1+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilites"
date: 2021-01-14T23:52:19+05:30
draft: false
type: updates
cvelist:
  - CVE-2016-10169
  - CVE-2018-19840
  - CVE-2019-1010319
  - CVE-2020-35738

---

Multiple vulnerabilites in wavpack were found, like OOB read
(which could potentially lead to a DOS attack), unexpected
control flow, crashes, integer overflow, and segfaults.
