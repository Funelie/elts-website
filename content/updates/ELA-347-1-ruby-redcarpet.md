---
title: "ELA-347-1 ruby-redcarpet security update"
package: ruby-redcarpet
version: 3.1.2-1+deb8u1
distribution: "Debian 8 jessie"
description: "cross-site scripting"
date: 2021-01-16T18:17:22+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-26298

---

In Redcarpet before version 3.5.1, there is an injection
vulnerability which can enable a cross-site scripting attack.

In affected versions, no HTML escaping was being performed when
processing quotes. This applies even when the `:escape_html`
option was being used.
