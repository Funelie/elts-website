---
title: "ELA-348-1 gst-plugins-bad1.0 security update"
package: gst-plugins-bad1.0
version: 1.4.4-2.1+deb8u3
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2021-01-19T01:12:17+05:30
draft: false
type: updates
cvelist:
  - TEMP-0000000-FAEBC0
---

Andrew Wesie discovered a buffer overflow in the H264 support of the
GStreamer multimedia framework, which could potentially result in the
execution of arbitrary code.
