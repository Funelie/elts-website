---
title: "ELA-349-1 mutt security update"
package: mutt
version: 1.5.23-3+deb8u5
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-01-21T01:50:23+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3181

---

rfc822.c in Mutt through 2.0.4 allows remote attackers to
cause a denial of service (mailbox unavailability) by sending
email messages with sequences of semicolon characters in
RFC822 address fields (aka terminators of empty groups).

A small email message from the attacker can cause large
memory consumption, and the victim may then be unable to
see email messages from other persons.
