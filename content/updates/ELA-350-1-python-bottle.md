---
title: "ELA-350-1 python-bottle security update"
package: python-bottle
version: 0.12.7-1+deb8u3
distribution: "Debian 8 jessie"
description: "web cache poisoning"
date: 2021-01-24T22:48:41+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-28473

---

The package src:python-bottle before 0.12.19 are vulnerable to
Web Cache Poisoning by using a vector called parameter cloaking.

When the attacker can separate query parameters using a
semicolon (;), they can cause a difference in the interpretation
of the request between the proxy (running with default
configuration) and the server. This can result in malicious
requests being cached as completely safe ones, as the proxy would
usually not see the semicolon as a separator, and therefore would
not include it in a cache key of an unkeyed parameter.
