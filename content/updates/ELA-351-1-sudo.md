---
title: "ELA-351-1 sudo security update"
package: sudo
version: 1.8.10p3-1+deb8u8
distribution: "Debian 8 jessie"
description: "fix for root privilege escalation"
date: 2021-01-27T11:26:30+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3156

---

The Qualys Research Labs discovered a heap-based buffer overflow
vulnerability in sudo, a program designed to provide limited super user
privileges to specific users. Any local user (sudoers and non-sudoers)
can exploit this flaw for root privilege escalation.
