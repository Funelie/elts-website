---
title: "ELA-352-1 dbus security update"
package: dbus
version: 1.8.22-0+deb8u4
distribution: "Debian 8 jessie"
description: "use after free"
date: 2021-01-28T10:40:23+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-35512

---

An issue has been found in dbus, a simple interprocess messaging system.
On a system having multiple usernames sharing the same UID a use-after-free might
happen, that could result in a denial of service or undefined behaviour, possibly
including incorrect authorization decisions.
