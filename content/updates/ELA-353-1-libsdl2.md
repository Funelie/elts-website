---
title: "ELA-353-1 libsdl2 security update"
package: libsdl2
version: 2.0.2+dfsg1-6+deb8u3
distribution: "Debian 8 jessie"
description: "buffer overflow; integer overflow; heap-based buffer over-read"
date: 2021-01-30T22:34:20+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-13616
  - CVE-2020-14409
  - CVE-2020-14410

---

Several issues have been found in libsdl2, a library for portable low
level access to a video framebuffer, audio output, mouse, and keyboard.
All issues are related to either buffer overflow, integer overflow or 
heap-based buffer over-read, resulting in a DoS or remote code execution
by using a crafted BMP file.
