---
title: "ELA-354-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u11
distribution: "Debian 8 Jessie"
description: "directory-traversal vulnerability"
date: 2021-02-01T18:41:46+00:00
draft: false
type: updates
cvelist:
  - CVE-2021-3281
---

It was discovered that there was a potential directory-traversal in
[Django](https://djangoproject.com), a popular Python-based web development
framework.

The `django.utils.archive.extract()` function, used by `startapp --template`
and `startproject --template`, allowed directory-traversal via an archive
with absolute paths or relative paths with dot (`.`) segments.
