---
title: "ELA-355-1 tzdata new upstream version"
package: tzdata
version: 2021a-0+deb8u1
distribution: "Debian 8 jessie"
description: "timezone database update"
date: 2021-02-02T13:42:16+01:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2021a. Notable
changes are:

 - South Sudan changed from +03 to +02 on 2021-02-01.
