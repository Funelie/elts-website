---
title: "ELA-356-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.75-2+2021a
distribution: "Debian 8 jessie"
description: "update to tzdata 2021a"
date: 2021-02-02T13:49:20+01:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2021a for the
Perl bindings.
