---
title: "ELA-357-1 jasper security update"
package: jasper
version: 1.900.1-debian1-2.4+deb8u7
distribution: "Debian 8 jessie"
description: "heap buffer overflow"
date: 2021-02-03T12:44:28+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3272

---


`jp2_decode in jp2/jp2_dec.c` in libjasper in JasPer has a heap-based
buffer over-read when there is an invalid relationship between the
number of channels and the number of image components.
