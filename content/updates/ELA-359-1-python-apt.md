---
title: "ELA-359-1 python-apt security update"
package: python-apt
version: 0.9.3.14
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-02-07T01:12:10+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-27351

---

Various memory and file descriptor leaks were discovered in the Python
interface to the APT package management runtime library, which could
result in denial of service.
