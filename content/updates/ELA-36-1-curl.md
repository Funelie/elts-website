---
title: "ELA-36-1 curl security update"
package: curl
version: 7.26.0-1+wheezy25+deb7u2
distribution: "Debian 7 Wheezy"
description: "Integer overflow"
date: 2018-09-15T17:41:41+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-14618
---

Zhaoyang Wu discovered that cURL, an URL transfer library, contains a buffer
overflow in the NTLM authentication code triggered by passwords that exceed 2GB
in length on 32bit systems.
