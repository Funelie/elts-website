---
title: "ELA-361-1 jasper security update"
package: jasper
version: 1.900.1-debian1-2.4+deb8u9
distribution: "Debian 8 jessie"
description: "heap overflow and null pointer access"
date: 2021-02-11T01:43:00+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-26926
  - CVE-2021-26927

---

CVE-2021-26926

    A heap buffer overflow vulnerability was discovered
    in JasPer, through jp2_dec.c in the jp2_decode() function.

CVE-2021-26927

    A null pointer access was discovered in JasPer, through
    jp2_dec.c in the jp2_decode() function.
