---
title: "ELA-364-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u19
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-02-16T17:46:28+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-11947
  - CVE-2020-15469
  - CVE-2020-15859
  - CVE-2020-25084
  - CVE-2020-29130
  - CVE-2020-29443
  - CVE-2021-20181
  - CVE-2021-20221

---

Several vulnerabilities were discovered in QEMU, a fast processor
emulator (notably used in KVM and Xen HVM virtualization). An attacker
could trigger a denial-of-service (DoS), information leak, and
possibly execute arbitrary code with the privileges of the QEMU
process on the host.

* CVE-2020-15469

    A MemoryRegionOps object may lack read/write callback methods,
    leading to a NULL pointer dereference.

* CVE-2020-15859

    QEMU has a use-after-free in hw/net/e1000e_core.c because a guest
    OS user can trigger an e1000e packet with the data's address set
    to the e1000e's MMIO address.

* CVE-2020-25084

    QEMU has a use-after-free in hw/usb/hcd-xhci.c because the
    usb_packet_map return value is not checked.

* CVE-2020-28916

    hw/net/e1000e_core.c has an infinite loop via an RX descriptor
    with a NULL buffer address.

* CVE-2020-29130

    slirp.c has a buffer over-read because it tries to read a certain
    amount of header data even if that exceeds the total packet
    length.

* CVE-2020-29443

    ide_atapi_cmd_reply_end in hw/ide/atapi.c allows out-of-bounds
    read access because a buffer index is not validated.

* CVE-2021-20181

    9pfs: ZDI-CAN-10904: QEMU Plan 9 file system TOCTOU privilege
    escalation vulnerability.

* CVE-2021-20221

    aarch64: GIC: out-of-bound heap buffer access via an interrupt ID
    field.
