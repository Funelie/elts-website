---
title: "ELA-365-1 php-horde-text-filter security update"
package: php-horde-text-filter
version: 2.2.1-5+deb8u1
distribution: "Debian 8 jessie"
description: "cross-site scripting"
date: 2021-02-18T15:02:44+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-5303
  - CVE-2021-26929

---

Alex Birnberg discovered a cross-site scripting (XSS) vulnerability in
the Horde Application Framework, more precisely its Text Filter API.
An attacker could take control of a user's mailbox by sending a
crafted e-mail. This update also fixes a separate minor XSS
vulnerability discovered by Liuzhu.

* CVE-2021-26929

    An XSS issue was discovered in Horde Groupware Webmail Edition
    (where the Horde_Text_Filter library is used). The attacker can
    send a plain text e-mail message, with JavaScript encoded as a
    link or email that is mishandled by preProcess in Text2html.php,
    because bespoke use of \x00\x00\x00 and \x01\x01\x01 interferes
    with XSS defenses.

* CVE-2016-5303

    Cross-site scripting (XSS) vulnerability in the Horde Text Filter
    API in Horde Groupware and Horde Groupware Webmail Edition allows
    remote attackers to inject arbitrary web script or HTML via
    crafted data:text/html content in a form (1) action or (2) xlink
    attribute.
