---
title: "ELA-366-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u14
distribution: "Debian 8 Jessie"
description: "multiple vulnerabilities"
date: 2021-02-18T18:29:37+00:00
draft: false
type: updates
cvelist:
  - CVE-2021-23840
  - CVE-2021-23841
---

It was discovered that there were two issues in the OpenSSL cryptographic
system:

* Prevent an issue where "Digital EnVeloPe" EVP-related calls could cause
  applications to behave incorrectly or even crash.

* Prevent an issue in the X509 certificate parsing caused by the lack of
  error handling while ingesting the "issuer" field.
