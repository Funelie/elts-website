---
title: "ELA-367-1 libbsd security update"
package: libbsd
version: 0.7.0-2+deb8u2
distribution: "Debian 8 jessie"
description: "out-of-bounds read"
date: 2021-02-18T23:05:59+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-20367

---

An issue has been found in libbsd, a library with utility functions from BSD systems.
A non-NUL terminated symbol name in the string table might result in an out-of-bounds read.
