---
title: "ELA-368-1 unrar-free security update"
package: unrar-free
version: 1:0.0.1+cvs20140707-1+deb8u1
distribution: "Debian 8 jessie"
description: "directory traversal vulnerability; NULL pointer dereference; buffer over-read"
date: 2021-02-18T23:51:01+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-14120
  - CVE-2017-14121
  - CVE-2017-14122

---

Several issues have been found in unrar-free, an unarchiver for .rar files.
CVE-2017-14120 is related to a directory traversal vulnerability for RAR v2 archives.
CVE-2017-14121 is related to NULL pointer dereference flaw triggered by a specially crafted RAR archive.
CVE-2017-14122 is related to stack-based buffer over-read.
