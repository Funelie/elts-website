---
title: "ELA-37-1 openssh security update"
package: openssh
version: 6.0p1-4+deb7u8
distribution: "Debian 7 Wheezy"
description: "user enumeration vulnerability"
date: 2018-09-15T23:17:18Z
draft: false
type: updates
cvelist:
  - CVE-2018-15473
---

It was discovered that there was a user enumeration vulnerability in
OpenSSH. A remote attacker could test whether a certain user exists
on a target server.
