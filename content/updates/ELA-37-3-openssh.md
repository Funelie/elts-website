---
title: "ELA-37-3 openssh security update"
package: openssh
version: 1:6.0p1-4+deb7u10
distribution: "Debian 7 Wheezy"
description: "user enumeration vulnerability"
date: 2018-09-18T00:41:01Z
draft: false
type: updates
cvelist:
  - CVE-2018-15473
---

This update properly implements the fix for the issue first identified in
ELA-37-1.  The initial update package, version 1:6.0p1-4+deb7u8, is broken
and the subsequent package, version 1:6.0p1-4+deb7u9, reverts the incorrect
patch and so is vulnerable (as described in ELA-37-2).  The package version
referenced in this advisory contains the complete and correct fix for
CVE-2018-15473.

The original advisory text follows:

It was discovered that there was a user enumeration vulnerability in
OpenSSH. A remote attacker could test whether a certain user exists
on a target server.
