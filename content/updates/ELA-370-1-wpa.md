---
title: "ELA-370-1 wpa security update"
package: wpa
version: 2.3-1+deb8u12
distribution: "Debian 8 jessie"
description: "buffer over-write"
date: 2021-02-20T13:08:30+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-0326

---

An issue has been found in wpa, a set of tools to support WPA and WPA2
(IEEE 802.11i).
Missing validation of data can result in a buffer over-write, which might
lead to a DoS of the wpa_supplicant process or potentially arbitrary code
execution.

The mentioned support for WPA-EAP-SUITE-B(-192) in the changelog does
not affect the version in Jessie.
