---
title: "ELA-371-1 openldap security update"
package: openldap
version: 2.4.40+dfsg-1+deb8u10
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-02-21T15:15:59+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-27212

---

A vulnerability in the Certificate List Exact Assertion validation
was discovered in OpenLDAP, a free implementation of the Lightweight
Directory Access Protocol. An unauthenticated remote attacker can
take advantage of this flaw to cause a denial of service (slapd
daemon crash) via specially crafted packets.
