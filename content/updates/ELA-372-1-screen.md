---
title: "ELA-372-1 screen security update"
package: screen
version: 4.2.1-3+deb8u2
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-02-26T21:03:24+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-26937

---

encoding.c in GNU Screen through 4.8.0 allows remote attackers
to cause a denial of service (invalid write access and application
crash) or possibly have unspecified other impact via a crafted
UTF-8 character sequence.

**NOTE**: In order to bring this update to effect, you will
          need to restart your screen session(s).
