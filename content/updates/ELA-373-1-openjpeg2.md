---
title: "ELA-373-1 openjpeg2 security update"
package: openjpeg2
version: 2.1.0-2+deb8u12
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-03-01T22:39:55-05:00
draft: false
type: updates
cvelist:
  - CVE-2020-27824
  - CVE-2020-27841
  - CVE-2020-27845

---

Several issues were identified in openjpeg2.

CVE-2020-27824: Global buffer overflow on irreversible conversion when too many decomposition levels are specified.

CVE-2020-27841: Crafted input to be processed by the openjpeg encoder could cause an out-of-bounds read.

CVE-2020-27845: Crafted input can cause out-of-bounds-read.
