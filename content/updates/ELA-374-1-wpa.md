---
title: "ELA-374-1 wpa security update"
package: wpa
version: 2.3-1+deb8u13
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-03-03T02:41:57+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-27803

---

A vulnerability was discovered in how p2p/p2p_pd.c in wpa_supplicant
before 2.10 processes P2P (Wi-Fi Direct) provision discovery requests.
It could result in denial of service or other impact (potentially
execution of arbitrary code), for an attacker within radio range.
