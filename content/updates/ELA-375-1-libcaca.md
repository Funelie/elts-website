---
title: "ELA-375-1 libcaca security update"
package: libcaca
version: 0.99.beta19-2+deb8u2
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2021-03-08T00:56:16+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3410

---

A buffer overflow issue in caca_resize function in 
libcaca/caca/canvas.c may lead to local execution of arbitrary code in 
the user context.
