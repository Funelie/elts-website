---
title: "ELA-377-1 pygments security update"
package: pygments
version: 2.0.1+dfsg-1.1+deb8u2
distribution: "Debian 8 jessie"
description: "cpu exhaustion"
date: 2021-03-12T11:11:24+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-20270

---

It was found that pygments, a generic syntax highlighter, is vulnerable
to a CPU exhaustion attack via a crafted SML file.
