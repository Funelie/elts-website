---
title: "ELA-378-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.258-1~deb8u1
distribution: "Debian 8 jessie"
description: "linux kernel update"
date: 2021-03-12T17:32:25+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-19318
  - CVE-2019-19813
  - CVE-2019-19816
  - CVE-2020-27815
  - CVE-2020-27825
  - CVE-2020-28374
  - CVE-2020-29568
  - CVE-2020-29569
  - CVE-2020-29660
  - CVE-2020-29661
  - CVE-2020-36158
  - CVE-2021-3178
  - CVE-2021-3347
  - CVE-2021-26930
  - CVE-2021-26931
  - CVE-2021-26932
  - CVE-2021-27363
  - CVE-2021-27364
  - CVE-2021-27365
  - CVE-2021-28038

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2019-19318, CVE-2019-19813, CVE-2019-19816

    "Team bobfuzzer" reported bugs in Btrfs that could lead to a
    use-after-free or heap buffer overflow, and could be triggered by
    crafted filesystem images.  A user permitted to mount and access
    arbitrary filesystems could use these to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.

CVE-2020-27815

    A flaw was reported in the JFS filesystem code allowing a local
    attacker with the ability to set extended attributes to cause a
    denial of service.

CVE-2020-27825

    Adam 'pi3' Zabrocki reported a use-after-free flaw in the ftrace
    ring buffer resizing logic due to a race condition, which could
    result in denial of service or information leak.

CVE-2020-28374

    David Disseldorp discovered that the LIO SCSI target implementation
    performed insufficient checking in certain XCOPY requests. An
    attacker with access to a LUN and knowledge of Unit Serial Number
    assignments can take advantage of this flaw to read and write to any
    LIO backstore, regardless of the SCSI transport settings.

CVE-2020-29568 (XSA-349)

    Michael Kurth and Pawel Wieczorkiewicz reported that frontends can
    trigger OOM in backends by updating a watched path.

CVE-2020-29569 (XSA-350)

    Olivier Benjamin and Pawel Wieczorkiewicz reported a use-after-free
    flaw which can be triggered by a block frontend in Linux blkback. A
    misbehaving guest can trigger a dom0 crash by continuously
    connecting / disconnecting a block frontend.

CVE-2020-29660

    Jann Horn reported a locking inconsistency issue in the tty
    subsystem which may allow a local attacker to mount a
    read-after-free attack against TIOCGSID.

CVE-2020-29661

    Jann Horn reported a locking issue in the tty subsystem which can
    result in a use-after-free. A local attacker can take advantage of
    this flaw for memory corruption or privilege escalation.

CVE-2020-36158

    A buffer overflow flaw was discovered in the mwifiex WiFi driver
    which could result in denial of service or the execution of
    arbitrary code via a long SSID value.

CVE-2021-3178

    吴异 reported an information leak in the NFSv3 server.  When only
    a subdirectory of a filesystem volume is exported, an NFS client
    listing the exported directory would obtain a file handle to the
    parent directory, allowing it to access files that were not meant
    to be exported.

    Even after this update, it is still possible for NFSv3 clients to
    guess valid file handles and access files outside an exported
    subdirectory, unless the "subtree_check" export option is enabled.
    It is recommended that you do not use that option but only export
    whole filesystem volumes.

CVE-2021-3347

    It was discovered that PI futexes have a kernel stack use-after-free
    during fault handling. An unprivileged user could use this flaw to
    crash the kernel (resulting in denial of service) or for privilege
    escalation.

CVE-2021-26930 (XSA-365)

    Olivier Benjamin, Norbert Manthey, Martin Mazein, and Jan
    H. Schönherr discovered that the Xen block backend driver
    (xen-blkback) did not handle grant mapping errors correctly.  A
    malicious guest could exploit this bug to cause a denial of
    service (crash), or possibly an information leak or privilege
    escalation, within the domain running the backend, which is
    typically dom0.

CVE-2021-26931 (XSA-362), CVE-2021-26932 (XSA-361), CVE-2021-28038 (XSA-367)

    Jan Beulich discovered that the Xen support code and various Xen
    backend drivers did not handle grant mapping errors correctly.  A
    malicious guest could exploit these bugs to cause a denial of
    service (crash) within the domain running the backend, which is
    typically dom0.

CVE-2021-27363

    Adam Nichols reported that the iSCSI initiator subsystem did not
    properly restrict access to transport handle attributes in sysfs.
    On a system acting as an iSCSI initiator, this is an information
    leak to local users and makes it easier to exploit CVE-2021-27364.

CVE-2021-27364

    Adam Nichols reported that the iSCSI initiator subsystem did not
    properly restrict access to its netlink management interface.  On
    a system acting as an iSCSI initiator, a local user could use
    these to cause a denial of service (disconnection of storage) or
    possibly for privilege escalation.

CVE-2021-27365

    Adam Nichols reported that the iSCSI initiator subsystem did not
    correctly limit the lengths of parameters or "passthrough PDUs"
    sent through its netlink management interface.  On a system acting
    as an iSCSI initiator, a local user could use these to leak the
    contents of kernel memory, to cause a denial of service (kernel
    memory corruption or crash), and probably for privilege
    escalation.
