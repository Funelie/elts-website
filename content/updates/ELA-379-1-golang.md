---
title: "ELA-379-1 golang security update"
package: golang
version: 2:1.3.3-1+deb8u3
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-03-13T18:35:59+01:00
draft: false
type: updates
cvelist:
  - CVE-2015-5739
  - CVE-2016-3959
  - CVE-2017-15041
  - CVE-2017-1000098
  - CVE-2018-7187
  - CVE-2018-16873
  - CVE-2018-16874
  - CVE-2019-16276
  - CVE-2019-17596
  - CVE-2020-16845
  - CVE-2021-3114

---

Several vulnerabilities were discovered in the Go programming
language. An attacker could trigger a denial-of-service (DoS), bypasss
access control, and execute arbitrary code on the developer's
computer.

* CVE-2016-3959

    The Verify function in crypto/dsa/dsa.go does not properly check
    parameters passed to the big integer library, which might allow
    remote attackers to cause a denial of service (infinite loop) via
    a crafted public key to a program that uses HTTPS client
    certificates or SSH server libraries.

* CVE-2017-15041

    Go allows "go get" remote command execution. Using custom domains,
    it is possible to arrange things so that example.com/pkg1 points
    to a Subversion repository but example.com/pkg1/pkg2 points to a
    Git repository. If the Subversion repository includes a Git
    checkout in its pkg2 directory and some other work is done to
    ensure the proper ordering of operations, "go get" can be tricked
    into reusing this Git checkout for the fetch of code from pkg2. If
    the Subversion repository's Git checkout has malicious commands in
    .git/hooks/, they will execute on the system running "go get."

* CVE-2017-1000098

    The net/http package's Request.ParseMultipartForm method starts
    writing to temporary files once the request body size surpasses
    the given "maxMemory" limit. It was possible for an attacker to
    generate a multipart request crafted such that the server ran out
    of file descriptors.

* CVE-2018-7187

    The "go get" implementation in Go 1.9.4, when the -insecure
    command-line option is used, does not validate the import path
    (get/vcs.go only checks for "://" anywhere in the string), which
    allows remote attackers to execute arbitrary OS commands via a
    crafted web site.

* CVE-2018-16873

    The "go get" command is vulnerable to remote code execution when
    executed with the -u flag and the import path of a malicious Go
    package, as it may treat the parent directory as a Git repository
    root, containing malicious configuration.

* CVE-2018-16874

    The "go get" command is vulnerable to directory traversal when
    executed with the import path of a malicious Go package which
    contains curly braces (both '{' and '}' characters). The attacker
    can cause an arbitrary filesystem write, which can lead to code
    execution.

* CVE-2015-5739

     The net/http library in net/textproto/reader.go does not properly
     parse HTTP header keys, which allows remote attackers to conduct
     HTTP request smuggling attacks via a space instead of a hyphen,
     as demonstrated by "Content Length" instead of "Content-Length."

* CVE-2019-16276

    Go allows HTTP Request Smuggling.

* CVE-2019-17596

    Go can panic upon an attempt to process network traffic containing
    an invalid DSA public key. There are several attack scenarios,
    such as traffic from a client to a server that verifies client
    certificates.

* CVE-2020-16845

    Go can have an infinite read loop in ReadUvarint and ReadVarint in
    encoding/binary via invalid inputs.

* CVE-2021-3114

    crypto/elliptic/p224.go can generate incorrect outputs, related to
    an underflow of the lowest limb during the final complete
    reduction in the P-224 field.
