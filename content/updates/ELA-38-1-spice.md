---
title: "ELA-38-1 spice security update"
package: spice
version: 0.11.0-1+deb7u5
distribution: "Debian 7 Wheezy"
description: "flexible array buffer overflow"
date: 2018-09-16T21:10:18Z
draft: false
type: updates
cvelist:
  - CVE-2018-10873
---

A vulnerability was discovered in SPICE before version 0.14.1 where the
generated code used for demarshalling messages lacked sufficient bounds
checks. A malicious client or server, after authentication, could send
specially crafted messages to its peer which would result in a crash or,
potentially, other impacts.

The issue has been fixed by upstream by bailing out with an error if the
pointer to the start of some message data is strictly greater than the
pointer to the end of the  message data.
