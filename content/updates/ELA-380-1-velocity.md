---
title: "ELA-380-1 velocity security update"
package: velocity
version: 1.7-4+deb8u1
distribution: "Debian 8 Jessie"
description: "arbitrary code execution vulnerability"
date: 2021-03-17T12:39:01+00:00
draft: false
type: updates
cvelist:
  - CVE-2020-13936
---

It was discovered that there was a potential arbitrary code execution
vulnerability in Velocity, a Java-based template engine for writing web
applications. It could be exploited by applications which allowed untrusted
users to upload/modify templates.
