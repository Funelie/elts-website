---
title: "ELA-381-1 velocity-tools security update"
package: velocity-tools
version: 2.0-3+deb8u1
distribution: "Debian 8 Jessie"
description: "cross-site scripting (XSS) vulnerability"
date: 2021-03-17T16:38:10+00:00
draft: false
type: updates
cvelist:
  - CVE-2020-13959
---

It was discovered that there was a cross-site scripting (XSS) vulnerability in
`velocity-tools`, a collection of useful tools for the "Velocity" template
engine.

The default error page could be exploited to steal session cookies, perform
requests in the name of the victim, used for phishing attacks and many other
similar attacks.
