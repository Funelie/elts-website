---
title: "ELA-382-1 squid3 security update"
package: squid3
version: 3.5.23-5+deb8u3
distribution: "Debian 8 jessie"
description: "HTTP Request Smuggling"
date: 2021-03-19T03:51:49+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-25097

---

Due to improper input validation, Squid is vulnerable to an HTTP
Request Smuggling attack.

This problem allows a trusted client to perform HTTP Request
Smuggling and access services otherwise forbidden by Squid
security controls.
