---
title: "ELA-384-1 pygments security update"
package: pygments
version: 2.0.1+dfsg-1.1+deb8u3
distribution: "Debian 8 Jessie"
description: "denial of service (DoS) vulnerability"
date: 2021-03-19T17:29:50+00:00
draft: false
type: updates
cvelist:
  - CVE-2021-27291
---

It was discovered that there was a series of denial of service vulnerabilities
in Pygments, a popular syntax highlighting library for Python.

A number of regular expressions had exponential or cubic worst-case complexity
which could cause a remote denial of service (DoS) when provided with malicious
input.
