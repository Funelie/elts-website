---
title: "ELA-388-1 zeromq3 security update"
package: zeromq3
version: 4.0.5+dfsg-2+deb8u4
distribution: "Debian 8 jessie"
description: "memory leak in client"
date: 2021-03-22T15:07:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-20234

---

An issue has been found in zeromq3, a lightweight messaging kernel.
When processing a delimiter in a pipe, that is not in an active state but still contains a message, this message is leaked.
