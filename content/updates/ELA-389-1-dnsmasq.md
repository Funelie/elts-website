---
title: "ELA-389-1 dnsmasq security update"
package: dnsmasq
version: 2.72-3+deb8u6
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-03-22T19:11:20+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-25681
  - CVE-2020-25682
  - CVE-2020-25683
  - CVE-2020-25684
  - CVE-2020-25687

---

Moshe Kol and Shlomi Oberman of JSOF discovered several
vulnerabilities in dnsmasq, a small caching DNS proxy and DHCP/TFTP
server. They could result in denial of service, cache poisoning or the
execution of arbitrary code.