---
title: "ELA-391-1 jasper security update"
package: jasper
version: 1.900.1-debian1-2.4+deb8u10
distribution: "Debian 8 jessie"
description: "null pointer dereference and missing checks"
date: 2021-03-27T17:34:27+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3443
  - CVE-2021-3467

---

Two issues have been found in jasper, a JPEG-2000 runtime library.
Both issues are related to jpeg 2000 decoding, where a null pointer
dereference and a missing check of valid component numbers referenced
by CDEF box, could be exploited.
