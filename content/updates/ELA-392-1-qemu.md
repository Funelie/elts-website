---
title: "ELA-392-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u20
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-03-31T19:07:00+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3409
  - CVE-2021-3416
  - CVE-2021-20203
  - CVE-2021-20255
  - CVE-2021-20257

---

Several security vulnerabilities have been discovered in QEMU, a fast processor
emulator.

CVE-2021-20257

    net: e1000: infinite loop while processing transmit descriptors


CVE-2021-20255

    A stack overflow via an infinite recursion vulnerability was found in the
    eepro100 i8255x device emulator of QEMU. This issue occurs while processing
    controller commands due to a DMA reentry issue. This flaw allows a guest
    user or process to consume CPU cycles or crash the QEMU process on the
    host, resulting in a denial of service.

CVE-2021-20203

    An integer overflow issue was found in the vmxnet3 NIC emulator of the QEMU
    for versions up to v5.2.0. It may occur if a guest was to supply invalid
    values for rx/tx queue size or other NIC parameters. A privileged guest
    user may use this flaw to crash the QEMU process on the host resulting in
    DoS scenario.

CVE-2021-3416

    A potential stack overflow via infinite loop issue was found in various NIC
    emulators of QEMU in versions up to and including 5.2.0. The issue occurs
    in loopback mode of a NIC wherein reentrant DMA checks get bypassed. A
    guest user/process may use this flaw to consume CPU cycles or crash the
    QEMU process on the host resulting in DoS scenario.

CVE-2021-3409

    The patch for CVE-2020-17380/CVE-2020-25085 was found to be ineffective,
    thus making QEMU vulnerable to the out-of-bounds read/write access issues
    previously found in the SDHCI controller emulation code. This flaw allows a
    malicious privileged guest to crash the QEMU process on the host, resulting
    in a denial of service or potential code execution.


