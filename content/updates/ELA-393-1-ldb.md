---
title: "ELA-393-1 ldb security update"
package: ldb
version: 2:1.1.20-0+deb8u3
distribution: "Debian 8 jessie"
description: "out of bounds access"
date: 2021-03-31T11:07:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-27840
  - CVE-2021-20277

---

Two issues have been found in ldb, an LDAP-like embedded database, for example used with samba.

Both issues are related to out of bounds access, either an out of bound read or a heap corrupton, both most likely leading to an application crash.
