---
title: "ELA-394-1 leptonlib security update"
package: leptonlib
version: 1.71-2.1+deb8u1
distribution: "Debian 8 jessie"
description: "heap-based buffer over-read"
date: 2021-03-31T11:47:29+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-36277
  - CVE-2020-36278
  - CVE-2020-36279
  - CVE-2020-36281

---

Several issues have been found in leptonlib, an image processing library.

All issues are related to heap-based buffer over-read in several functions or a denial of service (application crash) with crafted data.
