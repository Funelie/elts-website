---
title: "ELA-395-1 busybox security update"
package: busybox
version: 1:1.22.0-9+deb8u5
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-04-01T20:09:27+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-28831

---

The gunzip decompressor of Busybox, tiny utilities for small and embedded
systems, mishandled the error bit on the huft_build result pointer, with a
resultant invalid free or segmentation fault, via malformed gzip data.
