---
title: "ELA-396-1 underscore security update"
package: underscore
version: 1.7.0~dfsg-1+deb8u1
distribution: "Debian 8 jessie"
description: "arbitrary code execution"
date: 2021-04-02T02:45:58+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-23358

---

It was discovered that missing input sanitising in the template()
function of the Underscore JavaScript library could result in the
execution of arbitrary code.
