---
title: "ELA-397-1 spamassassin security update"
package: spamassassin
version: 3.4.2-0+deb8u4
distribution: "Debian 8 jessie"
description: "arbitrary code execution"
date: 2021-04-02T04:05:03+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-1946

---

Damian Lukowski discovered a flaw in spamassassin, a Perl-based spam
filter using text analysis. Malicious rule configuration files, possibly
downloaded from an updates server, could execute arbitrary commands
under multiple scenarios.
