---
title: "ELA-4-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb7u5
distribution: "Debian 7 Wheezy"
description: "possible DoS by a malicious server"
date: 2018-06-23T15:47:39+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-0732
---

Possible DoS by a malicious server that sends a very large prime value to the client during TLS handshake.
