---
title: "ELA-400-1 wordpress security update"
package: wordpress
version: 4.1.32+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-05T03:35:41+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-25286
  - CVE-2020-28032
  - CVE-2020-28033
  - CVE-2020-28034
  - CVE-2020-28035
  - CVE-2020-28036
  - CVE-2020-28037
  - CVE-2020-28038
  - CVE-2020-28039
  - CVE-2020-28040

---

There were several vulnerabilites reported against wordpress,
as follows:

CVE-2020-25286

    In wp-includes/comment-template.php in WordPress before 4.1.32
    comments from a post or page could sometimes be seen in the
    latest comments even if the post or page was not public.

CVE-2020-28032

    WordPress before 4.1.32 mishandles deserialization requests in
    wp-includes/Requests/Utility/FilteredIterator.php.

CVE-2020-28033

    WordPress before 4.1.32 mishandles embeds from disabled sites
    on a multisite network, as demonstrated by allowing a spam
    embed.

CVE-2020-28034

    WordPress before 4.1.32 allows XSS associated with global
    variables.

CVE-2020-28035

    WordPress before 4.1.32 allows attackers to gain privileges via
    XML-RPC.

CVE-2020-28036

    wp-includes/class-wp-xmlrpc-server.php in WordPress before
    4.1.32 allows attackers to gain privileges by using XML-RPC to
    comment on a post.

CVE-2020-28037

    is_blog_installed in wp-includes/functions.php in WordPress
    before 4.1.32 improperly determines whether WordPress is
    already installed, which might allow an attacker to perform
    a new installation, leading to remote code execution (as well
    as a denial of service for the old installation).

CVE-2020-28038

    WordPress before 4.1.32 allows stored XSS via post slugs.

CVE-2020-28039

    is_protected_meta in wp-includes/meta.php in WordPress before
    4.1.32 allows arbitrary file deletion because it does not
    properly determine whether a meta key is considered protected.

CVE-2020-28040

    WordPress before 4.1.32 allows CSRF attacks that change a
    theme's background image.
