---
title: "ELA-401-1 tomcat7 security update"
package: tomcat7
version: 7.0.56-3+really7.0.100-1+deb8u3
distribution: "Debian 8 jessie"
description: "possible remote code execution"
date: 2021-04-09T12:46:10+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-25329

---

It was discovered that the fix for CVE-2020-9484 was incomplete. When using
Apache Tomcat with a configuration edge case that was highly unlikely to be
used, the Tomcat instance was still vulnerable to CVE-2020-9484. Note that both
the previously published prerequisites for CVE-2020-9484 and the previously
published mitigations for CVE-2020-9484 also apply to this issue.

For reference the original advisory text follows.

When using Apache Tomcat if a) an attacker is able to control the contents and
name of a file on the server; and b) the server is configured to use the
PersistenceManager with a FileStore; and c) the PersistenceManager is
configured with sessionAttributeValueClassNameFilter=“null” (the default unless
a SecurityManager is used) or a sufficiently lax filter to allow the attacker
provided object to be deserialized; and d) the attacker knows the relative file
path from the storage location used by FileStore to the file the attacker has
control over; then, using a specifically crafted request, the attacker will be
able to trigger remote code execution via deserialization of the file under
their control. Note that all of conditions a) to d) must be true for the attack
to succeed.
