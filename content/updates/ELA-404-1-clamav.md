---
title: "ELA-404-1 clamav security update"
package: clamav
version: 0.102.4+dfsg-0+deb8u2
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-04-14T14:20:09+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-1405

---

A vulnerability in the email parsing module in Clam AntiVirus
(ClamAV) Software version 0.103.1 and all prior versions could
allow an unauthenticated, remote attacker to cause a denial of
service condition on an affected device. The vulnerability is
due to improper variable initialization that may result in an
NULL pointer read. An attacker could exploit this vulnerability
by sending a crafted email to an affected device. An exploit
could allow the attacker to cause the ClamAV scanning process
crash, resulting in a denial of service condition.
