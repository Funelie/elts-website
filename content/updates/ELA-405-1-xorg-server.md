---
title: "ELA-405-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u5
distribution: "Debian 8 Jessie"
description: "Input validation vulnerability"
date: 2021-04-15T11:33:32+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3472
---

Jan-Niklas Sohn discovered that there was an input validation failure in the
[X.Org](https://www.x.org/wiki/) display server.

Insufficient checks on the lengths of the XInput extension's
ChangeFeedbackControl request could have lead to out of bounds memory accesses
in the X server. These issues can lead to privilege escalation for authorised
clients, particularly on systems where the X server is running as a privileged
user.
