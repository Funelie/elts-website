---
title: "ELA-406-1 zabbix security update"
package: zabbix
version: 1:2.2.23+dfsg-0+deb8u2
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-21T16:32:29+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-15132
  - CVE-2020-11800
  - CVE-2020-15803

---

Multiple vulnerabilities were discovered in Zabbix, a network
monitoring solution. An attacker may remotely execute code on the
zabbix server, enumerate valid users and redirect to external links
through the zabbix web frontend.

* CVE-2019-15132

    Zabbix allows User Enumeration. With login requests, it is
    possible to enumerate application usernames based on the
    variability of server responses (e.g., the "Login name or password
    is incorrect" and "No permissions for system access" messages, or
    just blocking for a number of seconds). This affects both
    api_jsonrpc.php and index.php.

* CVE-2020-11800

    Zabbix allows remote attackers to execute arbitrary code on the
    Zabbix server.

* CVE-2020-15803

    Zabbix allows stored XSS in the URL Widget.
