---
title: "ELA-407-1 openjdk-8 security update"
package: openjdk-8
version: 8u292-b10-0+deb8u1
distribution: "Debian 8 jessie"
description: "sandbox bypass"
date: 2021-04-23T13:34:15+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-2161
  - CVE-2021-2163

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in bypass of sandbox restrictions.
