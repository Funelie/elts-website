---
title: "ELA-408-1 libspring-java security update"
package: libspring-java
version: 3.0.6.RELEASE-17+deb8u2
distribution: "Debian 8 jessie"
description: "XST attack"
date: 2021-04-23T20:02:03+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-11039

---

The Spring Framework allows web applications to change the HTTP
request method to any HTTP method (including TRACE) using the
HiddenHttpMethodFilter in Spring MVC. If an application has a
pre-existing XSS vulnerability, a malicious user (or attacker) can use
this filter to escalate to an XST (Cross Site Tracing) attack.
