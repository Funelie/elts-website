---
title: "ELA-41-1 lcms security update"
package: lcms
version: 1.19.dfsg2-1.2+deb7u2
distribution: "Debian 7 Wheezy"
description: "heap-based buffer overflow"
date: 2018-09-18T02:33:49Z
draft: false
type: updates
cvelist:
  - CVE-2018-16435
---

Little CMS (aka Little Color Management System) has an integer overflow in the
AllocateDataSet function in cmscgats.c, leading to a heap-based buffer overflow
in the SetData function via a crafted file in the second argument to
cmsIT8LoadFromFile.
