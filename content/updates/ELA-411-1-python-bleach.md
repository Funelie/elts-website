---
title: "ELA-411-1 python-bleach security update"
package: python-bleach
version: 1.4-1+deb8u2
distribution: "Debian 8 jessie"
description: "mutation XSS"
date: 2021-04-26T18:49:41+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-23980

---

It was discovered that python-bleach, a whitelist-based
HTML-sanitizing library for the Python language, is prone to a
mutation XSS vulnerability in bleach.clean when 'svg' or 'math' are in
the allowed tags, 'p' or 'br' are in allowed tags, 'style', 'title',
'noscript', 'script', 'textarea', 'noframes', 'iframe', or 'xmp' are
in allowed tags and 'strip_comments=False' is set.
