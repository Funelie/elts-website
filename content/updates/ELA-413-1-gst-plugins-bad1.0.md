---
title: "ELA-413-1 gst-plugins-bad1.0 security update"
package: gst-plugins-bad1.0
version: 1.4.4-2.1+deb8u4
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-27T12:45:39+02:00
draft: false
type: updates
cvelist:

---

Multiple vulnerabilities were discovered in plugins for the GStreamer
media framework, which may result in denial of service or potentially
the execution of arbitrary code if a malformed media file is opened.
