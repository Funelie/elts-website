---
title: "ELA-416-1 gst-plugins-base0.10 security update"
package: gst-plugins-base0.10
version: 0.10.36-2+deb8u3
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-04-27T13:05:11+02:00
draft: false
type: updates
cvelist:

---

Multiple vulnerabilities were discovered in plugins for the GStreamer
media framework, which may result in denial of service or potentially
the execution of arbitrary code if a malformed media file is opened.
