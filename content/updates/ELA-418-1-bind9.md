---
title: "ELA-418-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u22
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2021-05-04T12:25:25+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-25214
  - CVE-2021-25215
  - CVE-2021-25216

---

Several vulnerabilities were discovered in BIND, a DNS server
implementation.

CVE-2021-25214

    Greg Kuechle discovered that a malformed incoming IXFR transfer
    could trigger an assertion failure in named, resulting in denial
    of service.

CVE-2021-25215

    Siva Kakarla discovered that named could crash when a DNAME record
    placed in the ANSWER section during DNAME chasing turned out to be
    the final answer to a client query.

CVE-2021-25216

    It was discovered that the SPNEGO implementation used by BIND is
    prone to a buffer overflow vulnerability. This update switches to
    use the SPNEGO implementation from the Kerberos libraries.
