---
title: "ELA-419-1 imagemagick security update"
package: imagemagick
version: 8:6.8.9.9-5+deb8u24
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-05-04T15:17:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-20312

---

A flaw was found in ImageMagick, where an integer overflow in
WriteTHUMBNAILImage of coders/thumbnail.c may trigger undefined behavior via a
crafted image file that is submitted by an attacker and processed by an
application using ImageMagick. This could lead to a denial-of-service.
