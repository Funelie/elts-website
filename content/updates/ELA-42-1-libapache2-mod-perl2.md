---
title: "ELA-42-1 libapache2-mod-perl2 security update"
package: libapache2-mod-perl2
version: 2.0.7-3+deb7u1
distribution: "Debian 7 Wheezy"
description: "arbitrary code execution"
date: 2018-09-18T19:49:54+02:00
draft: false
type: updates
cvelist:
  - CVE-2011-2767
---

Jan Ingvoldstad discovered that libapache2-mod-perl2 allows attackers to
execute arbitrary Perl code by placing it in a user-owned .htaccess file,
because (contrary to the documentation) there is no configuration option that
permits Perl code for the administrator's control of HTTP request processing
without also permitting unprivileged users to run Perl code in the context of
the user account that runs Apache HTTP Server processes.

This update requires a restart of the Apache 2 web server to take effect.
