---
title: "ELA-420-1 exim4 security update"
package: exim4
version: 4.84.2-2+deb8u8
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2021-05-05T16:23:55+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-28007
  - CVE-2020-28008
  - CVE-2020-28009
  - CVE-2020-28011
  - CVE-2020-28012
  - CVE-2020-28013
  - CVE-2020-28014
  - CVE-2020-28015
  - CVE-2020-28017
  - CVE-2020-28020
  - CVE-2020-28021
  - CVE-2020-28022
  - CVE-2020-28024
  - CVE-2020-28025

---

The Qualys Research Labs reported several vulnerabilities in Exim, a mail
transport agent, which could result in local privilege escalation and
remote code execution.

Details can be found in the Qualys advisory at
https://www.qualys.com/2021/05/04/21nails/21nails.txt

