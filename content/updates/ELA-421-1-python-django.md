---
title: "ELA-421-1 python-django security update"
package: python-django
version: 1.7.11-1+deb8u13
distribution: "Debian 8 Jessie"
description: "Directory-traversal vulnerability"
date: 2021-05-06T12:51:47+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-31542
---


It was discovered that there was potential directory-traversal vulnerability in
Django, a popular Python-based web development framework.

The `MultiPartParser`, `UploadedFile` and `FieldFile` classes allowed
directory-traversal via uploaded files with suitably crafted file names. In
order to mitigate this risk, stricter `basename` and path sanitation is now
applied. Specifically, empty file names and paths with dot segments are
rejected.
