---
title: "ELA-422-1 samba security update"
package: samba
version: 2:4.2.14+dfsg-0+deb8u15
distribution: "Debian 8 jessie"
description: "unauthorized access"
date: 2021-05-08T20:00:08+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-20254

---

Peter Eriksson of Linköping University discovered a flaw in the smbd file
server which maps Windows group identities (SIDs) into unix group ids (gids).
The code that performs this had a flaw that could allow it to read data beyond
the end of the array in the case where a negative cache entry had been added to
the mapping cache. This could cause the calling code to return those values
into the process token that stores the group membership for a user. The
vulnerability could allow unauthorized access to files.
