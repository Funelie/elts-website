---
title: "ELA-424-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u10
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-05-10T14:30:07+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3517
  - CVE-2021-3518
  - CVE-2021-3537

---

Several vulnerabilities were discovered in libxml2, a library providing
support to read, modify and write XML and HTML files, which could cause
denial of service via application crash when parsing specially crafted
files.
