---
title: "ELA-425-1 rails security update"
package: rails
version: 2:4.1.8-1+deb8u9
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-05-12T02:17:10+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-22885
  - CVE-2021-22904

---

CVE-2021-22885

    There is a possible information disclosure/unintended method
    execution vulnerability in Action Pack when using the
    `redirect_to` or `polymorphic_url` helper with untrusted user
    input.

CVE-2021-22904

    There is a possible DoS vulnerability in the Token Authentication
    logic in Action Controller. Impacted code uses
    `authenticate_or_request_with_http_token` or
    `authenticate_with_http_token` for request authentication.
