---
title: "ELA-426-1 hivex security update"
package: hivex
version: 1.3.10-2+deb8u3
distribution: "Debian 8 jessie"
description: "out of bounds access"
date: 2021-05-12T03:31:32+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3504

---

Jemery Galindo discovered an out-of-bounds memory access in Hivex, a
library to parse Windows Registry hive files.
