---
title: "ELA-430-1 libimage-exiftool-perl security update"
package: libimage-exiftool-perl
version: 9.74-1+deb8u1
distribution: "Debian 8 jessie"
description: "arbitrary code execution"
date: 2021-05-16T15:05:01+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-22204

---

A vulnerability was discovered in libimage-exiftool-perl, a library and
program to read and write meta information in multimedia files, which
may result in execution of arbitrary code if a malformed DjVu file is
processed.
