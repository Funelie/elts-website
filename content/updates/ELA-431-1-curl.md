---
title: "ELA-431-1 curl security update"
package: curl
version: 7.38.0-4+deb8u20
distribution: "Debian 8 jessie"
description: "information disclosure"
date: 2021-05-17T16:18:16+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-22876

---

Viktor Szakats reported that libcurl, an URL transfer library, does
not strip off user credentials from the URL when automatically
populating the Referer HTTP request header field in outgoing HTTP
requests. Sensitive authentication data may leak to the server that is
the target of the second HTTP request.
