---
title: "ELA-432-1 tomcat7 bug fix update"
package: tomcat7
version: 7.0.56-3+really7.0.109-1
distribution: "Debian 8 jessie"
description: "BZ 64021 bug fix"
date: 2021-05-22T19:19:54+02:00
draft: false
type: updates
cvelist:

---

This update of the Tomcat 7 Servlet and JSP engine fixes upstream bug
[BZ 64021](https://bz.apache.org/bugzilla/show_bug.cgi?id=64021).
Under certain circumstances the ServletContainerInitializer (SCI)
of a web application was unable to find Tomcat's WebSocket support as the
latter was initialised after any application SCIs. This could prevent services
from being executed.

We recommend that you upgrade your Tomcat 7 packages if you are affected by
upstream bug BZ 64021. Otherwise an upgrade is not necessary.
