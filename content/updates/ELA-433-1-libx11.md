---
title: "ELA-433-1 libx11 security update"
package: libx11
version: 2:1.6.2-3+deb8u5
distribution: "Debian 8 jessie"
description: "protocol command injection"
date: 2021-05-24T08:51:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-31535

---

Roman Fiedler found that libX11, the X11 protocol client library, was
vulnerable to protocol command injection due to insufficient validation
of arguments to some functions.
