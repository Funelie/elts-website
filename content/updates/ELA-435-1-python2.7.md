---
title: "ELA-435-1 python2.7 security update"
package: python2.7
version: 2.7.9-2-ds1-1+deb8u7
distribution: "Debian 8 jessie"
description: "bad handling of query string separators"
date: 2021-05-30T00:00:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-23336

---

An issue has been found in Python2.7, an interactive high-level object-oriented language.

Python2.7 is vulnerable to Web Cache Poisoning via urllib.parse.parse_qsl and urllib.parse.parse_qs by using a vector called parameter cloaking. When the attacker can separate query parameters using a semicolon (;), they can cause a difference in the interpretation of the request between the proxy (running with default configuration) and the server. This can result in malicious requests being cached as completely safe ones, as the proxy would usually not see the semicolon as a separator, and therefore would not include it in a cache key of an unkeyed parameter.

**Attention, API-change!** Please be sure your software is working properly if it uses `urllib.parse.parse_qs` or `urllib.parse.parse_qsl`, `cgi.parse` or `cgi.parse_multipart`.

Earlier Python versions allowed using both ``;`` and ``&`` as query parameter separators in `urllib.parse.parse_qs` and `urllib.parse.parse_qsl`. Due to security concerns, and to conform with newer W3C recommendations, this has been changed to allow only a single separator key, with ``&`` as the default. This change also affects `cgi.parse` and `cgi.parse_multipart` as they use the affected functions internally. For more details, please see their respective documentation.
