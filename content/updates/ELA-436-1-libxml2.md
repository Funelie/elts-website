---
title: "ELA-436-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u11
distribution: "Debian 8 jessie"
description: "Parameter Laughs"
date: 2021-05-30T09:58:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3541

---

An issue has been found in libxml2, the GNOME XML library.

This issue is called "Parameter Laughs"-attack and is related to parameter
entities expansion.
It is similar to the "Billion Laughs"-attacks found earlier in libexpat.
More information can be found at [1]


[1] https://blog.hartwork.org/posts/cve-2021-3541-parameter-laughs-fixed-in-libxml2-2-9-11/
