---
title: "ELA-437-1 nginx security update"
package: nginx
version: 1.6.2-5+deb8u8
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-05-30T18:22:59+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-23017

---

Luis Merino, Markus Vervier and Eric Sesterhenn discovered an off-by-one
in Nginx, a high-performance web and reverse proxy server, which could
result in denial of service and potentially the execution of arbitrary
code.
