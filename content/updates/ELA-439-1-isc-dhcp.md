---
title: "ELA-439-1 isc-dhcp security update"
package: isc-dhcp
version: 4.3.1-6+deb8u5
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-06-03T14:03:22+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-25217

---

Jon Franklin and Pawel Wieczorkiewicz found an issue in the ISC DHCP
client and server when parsing lease information, which could lead to
denial of service via application crash.
