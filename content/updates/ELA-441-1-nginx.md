---
title: "ELA-441-1 nginx security update"
package: nginx
version: 1.6.2-5+deb8u9
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2021-06-07T23:05:22+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-20005

---

Jamie Landeg-Jones and Manfred Paul discovered a buffer overflow vulnerability
in NGINX, a small, powerful, scalable web/proxy server.

NGINX has a buffer overflow for years that exceed four digits, as demonstrated
by a file with a modification date in 1969 that causes an integer overflow (or
a false modification date far in the future), when encountered by the autoindex
module.
