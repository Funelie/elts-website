---
title: "ELA-443-1 jetty security update"
package: jetty
version: 6.1.26-4+deb8u2
distribution: "Debian 8 jessie"
description: "information disclosure"
date: 2021-06-17T20:06:20+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-28169

---

Steven Seeley discovered that in jetty, a Java servlet engine and
webserver, requests to the ConcatServlet and WelcomeFilter are able to
access protected resources within the WEB-INF directory. An attacker
may access sensitive information regarding the implementation of a web
application.

This update also improves the fix to CVE-2017-9735 to cover more
timing attacks.
