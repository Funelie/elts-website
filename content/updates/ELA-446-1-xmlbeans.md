---
title: "ELA-446-1 xmlbeans security update"
package: xmlbeans
version: 2.6.0-2+deb8u1
distribution: "Debian 8 jessie"
description: "XML Entity Expansion attack"
date: 2021-06-28T00:07:33+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-23926

---

The XML parsers used by XMLBeans did not set the properties needed to protect
the user from malicious XML input. Vulnerabilities include the possibility for
XML Entity Expansion attacks which could lead to a denial-of-service. This
update implements sensible defaults for the XML parsers to prevent these
kind of attacks.

