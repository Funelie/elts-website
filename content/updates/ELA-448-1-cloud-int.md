---
title: "ELA-448-1 cloud-int security update"
package: cloud-int
version: 0.7.6~bzr976-2+deb8u3
distribution: "Debian 8 jessie"
description: "logging raw passwords"
date: 2021-06-28T04:34:20+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3429

---

cloud-init has the ability to generate and set a randomized password
for system users. This functionality is enabled at runtime by
passing cloud-config data such as:

   chpasswd:
       list: |
           user1:RANDOM

When used this way, cloud-init logs the raw, unhashed password to a
world-readable local file.
