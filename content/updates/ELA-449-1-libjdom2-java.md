---
title: "ELA-449-1 libjdom2-java security update"
package: libjdom2-java
version: 2.0.6-1+deb8u1
distribution: "Debian 8 Jessie"
description: "XML External Entity (XXE) vulnerability"
date: 2021-06-29T12:49:01+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-33813
---

It was discovered that there was an XML External Entity (XXE) issue in
`libjdom2-java`, a library for reading and manipulating XML documents.
Attackers could have caused a denial of service attack via a specially-crafted
HTTP request.
