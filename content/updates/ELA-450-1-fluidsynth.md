---
title: "ELA-450-1 fluidsynth security update"
package: fluidsynth
version: 1.1.6-2+deb8u1
distribution: "Debian 8 jessie"
description: "use after free"
date: 2021-06-29T23:40:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-28421

---

A vulnerbility has been found in fluidsynth, a real-time MIDI software synthesizer.
Using a special crafted soundfont2 file, a use after free vulnerability might result in arbitrary code execution or a denial of service (DoS).
