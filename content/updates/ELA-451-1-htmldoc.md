---
title: "ELA-451-1 htmldoc security update"
package: htmldoc
version: 1.8.27-8+deb8u2
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-07-01T06:03:10+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-20308
  - CVE-2021-23158
  - CVE-2021-23165
  - CVE-2021-23180
  - CVE-2021-23191
  - CVE-2021-23206
  - CVE-2021-26252
  - CVE-2021-26259
  - CVE-2021-26948

---

A buffer overflow was discovered in HTMLDOC, a HTML processor that
generates indexed HTML, PS, and PDF, which could potentially result in
the execution of arbitrary code. In addition a number of crashes
were addressed.
