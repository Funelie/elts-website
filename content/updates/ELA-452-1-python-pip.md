---
title: "ELA-452-1 python-pip security update"
package: python-pip
version: 1.5.6-5+deb8u2
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2021-07-03T17:09:54+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3572

---

It was discovered that pip incorrectly handled unicode separators in git
references. A remote attacker could possibly use this issue to install a
different revision on a repository.
