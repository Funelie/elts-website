---
title: "ELA-453-1 openexr security update"
package: openexr
version: 1.6.1-8+deb8u2
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-07-03T18:57:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-9110
  - CVE-2017-9111
  - CVE-2017-9112
  - CVE-2017-9113
  - CVE-2017-9115
  - CVE-2017-9116
  - CVE-2017-12596
  - CVE-2020-11760
  - CVE-2020-11761
  - CVE-2020-11763
  - CVE-2020-11764
  - CVE-2021-3475
  - CVE-2021-3476
  - CVE-2021-3479

---

Several vulnerabilities were discovered in OpenEXR, a library and
tools for the OpenEXR high dynamic-range (HDR) image format. An
attacker could cause a denial of service (DoS) through application
crash and excessive memory consumption, and possibly execute code.
