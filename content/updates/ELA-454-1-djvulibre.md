---
title: "ELA-454-1 djvulibre security update"
package: djvulibre
version: 3.5.25.4-4+deb8u4
distribution: "Debian 8 jessie"
description: "crash or segfault"
date: 2021-07-04T02:12:40+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-3630

---

An out-of-bounds write vulnerability was found in DjVuLibre in
DJVU::DjVuTXT::decode() in DjVuText.cpp via a crafted djvu file
which may lead to crash and segmentation fault.
