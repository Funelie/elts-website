---
title: "ELA-455-1 libxstream-java security update"
package: libxstream-java
version: 1.4.11.1-1+deb8u3
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2021-07-05T18:28:58+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-29505

---

A vulnerability in XStream, a Java library to serialize objects to and
from XML, may allow a remote attacker to execute commands of the host
only by manipulating the processed input stream.

Note: the XStream project recommends to setup its security framework
with a whitelist limited to the minimal required types, rather than
relying on the black list (which got updated to address this
vulnerability). The project is also phasing out maintainance of the
black list, see https://x-stream.github.io/security.html .
