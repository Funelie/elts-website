---
title: "ELA-456-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u18
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-07-09T10:50:35+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-1927
  - CVE-2020-1934
  - CVE-2020-35452
  - CVE-2021-26690
  - CVE-2021-26691
  - CVE-2021-30641

---

Several vulnerabilities have been found in the Apache HTTP server, which
could result in denial of service. In addition the implementation of
the MergeSlashes option could result in unexpected behaviour.
