---
title: "ELA-457-1 php5 security update"
package: php5
version: 5.6.40+dfsg-0+deb8u14
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-07-15T10:40:42+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-9675
  - CVE-2020-7068
  - CVE-2020-7071
  - CVE-2021-21702
  - CVE-2021-21704
  - CVE-2021-21705

---

Several vulnerabilities were discovered in php5, a server-side,
HTML-embedded scripting language. An attacker could cause denial of
service (DoS), memory corruption and potentially execution of
arbitrary code, and server-side request forgery (SSRF) bypass.

* CVE-2019-9675

    phar_tar_writeheaders_int in ext/phar/tar.c has a buffer overflow
    via a long link value.

* CVE-2020-7068

    While processing PHAR files using phar extension,
    phar_parse_zipfile could be tricked into accessing freed memory,
    which could lead to a crash or information disclosure.

* CVE-2020-7071

    When validating URL with functions like filter_var($url,
    FILTER_VALIDATE_URL), PHP will accept an URL with invalid password
    as valid URL. This may lead to functions that rely on URL being
    valid to mis-parse the URL and produce wrong data as components of
    the URL.

* CVE-2021-21702

    When using SOAP extension to connect to a SOAP server, a malicious
    SOAP server could return malformed XML data as a response that
    would cause PHP to access a null pointer and thus cause a crash.

* CVE-2021-21704

    Multiple firebird issues.

* CVE-2021-21705

    SSRF bypass in FILTER_VALIDATE_URL.
