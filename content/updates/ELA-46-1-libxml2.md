---
title: "ELA-46-1 libxml2 security update"
package: libxml2
version: 2.8.0+dfsg1-7+wheezy13
distribution: "Debian 7 Wheezy"
description: "several fixes for NULL pointer dereference and LZMA memory handling"
date: 2018-09-25T22:33:28+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-14404
  - CVE-2018-14567
  - CVE-2018-9251
  - CVE-2017-18258
---

CVE-2018-14404
     Fix of a NULL pointer dereference which might result in a crash and
     thus in a denial of service.

CVE-2018-14567 and CVE-2018-9251
     Approvement in LZMA error handling which prevents an infinite loop.

CVE-2017-18258
     Limit available memory to 100MB to avoid exhaustive memory
     consumption by malicious files.

