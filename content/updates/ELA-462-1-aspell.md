---
title: "ELA-462-1 aspell security update"
package: aspell
version: 0.60.7~20110707-1.3+deb8u2
distribution: "Debian 8 jessie"
description: "heap based buffer overflow"
date: 2021-07-26T00:06:30+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-25051

---

One issue has been found in aspell, the GNU Aspell spell-checker.
It is related to a heap-based buffer overflow.
