---
title: "ELA-464-1 libsndfile security update"
package: libsndfile
version: 1.0.25-9.1+deb8u6
distribution: "Debian 8 jessie"
description: "heap buffer overflow"
date: 2021-07-30T00:28:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3246

---

An issue has been found in libsndfile, a library for reading/writing audio files.
A crafted WAV file can trigger a heap buffer overflow and might allow exectution of arbitrary code.

