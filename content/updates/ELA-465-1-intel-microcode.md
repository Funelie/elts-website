---
title: "ELA-465-1 intel-microcode security update"
package: intel-microcode
version: 3.20210608.2~deb8u2
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-08-02T04:45:38+05:30
draft: false
type: updates
cvelist:
  - CVE-2020-24489
  - CVE-2020-24511
  - CVE-2020-24512
  - CVE-2020-24513

---

This update ships updated CPU microcode for some types of Intel CPUs
and provides mitigations for security vulnerabilities which could
result in privilege escalation in combination with VT-d and various
side channel attacks.

Please note that one of the processors is not receiving this update
and so the users of 0x906ea processors that don't have Intel Wireless
on-board can use the package from the buster-security, instead.
