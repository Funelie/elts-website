---
title: "ELA-469-1 openexr security update"
package: openexr
version: 1.6.1-8+deb8u3
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-08-04T21:41:20+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3605
  - CVE-2021-20300
  - CVE-2021-20303

---

Several vulnerabilities were discovered in OpenEXR, a library and
tools for the OpenEXR high dynamic-range (HDR) image format. An
attacker could cause a denial of service (DoS) through application
crash, and possibly execute code.
