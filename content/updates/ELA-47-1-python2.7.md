---
title: "ELA-47-1 python2.7 security update"
package: python2.7
version: 2.7.3-6+deb7u5
distribution: "Debian 7 Wheezy"
description: "fixes for command injection, REDOS vulnerabilities and uninitialized Expat's hash"
date: 2018-09-30T11:26:46+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-1000802
  - CVE-2018-1060
  - CVE-2018-1061
  - CVE-2018-14647

---

CVE-2018-1000802
     fix command injection in shutil module

CVE-2018-1060 and CVE-2018-1061
     fix REDOS vulnerabilities in poplib and difflib modules

CVE-2018-14647
     fix uninitialized Expat's hash
