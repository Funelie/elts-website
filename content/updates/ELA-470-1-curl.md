---
title: "ELA-470-1 curl security update"
package: curl
version: 7.38.0-4+deb8u21
distribution: "Debian 8 jessie"
description: "Information disclosure in connection to telnet servers"
date: 2021-08-14T01:09:12+03:00
draft: false
type: updates
cvelist:
  - CVE-2021-22898

---

Information disclosure in connection to telnet servers was fixed in curl,
a client-side URL transfer library.
