---
title: "ELA-471-1 ceph security update"
package: ceph
version: 0.80.7-2+deb8u5
distribution: "Debian 8 jessie"
description: "HTTP Header injection"
date: 2021-08-09T18:44:10+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-10753
  - CVE-2021-3524

---

A flaw was found in the Red Hat Ceph Storage RadosGW (Ceph Object Gateway).
The vulnerability is related to the injection of HTTP headers via a CORS
ExposeHeader tag. The newline character in the ExposeHeader tag in the CORS
configuration file generates a header injection in the response when the CORS
request is made.

