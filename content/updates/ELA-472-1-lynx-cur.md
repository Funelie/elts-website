---
title: "ELA-472-1 lynx-cur security update"
package: lynx-cur
version: 2.8.9dev1-2+deb8u2
distribution: "Debian 8 Jessie"
description: "authentication leak"
date: 2021-08-09T16:47:19+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-38165
---

It was discovered that there was a remote authentication credential leak in the
`lynx` text-based web browser.

The package now correctly handles authentication subcomponents in URIs (eg.
`https://user:pass@example.com`) to avoid remote attackers discovering
cleartext credentials in SSL connection data.
