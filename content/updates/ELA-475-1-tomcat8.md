---
title: "ELA-475-1 tomcat8 security update"
package: tomcat8
version: 8.0.14-1+deb8u22
distribution: "Debian 8 jessie"
description: "authentication bybass"
date: 2021-08-11T15:58:12+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-30640
  - CVE-2021-33037

---

Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.

CVE-2021-30640

    A vulnerability in the JNDI Realm of Apache Tomcat allows an attacker to
    authenticate using variations of a valid user name and/or to bypass some of
    the protection provided by the LockOut Realm.

CVE-2021-33037

    Apache Tomcat did not correctly parse the HTTP transfer-encoding request
    header in some circumstances leading to the possibility to request
    smuggling when used with a reverse proxy. Specifically: - Tomcat
    incorrectly ignored the transfer encoding header if the client declared it
    would only accept an HTTP/1.0 response; - Tomcat honoured the identify
    encoding; and - Tomcat did not ensure that, if present, the chunked
    encoding was the final encoding.
