---
title: "ELA-476-1 openjdk-8 security update"
package: openjdk-8
version: 8u302-b08-1~deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-08-09T15:32:10-04:00
draft: false
type: updates
cvelist:
  - CVE-2021-2341
  - CVE-2021-2369
  - CVE-2021-2388

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in bypass of sandbox restrictions, incorrect validation of
signed Jars or information disclosure.

Thanks to Thorsten Glaser and ⮡ tarent for contributing the updated
packages to address these vulnerabilities.

