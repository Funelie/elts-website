---
title: "ELA-477-1 libspf2 security update"
package: libspf2
version: 1.2.10-5+deb8u1
distribution: "Debian 8 jessie"
description: "stack buffer overflow"
date: 2021-08-11T17:30:31+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-20314

---

Philipp Jeitner and Haya Shulman discovered a stack-based buffer
overflow in libspf2, a library for validating mail senders with SPF,
which could result in denial of service, or potentially execution of
arbitrary code when processing a specially crafted SPF record.

