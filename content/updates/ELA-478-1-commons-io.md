---
title: "ELA-478-1 commons-io security update"
package: commons-io
version: 2.4-2+deb8u1
distribution: "Debian 8 jessie"
description: "path traversal"
date: 2021-08-13T00:21:58+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-29425

---

Lukas Euler discovered a path traversal vulnerability in commons-io, a Java
library for common useful IO related classes. When invoking the method
FileNameUtils.normalize with an improper input string, like "//../foo", or
"\\..\foo", the result would be the same value, thus possibly providing access
to files in the parent directory, but not further above (thus "limited" path
traversal), if the calling code would use the result to construct a path value.

