---
title: "ELA-479-1 exiv2 security update"
package: exiv2
version: 0.24-4.1+deb8u6
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-08-30T08:39:49+05:30
draft: false
type: updates
cvelist:
  - CVE-2019-20421
  - CVE-2021-3482
  - CVE-2021-29457
  - CVE-2021-29473
  - CVE-2021-31291
  - CVE-2021-31292

---

Several vulnerabilities have been discovered in Exiv2, a C++ library
and a command line utility to manage image metadata which could result
in denial of service or the execution of arbitrary code if a malformed
file is parsed.
