---
title: "ELA-480-1 squashfs-tools security update"
package: squashfs-tools
version: 1:4.2+20130409-2+deb8u1
distribution: "Debian 8 jessie"
description: "unvalidated filepaths"
date: 2021-08-31T15:57:53+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-40153

---

An issue has been found in squashfs-tools, a tool to create and append to squashfs filesystems.
As unsquashfs did not validate all filepaths, it would allow writing outside of the original destination.
