---
title: "ELA-481-1 qemu security update"
package: qemu
version: 1:2.1+dfsg-12+deb8u21
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-08-31T23:48:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3527
  - CVE-2021-3592
  - CVE-2021-3594
  - CVE-2021-3682
  - CVE-2021-3713

---

Several security vulnerabilities have been found in Qemu, a fast processor
emulator.

CVE-2021-3713

    An out-of-bounds write flaw was found in the UAS (USB Attached SCSI) device
    emulation of QEMU. The device uses the guest supplied stream number
    unchecked, which can lead to out-of-bounds access to the UASDevice->data3
    and UASDevice->status3 fields. A malicious guest user could use this flaw
    to crash QEMU or potentially achieve code execution with the privileges of
    the QEMU process on the host.

CVE-2021-3682

    A flaw was found in the USB redirector device emulation of QEMU. It occurs
    when dropping packets during a bulk transfer from a SPICE client due to the
    packet queue being full. A malicious SPICE client could use this flaw to
    make QEMU call free() with faked heap chunk metadata, resulting in a crash
    of QEMU or potential code execution with the privileges of the QEMU process
    on the host.

CVE-2021-3527

    A flaw was found in the USB redirector device (usb-redir) of QEMU. Small
    USB packets are combined into a single, large transfer request, to reduce
    the overhead and improve performance. The combined size of the bulk
    transfer is used to dynamically allocate a variable length array (VLA) on
    the stack without proper validation. Since the total size is not bounded, a
    malicious guest could use this flaw to influence the array length and cause
    the QEMU process to perform an excessive allocation on the stack, resulting
    in a denial of service.

CVE-2021-3594

    An invalid pointer initialization issue was found in the SLiRP networking
    implementation of QEMU. The flaw exists in the udp_input() function and
    could occur while processing a udp packet that is smaller than the size of
    the 'udphdr' structure. This issue may lead to out-of-bounds read access or
    indirect host memory disclosure to the guest. The highest threat from this
    vulnerability is to data confidentiality.

CVE-2021-3592

    An invalid pointer initialization issue was found in the SLiRP networking
    implementation of QEMU. The flaw exists in the bootp_input() function and
    could occur while processing a udp packet that is smaller than the size of
    the 'bootp_t' structure. A malicious guest could use this flaw to leak 10
    bytes of uninitialized heap memory from the host. The highest threat from
    this vulnerability is to data confidentiality.
