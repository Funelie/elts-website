---
title: "ELA-481-2 qemu regression update"
package: qemu
version: 1:2.1+dfsg-12+deb8u22
distribution: "Debian 8 jessie"
description: "regression update"
date: 2021-09-12T01:19:19+02:00
draft: false
type: updates
cvelist:

---

It was found that the patch for CVE-2021-3592 introduced a regression which
prevented ssh connections to the host system. Since there is no imminent
solution for the problem, the patch for CVE-2021-3592 has been reverted.
Updated qemu packages are now available to correct this issue.

