---
title: "ELA-484-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u15
distribution: "Debian 8 jessie"
description: "alternate chains verification"
date: 2021-09-18T22:19:24+02:00
draft: false
type: updates
cvelist:

---

OpenSSL, a cryptography library for secure communication, fails to
validate alternate trust chains in some conditions.  In particular
this breaks connecting to servers that use Let's Encrypt certificates,
starting 2021-10-01.
