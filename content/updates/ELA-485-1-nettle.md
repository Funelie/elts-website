---
title: "ELA-485-1 nettle security update"
package: nettle
version: 2.7.1-5+deb8u3
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-09-19T00:53:39+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3580
  - CVE-2021-20305

---

Multiple vulnerabilities were discovered in nettle, a low level cryptographic
library, which could result in denial of service (remote crash in RSA
decryption via specially crafted ciphertext, crash on ECDSA signature
verification) or incorrect verification of ECDSA signatures.
