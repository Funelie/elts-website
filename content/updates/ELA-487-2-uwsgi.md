---
title: "ELA-487-2 uwsgi regression update"
package: uwsgi
version: 2.0.7-1+deb8u5
distribution: "Debian 8 jessie"
description: "regression update"
date: 2021-10-20T19:09:38+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-36160

---

A regression was introduced in ELA-487-1, where the uwsgi proxy module
for Apache2 (mod_proxy_uwsgi) interprets incorrect Apache
configurations in a less forgiving way, causing existing setups to
fail after upgrade.
