---
title: "ELA-488-1 libxstream-java security update"
package: libxstream-java
version: 1.4.11.1-1+deb8u4
distribution: "Debian 8 jessie"
description: "remote code execution"
date: 2021-09-30T14:24:11+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-39139
  - CVE-2021-39140
  - CVE-2021-39141
  - CVE-2021-39144
  - CVE-2021-39145
  - CVE-2021-39146
  - CVE-2021-39147
  - CVE-2021-39148
  - CVE-2021-39149
  - CVE-2021-39150
  - CVE-2021-39151
  - CVE-2021-39152
  - CVE-2021-39153
  - CVE-2021-39154

---

Multiple security vulnerabilities have been discovered in XStream, a Java
library to serialize objects to XML and back again.

These vulnerabilities may allow a remote attacker to load and execute arbitrary
code from a remote host only by manipulating the processed input stream.

XStream itself sets up a whitelist by default now, i.e. it blocks all classes
except those types it has explicit converters for. It used to have a blacklist
by default, i.e. it tried to block all currently known critical classes of the
Java runtime. Main reason for the blacklist were compatibility, it allowed to
use newer versions of XStream as drop-in replacement. However, this approach
has failed. A growing list of security reports has proven, that a blacklist is
inherently unsafe, apart from the fact that types of 3rd libraries were not
even considered. A blacklist scenario should be avoided in general, because it
provides a false sense of security.

See also https://x-stream.github.io/security.html#framework
