---
title: "ELA-489-1 weechat security update"
package: weechat
version: 1.0.1-1+deb8u4
distribution: "Debian 8 jessie"
description: "denial of service in the Relay plugin"
date: 2021-09-30T23:52:24+03:00
draft: false
type: updates
cvelist:
  - CVE-2021-40516

---

A crafted WebSocket frame could result in a crash in the Relay plugin
of the chat client WeeChat.
