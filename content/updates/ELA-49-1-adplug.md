---
title: "ELA-49-1 adplug security update"
package: adplug
version: 2.2.1+dfsg3-0.1+deb7u1
distribution: "Debian 7 Wheezy"
description: "double-free vulnerability"
date: 2018-10-07T18:41:16+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-17825
---

It was discovered that there was a potential denial of service (DoS)
attack due to double-free vulnerability in the "adplug" sound library.
