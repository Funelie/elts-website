---
title: "ELA-490-1 nghttp2 security update"
package: nghttp2
version: 0.6.4-2+deb8u1
distribution: "Debian 8 jessie"
description: "denial-of-service"
date: 2021-10-01T20:43:55+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-11080

---

An overly large HTTP/2 SETTINGS frame payload causes denial of service. The
proof of concept attack involves a malicious client constructing a SETTINGS
frame with a length of 14,400 bytes (2400 individual settings entries) over and
over again. The attack causes the CPU to spike at 100%.
