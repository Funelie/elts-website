---
title: "ELA-494-1 curl security update"
package: curl
version: 7.38.0-4+deb8u22
distribution: "Debian 8 jessie"
description: "TLS issues"
date: 2021-10-09T11:07:16+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-22946
  - CVE-2021-22947

---

Two issues have been found in curl, a command line tool and an easy-to-use client-side library for transferring data with URL syntax.

CVE-2021-22946
     Crafted answers from a server might force clients to not use TLS on
     connections though TLS was required and expected.

CVE-2021-22947
     When using STARTTLS to initiate a TLS connection, the server might
     send multiple answers before the TLS upgrade and such the client
     would handle them as being trusted. This could be used by a
     MITM-attacker to inject fake response data.
