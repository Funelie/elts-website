---
title: "ELA-496-1 icu security update"
package: icu
version: 52.1-8+deb8u9
distribution: "Debian 8 Jessie"
description: "Use-after-free vulnerability"
date: 2021-10-12T11:54:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-21913
---

It was discovered that there was a potential use-after-free vulnerability in
icu, a library which provides Unicode and locale functionality.
