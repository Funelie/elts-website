---
title: "ELA-499-1 hiredis security update"
package: hiredis
version: 0.11.0-4+deb8u2
distribution: "Debian 8 Jessie"
description: "integer-overflow vulnerability"
date: 2021-10-23T17:18:24+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-32765
---

It was discovered that there was an integer-overflow vulnerability in hiredis,
a C client library for communicating with Redis databases. This occurred
within the handling and parsing of 'multi-bulk' replies.
