---
title: "ELA-5-1 gnupg security update"
package: gnupg
version: 1.4.12-7+deb7u10
distribution: "Debian 7 Wheezy"
description: "insufficient input sanitisation"
date: 2018-06-23T15:22:38Z
draft: false
type: updates
cvelist:
  - CVE-2018-12020
---

Marcus Brinkmann discovered that GnuGPG performed insufficient
sanitisation of file names displayed in status messages, which could be
abused to fake the verification status of a signed email.

Details can be found in the upstream advisory at
https://lists.gnupg.org/pipermail/gnupg-announce/2018q2/000425.html
