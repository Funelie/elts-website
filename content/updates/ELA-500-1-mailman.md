---
title: "ELA-500-1 mailman security update"
package: mailman
version: 1:2.1.18-2+deb8u8
distribution: "Debian 8 Jessie"
description: "privilege escalation vulnerability"
date: 2021-10-23T17:55:57+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-42096
  - CVE-2021-42097
---

It was discovered that there was a potential remote privilege
escalation vulnerability in the Mailman mailing list manager.

Some CSRF token values were derived from the admin password, and that
could have been used to conductg a brute-force attack against that
password.
