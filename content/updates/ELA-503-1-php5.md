---
title: "ELA-503-1 php5 security update"
package: php5
version: 5.6.40+dfsg-0+deb8u15
distribution: "Debian 8 jessie"
description: "privilege escalation"
date: 2021-10-27T12:48:48+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-21703

---

An out-of-bounds read and write flaw was discovered in the PHP-FPM
code, which could result in escalation of privileges from local
unprivileged user to the root user.
