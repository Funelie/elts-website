---
title: "ELA-504-1 jbig2dec security update"
package: jbig2dec
version: 0.13-4~deb8u3
distribution: "Debian 8 jessie"
description: "overflow and null pointer dereference"
date: 2021-10-29T01:04:04+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-9216
  - CVE-2020-12268

---

Two issues have been found in jbig2dec, a JBIG2 decoder library.
One is related to an overflow with a crafted image file. The other is related to a NULL pointer dereference.
