---
title: "ELA-507-1 opencv security update"
package: opencv
version: 2.4.9.1+dfsg-1+deb8u3
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-10-30T16:43:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-14493
  - CVE-2019-15939

---

Two security vulnerabilities have been addressed in OpenCV, the Open Computer
Vision Library. A NULL pointer dereference and a divide-by-zero error may lead
to a denial-of-service if malformed input is processed.
