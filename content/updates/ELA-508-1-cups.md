---
title: "ELA-508-1 cups security update"
package: cups
version: 1.7.5-11+deb8u9
distribution: "Debian 8 jessie"
description: "missing input validation"
date: 2021-10-31T00:33:58+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-10001

---

An issue has been found in cups, the Common UNIX Printing System.
Due to an input validation issue a malicious application might be allowed
to read restricted memory.
