---
title: "ELA-51-1 tomcat7 security update"
package: tomcat7
version: 7.0.28-4+deb7u20
distribution: "Debian 7 Wheezy"
description: "Open Redirect"
date: 2018-10-14T19:40:06+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-11784
---

Sergey Bobrov discovered that when the default servlet returned a redirect to a
directory (e.g. redirecting to /foo/ when the user requested /foo) a specially
crafted URL could be used to cause the redirect to be generated to any URI of
the attackers choice.
