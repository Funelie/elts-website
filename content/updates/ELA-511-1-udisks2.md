---
title: "ELA-511-1 udisks2 security update"
package: udisks2
version: 2.1.3-5+deb8u1
distribution: "Debian 8 jessie"
description: "denial of service via system crash"
date: 2021-11-05T09:56:25+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3802

---

Stefan Walter found that udisks2, a service to access and manipulate
storage devices, could cause denial of service via system crash if a
corrupted or specially crafted ext2/3/4 device or image was mounted,
which could happen automatically on certain environments.
