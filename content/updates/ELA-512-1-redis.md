---
title: "ELA-512-1 redis security update"
package: redis
version: 2:2.8.17-1+deb8u9
distribution: "Debian 8 Jessie"
description: "Multiple vulnerabilities"
date: 2021-11-06T09:39:57+00:00
draft: false
type: updates
cvelist:
  - CVE-2021-32672
  - CVE-2021-32687
  - CVE-2021-32675
  - CVE-2021-32626
---

A number of vulnerabilities were discovered in Redis, a popular key/value database:

 * CVE-2021-32672: Random heap reading issue with Lua Debugger.

 * CVE-2021-32687: Integer to heap buffer overflow with intsets, when
   set-max-intset-entries is manually configured to a non-default, very large
   value.

 * CVE-2021-32675: Denial Of Service when processing RESP request payloads
   with a large number of elements on many connections.

 * CVE-2021-32626: Specially crafted Lua scripts may result with Heap
   buffer overflow.
