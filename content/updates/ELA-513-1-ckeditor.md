---
title: "ELA-513-1 ckeditor security update"
package: ckeditor
version: 4.4.4+dfsg1-3+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-11-09T14:33:58+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-33829
  - CVE-2021-37695

---

CKEditor, an open source WYSIWYG HTML editor with rich content
support, which can be embedded into web pages, had two
vulnerabilites as follows:

CVE-2021-33829

    A cross-site scripting (XSS) vulnerability in the HTML Data
    Processor in CKEditor 4 allows remote attackers to inject
    executable JavaScript code through a crafted comment because
    --!> is mishandled.

CVE-2021-37695

    A potential vulnerability has been discovered in CKEditor 4
    Fake Objects package. The vulnerability allowed to inject
    malformed Fake Objects HTML, which could result in executing
    JavaScript code.
