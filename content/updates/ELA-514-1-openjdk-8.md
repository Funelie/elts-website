---
title: "ELA-514-1 openjdk-8 security update"
package: openjdk-8
version: 8u312-b07-1~deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-11-09T14:02:27-05:00
draft: false
type: updates
cvelist:
  - CVE-2021-35550
  - CVE-2021-35556
  - CVE-2021-35559
  - CVE-2021-35561
  - CVE-2021-35564
  - CVE-2021-35565
  - CVE-2021-35567
  - CVE-2021-35578
  - CVE-2021-35586
  - CVE-2021-35588
  - CVE-2021-35603

---

Several vulnerabilities have been discovered in the OpenJDK Java runtime,
including issues with cyprographic hashing, TLS client handshaking, and
various other issues.

Thanks to Thorsten Glaser and ⮡ tarent for contributing the updated
packages to address these vulnerabilities.
