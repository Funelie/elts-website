---
title: "ELA-515-1 jqueryui security update"
package: jqueryui
version: 1.10.1+dfsg-1+deb8u1
distribution: "Debian 8 jessie"
description: "cross-site scripting"
date: 2021-11-11T10:07:50+01:00
draft: false
type: updates
cvelist:
  - CVE-2016-7103
  - CVE-2021-41182
  - CVE-2021-41183
  - CVE-2021-41184

---

Several cross-site scripting (XSS) vulnerabilities have been found in
jqueryui, a JavaScript UI library for dynamic web applications, which
could allow attackers with sufficient access to inject arbitrary
code.
