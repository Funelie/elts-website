---
title: "ELA-516-1 openjdk-7 security update"
package: openjdk-7
version: 7u321-2.6.28-0+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-11-11T10:18:09+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-35550
  - CVE-2021-35556
  - CVE-2021-35559
  - CVE-2021-35561
  - CVE-2021-35564
  - CVE-2021-35565
  - CVE-2021-35586
  - CVE-2021-35588
  - CVE-2021-35603

---

Several vulnerabilities have been discovered in the OpenJDK Java
runtime, which may result in denial of service, selection of weak ciphers,
sandbox bypass or information disclosure.
