---
title: "ELA-518-1 postgresql-9.4 security update"
package: postgresql-9.4
version: 9.4.26-0+deb8u5
distribution: "Debian 8 jessie"
description: "query injection"
date: 2021-11-18T16:34:23+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-23214
  - CVE-2021-23222

---

Jacob Champion discovered that PostgreSQL, an object-relational SQL
database, may process unencrypted bytes from a database connection
even if it is encrypted. A man-in-the-middle attacker can inject
arbitrary SQL queries when a connection is first established.

* CVE-2021-23214

    Server processes unencrypted bytes from man-in-the-middle - when
    the server is configured to use trust authentication with a
    clientcert requirement or to use cert authentication, a
    man-in-the-middle attacker can inject arbitrary SQL queries when a
    connection is first established, despite the use of SSL
    certificate verification and encryption.

* CVE-2021-23222

    libpq processes unencrypted bytes from man-in-the-middle - a
    man-in-the-middle attacker can inject false responses to the
    client's first few queries, despite the use of SSL certificate
    verification and encryption. If more preconditions hold, the
    attacker can exfiltrate the client's password or other
    confidential data that might be transmitted early in a
    session. The attacker must have a way to trick the client's
    intended server into making the confidential data accessible to
    the attacker. A known implementation having that property is a
    PostgreSQL configuration vulnerable to CVE-2021-23214.
