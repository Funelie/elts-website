---
title: "ELA-521-1 libmodbus security update"
package: libmodbus
version: 3.0.6-1+deb8u1
distribution: "Debian 8 jessie"
description: "out of bound reads"
date: 2021-11-22T16:52:03+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-14462
  - CVE-2019-14463

---

Two issues have been found in libmodbus, a library for the Modbus protocol.
Both issues are related to out of bound reads, which could result in a denial of service or other unspecified impact.
