---
title: "ELA-523-1 ntfs-3g security update"
package: ntfs-3g
version: 1:2014.2.15AR.2-1+deb8u5
distribution: "Debian 8 jessie"
description: "multiple buffer overflow"
date: 2021-11-30T01:46:01+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-33285
  - CVE-2021-33286
  - CVE-2021-33287
  - CVE-2021-33289
  - CVE-2021-35266
  - CVE-2021-35267
  - CVE-2021-35268
  - CVE-2021-35269
  - CVE-2021-39251
  - CVE-2021-39252
  - CVE-2021-39253
  - CVE-2021-39254
  - CVE-2021-39255
  - CVE-2021-39256
  - CVE-2021-39257
  - CVE-2021-39258
  - CVE-2021-39259
  - CVE-2021-39260
  - CVE-2021-39261
  - CVE-2021-39262
  - CVE-2021-39263

---

Several vulnerabilities were discovered in NTFS-3G, a read-write NTFS
driver for FUSE. A local user can take advantage of these flaws for
local root privilege escalation.
