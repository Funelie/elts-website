---
title: "ELA-524-1 gmp security update"
package: gmp
version: 2:6.0.0+dfsg-6+deb8u1
distribution: "Debian 8 jessie"
description: "segmentation fault"
date: 2021-11-30T02:57:05+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-43618

---

GNU Multiple Precision Arithmetic Library (GMP) through
6.2.1 has an mpz/inp_raw.c integer overflow and resultant
buffer overflow via crafted input, leading to a
segmentation fault on 32-bit platforms.
