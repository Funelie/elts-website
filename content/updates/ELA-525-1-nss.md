---
title: "ELA-525-1 nss security update"
package: nss
version: 2:3.26-1+debu8u14
distribution: "Debian 8 jessie"
description: "heap overflow"
date: 2021-12-02T18:15:10+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-43527

---

Tavis Ormandy discovered that nss, the Mozilla Network Security Service
library, is prone to a heap overflow flaw when verifying DSA or RSA-PPS
signatures, which could result in denial of service or potentially the
execution of arbitrary code.
