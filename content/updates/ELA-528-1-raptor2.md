---
title: "ELA-528-1 raptor2 security update"
package: raptor2
version: 2.0.14-1+deb8u2
distribution: "Debian 8 jessie"
description: "malformed data result in segfault"
date: 2021-12-14T00:29:39+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-25713

---

An issue has been found in raptor2, a Raptor RDF parser and serializer
library. Malformed input file can lead to a segfault.

