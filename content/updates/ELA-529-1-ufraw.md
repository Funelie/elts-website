---
title: "ELA-529-1 ufraw security update"
package: ufraw
version: 0.20-2+deb8u2
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2021-12-24T02:10:08+01:00
draft: false
type: updates
cvelist:
  - CVE-2015-8366

---

An issue has been found in ufraw, a standalone importer for raw camera images.
Due to an array index error in smal_decode_segment() an attacker might be able to cause memory errors and possibly execute arbitrary code.
