---
title: "ELA-531-1 ruby2.1 security update"
package: ruby2.1
version: 2.1.5-2+deb8u13
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2021-12-27T06:13:40+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-41817
  - CVE-2021-41819

---

A cookie prefix spoofing vulnerability in CGI::Cookie.parse and a
regular expression denial of service vulnerability (ReDoS) on date
parsing methods was discovered in src:ruby2.1, the Ruby interpreter.
