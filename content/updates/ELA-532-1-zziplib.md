---
title: "ELA-532-1 zziplib security update"
package: zziplib
version: 0.13.62-3+deb8u3
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2021-12-28T00:52:41+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-18442

---

An issue has been found in zziplib, a library providing read access on
ZIP-archive.
Because of mishandling a return value, an attacker might cause a denial of
service due to an infinite loop.

