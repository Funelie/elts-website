---
title: "ELA-533-1 python-gnupg security update"
package: python-gnupg
version: 0.3.6-1+deb8u2
distribution: "Debian 8 jessie"
description: "spoofing"
date: 2021-12-29T02:48:06+05:30
draft: false
type: updates
cvelist:
  - CVE-2018-12020

---

Marcus Brinkmann discovered that GnuPG before 2.2.8 improperly handled certain
command line parameters. A remote attacker could use this to spoof the output of
GnuPG and cause unsigned e-mail to appear signed.
