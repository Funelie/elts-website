---
title: "ELA-534-1 xorg-server security update"
package: xorg-server
version: 2:1.16.4-1+deb8u6
distribution: "Debian 8 jessie"
description: "privilige escalation"
date: 2021-12-29T22:58:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-4008
  - CVE-2021-4009
  - CVE-2021-4011

---

Jan-Niklas Sohn discovered that multiple input validation failures in X server extensions
of the X.org X server may result in privilege escalation if the X server is running privileged.


