---
title: "ELA-535-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.290-1~deb8u1
distribution: "Debian 8 jessie"
description: "linux kernel update"
date: 2021-12-30T11:55:44+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-3702
  - CVE-2020-16119
  - CVE-2021-0920
  - CVE-2021-3612
  - CVE-2021-3653
  - CVE-2021-3655
  - CVE-2021-3679
  - CVE-2021-3732
  - CVE-2021-3753
  - CVE-2021-3760
  - CVE-2021-20317
  - CVE-2021-20321
  - CVE-2021-20322
  - CVE-2021-22543
  - CVE-2021-37159
  - CVE-2021-38160
  - CVE-2021-38198
  - CVE-2021-38199
  - CVE-2021-38204
  - CVE-2021-38205
  - CVE-2021-40490
  - CVE-2021-41864
  - CVE-2021-42008
  - CVE-2021-42739
  - CVE-2021-43389

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leaks.

CVE-2020-3702

    A flaw was found in the driver for Atheros IEEE 802.11n family of
    chipsets (ath9k) allowing information disclosure.

CVE-2020-16119

    Hadar Manor reported a use-after-free in the DCCP protocol
    implementation in the Linux kernel. A local attacker can take
    advantage of this flaw to cause a denial of service or potentially
    to execute arbitrary code.

CVE-2021-0920

    A race condition was discovered in the local sockets (AF_UNIX)
    subsystem, which could lead to a use-after-free.  A local user
    could exploit this for denial of service (memory corruption or
    crash), or possibly for privilege escalation.

CVE-2021-3612

    Murray McAllister reported a flaw in the joystick input subsystem.
    A local user permitted to access a joystick device could exploit
    this to read and write out-of-bounds in the kernel, which could
    be used for privilege escalation.

CVE-2021-3653

    Maxim Levitsky discovered a vulnerability in the KVM hypervisor
    implementation for AMD processors in the Linux kernel: Missing
    validation of the `int_ctl` VMCB field could allow a malicious L1
    guest to enable AVIC support (Advanced Virtual Interrupt
    Controller) for the L2 guest. The L2 guest can take advantage of
    this flaw to write to a limited but still relatively large subset
    of the host physical memory.

CVE-2021-3655

    Ilja Van Sprundel and Marcelo Ricardo Leitner found multiple flaws
    in the SCTP implementation, where missing validation could lead to
    an out-of-bounds read.  On a system using SCTP, a networked
    attacker could exploit these to cause a denial of service (crash).

CVE-2021-3679

    A flaw in the Linux kernel tracing module functionality could
    allow a privileged local user (with CAP_SYS_ADMIN capability) to
    cause a denial of service (resource starvation).

CVE-2021-3732

    Alois Wohlschlager reported a flaw in the implementation of the
    overlayfs subsystem, allowing a local attacker with privileges to
    mount a filesystem to reveal files hidden in the original mount.

CVE-2021-3753

    Minh Yuan reported a race condition in the vt_k_ioctl in
    drivers/tty/vt/vt_ioctl.c, which may cause an out of bounds read
    in vt.

CVE-2021-3760

    Lin Horse reported a flaw in the NCI (NFC Controller Interface)
    driver, which could lead to a use-after-free.

    However, this driver is not included in the binary packages
    provided by Debian.

CVE-2021-20317

    It was discovered that the timer queue structure could become
    corrupt, leading to waiting tasks never being woken up.  A local
    user with certain privileges could exploit this to cause a denial
    of service (system hang).

CVE-2021-20321

    A race condition was discovered in the overlayfs filesystem
    driver.  A local user with access to an overlayfs mount and to its
    underlying upper directory could exploit this for privilege
    escalation.

CVE-2021-20322

    An information leak was discovered in the IPv4 implementation.  A
    remote attacker could exploit this to quickly discover which UDP
    ports a system is using, making it easier for them to carry out a
    DNS poisoning attack against that system.

CVE-2021-22543

    David Stevens discovered a flaw in how the KVM hypervisor maps
    host memory into a guest.  A local user permitted to access
    /dev/kvm could use this to cause certain pages to be freed when
    they should not, leading to a use-after-free.  This could be used
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2021-37159

    A flaw was discovered in the hso driver for Option mobile
    broadband modems.  An error during initialisation could lead to a
    double-free or use-after-free.  An attacker able to plug in USB
    devices could use this to cause a denial of service (crash or
    memory corruption) or possibly to run arbitrary code.

CVE-2021-38160

    A flaw in the virtio_console was discovered allowing data
    corruption or data loss by an untrusted device.

CVE-2021-38198

    A flaw was discovered in the KVM implementation for x86
    processors, that could result in virtual memory protection within
    a guest not being applied correctly.  When shadow page tables are
    used - i.e. for nested virtualisation, or on CPUs lacking the EPT
    or NPT feature - a user of the guest OS might be able to exploit
    this for denial of service or privilege escalation within the
    guest.

CVE-2021-38199

    Michael Wakabayashi reported a flaw in the NFSv4 client
    implementation, where incorrect connection setup ordering allows
    operations of a remote NFSv4 server to cause a denial of service.

CVE-2021-38204

    A flaw was discovered in the max4321-hcd USB host controller
    driver, which could lead to a use-after-free.

    However, this driver is not included in the binary packages
    provided by Debian.

CVE-2021-38205

    An information leak was discovered in the xilinx_emaclite network
    driver.  On a custom kernel where this driver is enabled and used,
    this might make it easier to exploit other kernel bugs.

CVE-2021-40490

    A race condition was discovered in the ext4 subsystem when writing
    to an inline_data file while its xattrs are changing. This could
    result in denial of service.

CVE-2021-41864

    An integer overflow was discovered in the Extended BPF (eBPF)
    subsystem.  A local user could exploit this for denial of service
    (memory corruption or crash), or possibly for privilege
    escalation.

    This can be mitigated by setting sysctl
    kernel.unprivileged_bpf_disabled=1, which disables eBPF use by
    unprivileged users.

CVE-2021-42008

    A heap buffer overflow was discovered in the 6pack serial port
    network driver.  A local user with CAP_NET_ADMIN capability could
    exploit this for denial of service (memory corruption or crash), or
    possibly for privilege escalation.

CVE-2021-42739

    A heap buffer overflow was discovered in the firedtv driver for
    FireWire-connected DVB receivers.  A local user with access to a
    firedtv device could exploit this for denial of service (memory
    corruption or crash), or possibly for privilege escalation.

CVE-2021-43389

    The Active Defense Lab of Venustech discovered a flaw in the CMTP
    subsystem as used by Bluetooth, which could lead to an
    out-of-bounds read and object type confusion.  A local user with
    CAP_NET_ADMIN capability in the initial user namespace could
    exploit this for denial of service (memory corruption or crash),
    or possibly for privilege escalation.
