---
title: "ELA-536-1 lxml security update"
package: lxml
version: 3.4.0-1+deb8u5
distribution: "Debian 8 jessie"
description: "malicious content pass through"
date: 2022-01-01T12:02:26+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-43818

---

lxml is a library for processing XML and HTML in the Python language.
Prior to version 4.6.5, the HTML Cleaner in lxml.html lets certain
crafted script content pass through, as well as script content in
SVG files embedded using data URIs.
