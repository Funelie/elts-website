---
title: "ELA-537-1 salt security update"
package: salt
version: 2014.1.13+ds-3+deb8u2
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-01-03T19:21:00+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-16846
  - CVE-2020-17490
  - CVE-2020-35662
  - CVE-2021-3197
  - CVE-2021-21996
  - CVE-2021-25282
  - CVE-2021-25283
  - CVE-2021-25284

---

Multiple security vulnerabilities have been discovered in Salt, a
powerful remote execution manager, that allow for local privilege
escalation on a minion, server side template injection attacks, shell
and command injections or incorrect validation of SSL certificates.

* CVE-2020-16846

    Sending crafted web requests to the Salt API, with the SSH client
    enabled, can result in shell injection.

* CVE-2020-17490

    The TLS module creates certificates with weak file permissions.

* CVE-2020-35662

    When authenticating to services using certain modules, the SSL
    certificate is not always validated.

* CVE-2021-3197

    The salt-api's ssh client is vulnerable to a shell injection by
    including ProxyCommand in an argument, or via ssh_options provided
    in an API request.

* CVE-2021-21996

    A user who has control of the source, and source_hash URLs can
    gain full file system access as root on a salt minion.

* CVE-2021-25282

    The salt.wheel.pillar_roots.write method is vulnerable to
    directory traversal.

* CVE-2021-25283

    The jinja renderer does not protect against server side template
    injection attacks.

* CVE-2021-25284

    salt.modules.cmdmod can log credentials to the info or error log
    level.
