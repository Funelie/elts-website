---
title: "ELA-538-1 clamav security update"
package: clamav
version: 0.103.4+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "new version update"
date: 2022-01-10T17:59:09+01:00
draft: false
type: updates
cvelist:

---

Version 0.102 of ClamAV, an anti-virus toolkit, is end-of-life. ClamAV
has been updated to version 0.103 to be able to receive virus signature
updates.
