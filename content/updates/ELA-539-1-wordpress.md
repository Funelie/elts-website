---
title: "ELA-539-1 wordpress security update"
package: wordpress
version: 4.1.34+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-01-13T02:59:05+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-21661
  - CVE-2022-21662
  - CVE-2022-21663
  - CVE-2022-21664

---

Several vulnerabilities were discovered in Wordpress, a web blogging
tool. They allowed remote attackers to perform SQL injection, run
unchecked SQL queries, bypass hardening, or perform Cross-Site
Scripting (XSS) attacks.
