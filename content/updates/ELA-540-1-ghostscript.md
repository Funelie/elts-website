---
title: "ELA-540-1 ghostscript security update"
package: ghostscript
version: 9.26a~dfsg-0+deb8u8
distribution: "Debian 8 jessie"
description: "heap-based buffer overflow"
date: 2022-01-16T23:25:57+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-45944
  - CVE-2021-45949

---

Multiple security issues were discovered in Ghostscript, the GPL PostScript/PDF
interpreter, which could result in denial of service and potentially the
execution of arbitrary code if malformed document files are processed.
