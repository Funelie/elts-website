---
title: "ELA-541-1 uriparser security update"
package: uriparser
version: 0.8.0.1-2+deb8u3
distribution: "Debian 8 Jessie"
description: "invalid free vulnerabilities"
date: 2022-01-17T10:37:17+00:00
draft: false
type: updates
cvelist:
  - CVE-2021-46141
  - CVE-2021-46142
---

It was discovered that there were two "invalid free" issues in `uriparser`, a C
library for parsing URLs according to RFC 3986.
