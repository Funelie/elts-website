---
title: "ELA-541-2 uriparser security update"
package: uriparser
version: 0.8.0.1-2+deb8u4
distribution: "Debian 8 Jessie"
description: "address incomplete fix"
date: 2022-01-26T09:38:43-08:00
draft: false
type: updates
cvelist:
  - CVE-2021-46141
---

It was discovered that the fix for CVE-2021-46141 released in uriparser version
0.8.0.1-2+deb8u3 was incomplete.
