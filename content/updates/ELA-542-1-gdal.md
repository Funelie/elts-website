---
title: "ELA-542-1 gdal security update"
package: gdal
version: 1.10.1+dfsg-8+deb8u3
distribution: "Debian 8 jessie"
description: "heap buffer overflow"
date: 2022-01-17T11:58:09+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-45943

---

An issue was found in GDAL, a geospatial library, that could lead to
denial of service via application crash or possibly the execution of
arbitrary code if maliciously crafted data was parsed.
