---
title: "ELA-546-1 pillow security update"
package: pillow
version: 2.6.1-2+deb8u7
distribution: "Debian 8 jessie"
description: "arbitrary code execution"
date: 2022-01-24T11:20:44+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-28675
  - CVE-2021-28676
  - CVE-2021-28677
  - CVE-2021-34552
  - CVE-2022-22815
  - CVE-2022-22816
  - CVE-2022-22817

---

Multiple security issues were discovered in Pillow, a Python imaging
library, which could result in denial of service and potentially
the execution of arbitrary code if malformed images are processed.
