---
title: "ELA-547-1 golang security update"
package: golang
version: 2:1.3.3-1+deb8u4
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-01-24T16:42:10+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-33196
  - CVE-2021-36221
  - CVE-2021-39293
  - CVE-2021-41771
  - CVE-2021-44717

---

Several vulnerabilities were discovered in the Go programming
language. An attacker could trigger denial-of-service (DoS) and
information leak.

* CVE-2021-33196

    In archive/zip, a crafted file count (in an archive's header) can
    cause a NewReader or OpenReader panic.

* CVE-2021-36221

    Go has a race condition that can lead to a net/http/httputil
    ReverseProxy panic upon an ErrAbortHandler abort.

* CVE-2021-39293

    Follow-up fix to CVE-2021-33196

* CVE-2021-41771

    ImportedSymbols in debug/macho (for Open or OpenFat) accesses a
    Memory Location After the End of a Buffer, aka an out-of-bounds
    slice situation.

* CVE-2021-44717

    Go on UNIX allows write operations to an unintended file or
    unintended network connection as a consequence of erroneous
    closing of file descriptor 0 after file-descriptor exhaustion.
