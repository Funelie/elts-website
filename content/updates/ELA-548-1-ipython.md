---
title: "ELA-548-1 ipython security update"
package: ipython
version: 2.3.0-2+deb8u1
distribution: "Debian 8 Jessie"
description: "arbitrary code execution vulnerability"
date: 2022-01-24T08:45:13-08:00
draft: false
type: updates
cvelist:
  - CVE-2022-21699
---

It was discovered that there was a potential arbitrary code execution
vulnerability in IPython, the interactive Python shell.

This issue stemmed from IPython executing untrusted files in the current
working directory. According to upstream:

  Almost all versions of IPython looks for configuration and profiles in
  current working directory. Since IPython was developed before pip and
  environments existed, it was used a convenient way to load code/packages in a
  project dependant way.

  In 2022, it is not necessary anymore, and can lead to confusing behavior
  where for example cloning a repository and starting IPython or loading a
  notebook from any Jupyter-Compatible interface that has ipython set as a
  kernel can lead to code execution.

To address this problem, the current working directory is no longer searched
for profiles or configuration files.
