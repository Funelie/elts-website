---
title: "ELA-549-1 apr security update"
package: apr
version: 1.5.1-3+deb8u1
distribution: "Debian 8 jessie"
description: "out of bounds memory access"
date: 2022-01-24T23:37:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-12613

---

An issue has been found in apr, the Apache Portable Runtime Library.
The issue is related to out of bounds memory access due to invalid date fields.

