---
title: "ELA-55-1 firmware-nonfree security update"
package: firmware-nonfree
version: 20161130-4~deb7u1
distribution: "Debian 7 Wheezy"
description: "wifi firmware update"
date: 2018-11-07T23:11:22+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-9417
  - CVE-2017-13081
  - CVE-2017-13080
  - CVE-2017-13079
  - CVE-2017-13078
  - CVE-2017-13077
  - CVE-2017-0561
  - CVE-2016-0801
---

Several security vulnerabilities were discovered in WiFi Firmware that could
allow remote attackers within radio range to spoof frames from access points to
clients or execute arbitrary code.
