---
title: "ELA-550-1 nss security update"
package: nss
version: 2:3.26-1+debu8u16
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-01-25T16:23:14+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-22747

---

It was found that nss, the Mozilla Network Security Service library, was
vulnerable to a NULL pointer dereference when parsing empty PKCS 7
sequences, which could result in denial of service.
