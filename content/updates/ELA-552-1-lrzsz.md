---
title: "ELA-552-1 lrzsz security update"
package: lrzsz
version: 0.12.21-7+deb8u1
distribution: "Debian 8 jessie"
description: "possible data leak"
date: 2022-01-25T23:00:21+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-10195

---

An issues has been found in lrzsz, a set of tools for zmodem/xmodem/ymodem file transfer.
Due to an incorrect length check, which might result in a size_t wrap around, an information leak to the receiving side could happen.
