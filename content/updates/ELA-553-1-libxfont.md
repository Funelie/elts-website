---
title: "ELA-553-1 libxfont security update"
package: libxfont
version: 1:1.5.1-1+deb8u2
distribution: "Debian 8 jessie"
description: "open (but not read) local files"
date: 2022-01-25T23:09:10+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-16611

---

An issue has been found in libxfont, an X11 font rasterisation library.
By creating symlinks, a local attacker can open (but not read) local files as user root. 
This might create unwanted actions with special files like /dev/watchdog.
