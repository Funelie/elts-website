---
title: "ELA-556-1 expat security update"
package: expat
version: 2.1.0-6+deb8u7
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-02-01T12:46:28+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-45960
  - CVE-2021-46143
  - CVE-2022-22822
  - CVE-2022-22823
  - CVE-2022-22824
  - CVE-2022-22825
  - CVE-2022-22826
  - CVE-2022-22827
  - CVE-2022-23852
  - CVE-2022-23990

---

Multiple security vulnerabilities have been discovered in Expat, the XML
parsing C library. Integer overflows or invalid shifts may lead to a denial of
service or other unspecified impact.

