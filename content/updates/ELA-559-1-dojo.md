---
title: "ELA-559-1 dojo security update"
package: dojo
version: 1.10.2+dfsg-1+deb8u4
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-02-06T14:03:01+05:30
draft: false
type: updates
cvelist:
  - CVE-2018-6561
  - CVE-2020-4051
  - CVE-2021-23450

---

Multiple vulnerabilities were found in src:dojo, as follows:

CVE-2018-6561

    `dijit.Editor` in Dojo allows XSS via the onload attribute
    of an SVG element.

CVE-2020-4051

    In Dijit, there is a cross-site scripting vulnerability in
    the Editor's LinkDialog plugin.

CVE-2021-23450

    It was found that Dojo is vulnerable to Prototype Pollution
    via the setObject function.
