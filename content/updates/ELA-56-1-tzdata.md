---
title: "ELA-56-1 tzdata new upstream version"
package: tzdata
version: 2018g-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "New version update"
date: 2018-11-11T22:43:13+01:00
draft: false
type: updates
cvelist:

---

This update brings the timezone changes from the upstream 2018g release.
