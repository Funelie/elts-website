---
title: "ELA-561-1 xterm security update"
package: xterm
version: 312-2+deb8u4
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2022-02-07T22:09:44+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-24130

---

xterm, an X terminal emulator, when Sixel support is enabled, allows attackers
to trigger a buffer overflow in set_sixel in graphics_sixel.c via crafted text.
