---
title: "ELA-564-1 libxstream-java security update"
package: libxstream-java
version: 1.4.11.1-1+deb8u5
distribution: "Debian 8 Jessie"
description: "Denial of Service (DoS) vulnerability"
date: 2022-02-15T13:48:11-08:00
draft: false
type: updates
cvelist:
  - CVE-2021-43859
---

It was discovered that there was a potential remote denial of service (DoS)
attack in XStream, a Java library used to serialize objects to XML and back
again.

An attacker could have consumed 100% of the CPU resources, but the library now
monitors and accumulates the time it takes to add elements to collections, and
throws an exception if a set threshold is exceeded.
