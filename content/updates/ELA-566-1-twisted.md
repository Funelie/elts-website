---
title: "ELA-566-1 twisted security update"
package: twisted
version: 14.0.2-3+deb8u3
distribution: "Debian 8 jessie"
description: "information leak"
date: 2022-02-19T17:31:27+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-21712

---

It was discovered that Twisted, a Python event-based framework for
internet applications, exposes cookies and authorization headers when
following cross-origin redirects. This issue is present in the
`twisted.web.RedirectAgent` and `twisted.web.BrowserLikeRedirectAgent`
functions.
