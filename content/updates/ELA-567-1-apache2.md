---
title: "ELA-567-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u20
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-02-19T17:07:49+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-44224
  - CVE-2021-44790

---

Two vulnerabilities have been discovered in the Apache HTTP server:

CVE-2021-44224

    When operating as a forward proxy, Apache was depending on the setup suspectable to denial of service or Server Side Request forgery.

CVE-2021-44790

    A buffer overflow in mod_lua may result in denial of service or potentially the execution of arbitrary code.

