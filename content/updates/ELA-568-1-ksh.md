---
title: "ELA-568-1 ksh security update"
package: ksh
version: 93u+20120801-1+deb8u1
distribution: "Debian 8 jessie"
description: "override or bypass environment restrictions"
date: 2022-02-20T01:36:38+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-14868

---

A flaw was found in the way it evaluates certain
environment variables. An attacker could use this
flaw to override or bypass environment restrictions
to execute shell commands. Services and
applications that allow remote unauthenticated
attackers to provide one of those environment
variables could allow them to exploit this issue
remotely.
