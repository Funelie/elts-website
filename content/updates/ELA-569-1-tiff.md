---
title: "ELA-569-1 tiff security update"
package: tiff
version: 4.0.3-12.3+deb8u13
distribution: "Debian 8 jessie"
description: "null pointer and out-of-bounds read issue"
date: 2022-02-24T00:04:42+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-0561
  - CVE-2022-0562
  - CVE-2022-22844

---

Several issues have been found in tiff, a library and tools to manipulate and convert files in the Tag Image File Format (TIFF).

CVE-2022-22844

    out-of-bounds read in _TIFFmemcpy in certain situations involving a
    custom tag and 0x0200 as the second word of the DE field.

CVE-2022-0562

    Null source pointer passed as an argument to memcpy() function within
    TIFFReadDirectory(). This could result in a Denial of Service via
    crafted TIFF files.

CVE-2022-0561

    Null source pointer passed as an argument to memcpy() function within
    TIFFFetchStripThing(). This could result in a Denial of Service via
    crafted TIFF files.

