---
title: "ELA-57-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.58-1+2018g
distribution: "Debian 7 Wheezy"
description: "New version update"
date: 2018-11-11T22:51:14+01:00
draft: false
type: updates
cvelist:

---

This update brings the Olson database changes from the 2018g version to
the Perl bindings.
