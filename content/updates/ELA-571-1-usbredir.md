---
title: "ELA-571-1 usbredir security update"
package: usbredir
version: 0.7-1+deb8u1
distribution: "Debian 8 jessie"
description: "user-after-free"
date: 2022-03-02T12:38:46+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3700

---

A use-after-free vulnerability was found in Usbredirparser, a parser
for the usbredir protocol, which could result in denial of service or
potentially arbitrary code execution.
