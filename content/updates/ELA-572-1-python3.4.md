---
title: "ELA-572-1 python3.4 security update"
package: python3.4
version: 3.4.2-1+deb8u12
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-03-03T16:59:47+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3177
  - CVE-2021-4189
  - CVE-2021-23336

---

Several vulnerabilities were found in Python 3.4, an interactive
high-level object-oriented language, that could result in denial
of service, port scanning, web cache poisoning or potentially
code execution.
