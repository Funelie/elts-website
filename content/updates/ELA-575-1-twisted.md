---
title: "ELA-575-1 twisted security update"
package: twisted
version: 14.0.2-3+deb8u4
distribution: "Debian 8 Jessie"
description: "Denial of Service (DoS) vulnerability"
date: 2022-03-08T12:04:24+00:00
draft: false
type: updates
cvelist:
  - CVE-2022-21716
---

It was discovered that there was an issue in the Twisted Python network
framework where SSH client and server implementations could accept an infinite
amount of data for the peer's SSH version identifier and that a buffer then
uses all available memory.
