---
title: "ELA-576-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u5
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-03-15T23:40:01+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-20807
  - CVE-2021-3778
  - CVE-2021-3796
  - CVE-2021-3872
  - CVE-2021-3927
  - CVE-2021-3928
  - CVE-2021-3973
  - CVE-2021-3974
  - CVE-2021-3984
  - CVE-2021-4019
  - CVE-2021-4069
  - CVE-2021-4192
  - CVE-2021-4193
  - CVE-2022-0213
  - CVE-2022-0319
  - CVE-2022-0359
  - CVE-2022-0361
  - CVE-2022-0368
  - CVE-2022-0408
  - CVE-2022-0554
  - CVE-2022-0685
  - CVE-2022-0714
  - CVE-2022-0729

---

Multiple security vulnerabilities have been discovered in vim, an enhanced vi
editor. Buffer overflows, out-of-bounds reads and Null pointer derefrences may
lead to a denial of service (application crash) or other unspecified impact.
