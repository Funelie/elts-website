---
title: "ELA-577-1 linux-4.9 security update"
package: linux-4.9
version: 4.9.303-1~deb8u1
distribution: "Debian 8 jessie"
description: "linux kernel update"
date: 2022-03-16T08:57:05+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-3640
  - CVE-2021-3752
  - CVE-2021-4002
  - CVE-2021-4083
  - CVE-2021-4155
  - CVE-2021-4202
  - CVE-2021-28711
  - CVE-2021-28712
  - CVE-2021-28713
  - CVE-2021-28714
  - CVE-2021-28715
  - CVE-2021-29264
  - CVE-2021-33033
  - CVE-2021-39685
  - CVE-2021-39686
  - CVE-2021-39698
  - CVE-2021-39714
  - CVE-2021-43976
  - CVE-2021-45095
  - CVE-2022-0001
  - CVE-2022-0002
  - CVE-2022-0330
  - CVE-2022-0435
  - CVE-2022-0487
  - CVE-2022-0492
  - CVE-2022-0617
  - CVE-2022-24448
  - CVE-2022-25258
  - CVE-2022-25375

---

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-3640

    LinMa of BlockSec Team discovered a race condition in the
    Bluetooth SCO implementation that can lead to a use-after-free.  A
    local user could exploit this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.

CVE-2021-3752

    Likang Luo of NSFOCUS Security Team discovered a flaw in the
    Bluetooth L2CAP implementation that can lead to a user-after-free.
    A local user could exploit this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.

CVE-2021-4002

    It was discovered that hugetlbfs, the virtual filesystem used by
    applications to allocate huge pages in RAM, did not flush the
    CPU's TLB in one case where it was necessary.  In some
    circumstances a local user would be able to read and write huge
    pages after they are freed and reallocated to a different process.
    This could lead to privilege escalation, denial of service or
    information leaks.

CVE-2021-4083

    Jann Horn reported a race condition in the local (Unix) sockets
    garbage collector, that can lead to use-after-free.  A local user
    could exploit this to cause a denial of service (memory corruption
    or crash) or possibly for privilege escalation.

CVE-2021-4155

    Kirill Tkhai discovered a data leak in the way the XFS_IOC_ALLOCSP
    IOCTL in the XFS filesystem allowed for a size increase of files
    with unaligned size. A local attacker can take advantage of this
    flaw to leak data on the XFS filesystem.

CVE-2021-4202

    Lin Ma discovered a race condition in the NCI (NFC Controller
    Interface) driver, which could lead to a use-after-free.  A local
    user could exploit this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.

    This protocol is not enabled in Debian's official kernel
    configurations.

CVE-2021-28711, CVE-2021-28712, CVE-2021-28713 (XSA-391)

    Juergen Gross reported that malicious PV backends can cause a denial
    of service to guests being serviced by those backends via high
    frequency events, even if those backends are running in a less
    privileged environment.

CVE-2021-28714, CVE-2021-28715 (XSA-392)

    Juergen Gross discovered that Xen guests can force the Linux
    netback driver to hog large amounts of kernel memory, resulting in
    denial of service.

CVE-2021-29264

    It was discovered that the "gianfar" Ethernet driver used with
    some Freescale SoCs did not correctly handle a Rx queue overrun
    when jumbo packets were enabled.  On systems using this driver and
    jumbo packets, an attacker on the network could exploit this to
    cause a denial of service (crash).

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2021-33033

    The syzbot tool found a reference counting bug in the CIPSO
    implementation that can lead to a use-after-free.

    This protocol is not enabled in Debian's official kernel
    configurations.

CVE-2021-39685

    Szymon Heidrich discovered a buffer overflow vulnerability in the
    USB gadget subsystem, resulting in information disclosure, denial of
    service or privilege escalation.

CVE-2021-39686

    A race condition was discovered in the Android binder driver, that
    could lead to incorrect security checks.  On systems where the
    binder driver is loaded, a local user could exploit this for
    privilege escalation.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2021-39698

    Linus Torvalds reported a flaw in the file polling implementation,
    which could lead to a use-after-free.  A local user could exploit
    this for denial of service (memory corruption or crash) or
    possibly for privilege escalation.

CVE-2021-39714

    A potential reference count overflow was found in the Android Ion
    driver.  On systems where the Ion driver is loaded, a local user
    could exploit this for denial of service (memory corruption or
    crash) or possibly for privilege escalation.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2021-43976

    Zekun Shen and Brendan Dolan-Gavitt discovered a flaw in the
    mwifiex_usb_recv() function of the Marvell WiFi-Ex USB Driver. An
    attacker able to connect a crafted USB device can take advantage of
    this flaw to cause a denial of service.

CVE-2021-45095

    It was discovered that the Phone Network protocol (PhoNet) driver
    has a reference count leak in the pep_sock_accept() function.

CVE-2022-0001 (INTEL-SA-00598)

    Researchers at VUSec discovered that the Branch History Buffer in
    Intel processors can be exploited to create information side-
    channels with speculative execution.  This issue is similar to
    Spectre variant 2, but requires additional mitigations on some
    processors.

    This can be exploited to obtain sensitive information from a
    different security context, such as from user-space to the kernel,
    or from a KVM guest to the kernel.

CVE-2022-0002 (INTEL-SA-00598)

    This is a similar issue to CVE-2022-0001, but covers exploitation
    within a security context, such as from JIT-compiled code in a
    sandbox to hosting code in the same process.

    This can be partly mitigated by disabling eBPF for unprivileged
    users with the sysctl: kernel.unprivileged_bpf_disabled=2.  This
    update does that by default.

CVE-2022-0330

    Sushma Venkatesh Reddy discovered a missing GPU TLB flush in the
    i915 driver, resulting in denial of service or privilege escalation.

CVE-2022-0435

    Samuel Page and Eric Dumazet reported a stack overflow in the
    networking module for the Transparent Inter-Process Communication
    (TIPC) protocol, resulting in denial of service or potentially the
    execution of arbitrary code.

CVE-2022-0487

    A use-after-free was discovered in the MOXART SD/MMC Host Controller
    support driver. This flaw does not impact the Debian binary packages
    as CONFIG_MMC_MOXART is not set.

CVE-2022-0492

    Yiqi Sun and Kevin Wang reported that the cgroup-v1 subsystem does
    not properly restrict access to the release-agent feature. A local
    user can take advantage of this flaw for privilege escalation and
    bypass of namespace isolation.

CVE-2022-0617

    butt3rflyh4ck discovered a NULL pointer dereference in the UDF
    filesystem. A local user that can mount a specially crafted UDF
    image can use this flaw to crash the system.

CVE-2022-24448

    Lyu Tao reported a flaw in the NFS implementation in the Linux
    kernel when handling requests to open a directory on a regular file,
    which could result in a information leak.

CVE-2022-25258

    Szymon Heidrich reported the USB Gadget subsystem lacks certain
    validation of interface OS descriptor requests, resulting in memory
    corruption.

CVE-2022-25375

    Szymon Heidrich reported that the RNDIS USB gadget lacks validation
    of the size of the RNDIS_MSG_SET command, resulting in information
    leak from kernel memory.
