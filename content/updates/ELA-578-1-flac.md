---
title: "ELA-578-1 flac security update"
package: flac
version: 1.3.0-3+deb8u2
distribution: "Debian 8 jessie"
description: "out of bounds"
date: 2022-03-17T02:42:18+05:30
draft: false
type: updates
cvelist:
  - CVE-2021-0561

---

In append_to_verify_fifo_interleaved_ of stream_encoder.c, there is
a possible out of bounds write due to a missing bounds check. This
could lead to local information disclosure with no additional
execution privileges needed.
