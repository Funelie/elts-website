---
title: "ELA-579-1 debian-archive-keyring update"
package: debian-archive-keyring
version: 2017.5~deb8u2
distribution: "Debian 8 jessie"
description: "Added buster and bullseye keys"
date: 2022-03-16T22:29:39+01:00
draft: false
type: updates
cvelist:

---

debian-archive-keyring is a package containing GnuPG archive keys of the Debian
archive. New GPG-keys are being constantly added with every new Debian release.


For Debian 8 Jessie, GPG-keys for 10/buster and 11/bullseye Debian release are added
in the version 2017.5~deb8u2.
