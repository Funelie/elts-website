---
title: "ELA-58-1 tiff3 security update"
package: tiff3
version: 3.9.6-11+deb7u13
distribution: "Debian 7 Wheezy"
description: "fix for out of bound write"
date: 2018-11-17T19:33:15+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-18557
---

Out-of-bounds write due to ignoring buffer size can cause a denial of service (application crash) or possibly have unspecified other impact via a crafted image file.


