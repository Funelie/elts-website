---
title: "ELA-580-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u17
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-03-17T10:54:55+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-0778

---

Tavis Ormandy discovered that the BN_mod_sqrt() function of OpenSSL
could be tricked into an infinite loop. This could result in denial of
service via malformed certificates.
