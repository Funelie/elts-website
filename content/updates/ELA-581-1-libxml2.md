---
title: "ELA-581-1 libxml2 security update"
package: libxml2
version: 2.9.1+dfsg1-5+deb8u12
distribution: "Debian 8 jessie"
description: "use-after-free"
date: 2022-03-17T21:55:05+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-23308

---

One vulnerability has been discovered in the libxml2: GNOME XML library.

CVE-2022-23308

    the application that validates XML using xmlTextReaderRead() with XML_PARSE_DTDATTR
    and XML_PARSE_DTDVALID enabled becomes vulnerable to this use-after-free bug.
    This issue can result in denial of service.
