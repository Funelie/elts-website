---
title: "ELA-582-1 wordpress security update"
package: wordpress
version: 4.1.35+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-03-21T00:02:47+05:30
draft: false
type: updates
cvelist:

---

Several vulnerabilities like Prototype Pollution Vulnerability in a
jQuery dependency and in the block editor, and Stored Cross Site
Scripting Vulnerability were discovered in Wordpress, a web blogging
tool.
