---
title: "ELA-583-1 paramiko security update"
package: paramiko
version: 1.15.1-1+deb8u2
distribution: "Debian 8 Jessie"
description: "information disclosure"
date: 2022-03-21T11:50:23+00:00
draft: false
type: updates
cvelist:
  - CVE-2022-24302
---

It was discovered that there was a potential race condition in Paramiko, a
pure-Python implementation of the SSH algorithm. In particular, unauthorised
information disclosure could have occurred during the creation of SSH private
keys.
