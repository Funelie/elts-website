---
title: "ELA-584-1 bind9 security update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u24
distribution: "Debian 8 jessie"
description: "cache poisoning"
date: 2022-03-21T15:18:53+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-25220

---

It was found that bind9, an internet domain name server, was vulnerable to
cache poisoning. When using forwarders, bogus NS records supplied by, or via,
those forwarders may be cached and used by named if it needs to recurse for any
reason, causing it to obtain and pass on potentially incorrect answers.
