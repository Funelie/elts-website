---
title: "ELA-584-2 bind9 regression update"
package: bind9
version: 1:9.9.5.dfsg-9+deb8u24
distribution: "Debian 8 jessie"
description: "regression in isc-dhcp-client"
date: 2022-03-23T01:24:27+01:00
draft: false
type: updates
cvelist:

---

The patch for CVE-2021-25220 caused a regression in the isc-dhcp-client package
which prevented network configuration via the dhclient. This patch has been
reverted until the regression can be properly addressed.

