---
title: "ELA-585-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u22
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2022-03-22T08:33:32+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-22719
  - CVE-2022-22720
  - CVE-2022-22721
  - CVE-2022-23943

---

Several vulnerabilities have been discovered in the Apache HTTP server,
which could result in denial of service, request smuggling or buffer
overflows.
