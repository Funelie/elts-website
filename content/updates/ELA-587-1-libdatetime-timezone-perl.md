---
title: "ELA-587-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.75-2+2022a
distribution: "Debian 8 jessie"
description: "update to tzdata 2022a"
date: 2022-03-29T19:23:28+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2022a for the
Perl bindings.

