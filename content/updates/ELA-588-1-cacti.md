---
title: "ELA-588-1 cacti security update"
package: cacti
version: 0.8.8b+dfsg-8+deb8u10
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-03-29T23:12:48+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-10060
  - CVE-2018-10061
  - CVE-2020-13230
  - CVE-2020-23226
  - CVE-2021-23225
  - CVE-2022-0730

---

Multiple vulnerabilities were discovered in Cacti, a web interface for
graphing of monitoring systems, leading to authentication bypass and
cross-site scripting (XSS). An attacker may get access to unauthorized
areas and impersonate other users, under certain conditions.

* CVE-2018-10060

    Cacti has XSS because it does not properly reject unintended
    characters, related to use of the sanitize_uri function in
    lib/functions.php.

* CVE-2018-10061

    Cacti has XSS because it makes certain htmlspecialchars calls
    without the ENT_QUOTES flag (these calls occur when the
    html_escape function in lib/html.php is not used).

* CVE-2020-13230

    Disabling a user account does not immediately invalidate any
    permissions granted to that account (e.g., permission to view
    logs).

* CVE-2020-23226

    Multiple Cross Site Scripting (XSS) vulnerabilities exist in
    multiple files.

* CVE-2021-23225

    Cacti allows authenticated users with User Management permissions
    to inject arbitrary web script or HTML in the "new_username" field
    during creation of a new user via "Copy" method at user_admin.php.

* CVE-2022-0730

    Under certain ldap conditions, Cacti authentication can be
    bypassed with certain credential types.

Additionally, follow-up fixes were included for CVE-2019-11025
(DLA-1757-1) and CVE-2020-7106 (DLA-2069-1).
