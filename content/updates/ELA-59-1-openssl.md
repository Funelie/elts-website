---
title: "ELA-59-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb7u7
distribution: "Debian 7 Wheezy"
description: "fix for different timing vulnerabilities"
date: 2018-11-21T19:46:50+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-0735
  - CVE-2018-5407
---

CVE-2018-0735
     Samuel Weiser reported a timing vulnerability in the OpenSSL ECDSA signature generation, which might leak information to recover the private key.

CVE-2018-5407
     Alejandro Cabrera Aldaya, Billy Brumley, Sohaib ul Hassan, Cesar Pereida Garcia and Nicola Tuveri reported a vulnerability to a timing side channel attack, which might be used to recover the private key.

