---
title: "ELA-591-1 minidlna security update"
package: minidlna
version: 1.1.2+dfsg-1.1+deb8u1
distribution: "Debian 8 jessie"
description: "DNS rebinding"
date: 2022-04-10T00:26:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-26505

---

Validate HTTP requests to protect against DNS rebinding, thus forbid a remote web server to exfiltrate media files.
