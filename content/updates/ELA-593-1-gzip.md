---
title: "ELA-593-1 gzip security update"
package: gzip
version: 1.6-4+deb8u1
distribution: "Debian 8 jessie"
description: "arbitrary-file-write vulnerability"
date: 2022-04-10T18:42:35+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-1271

---

An arbitrary-file-write vulnerability was discovered in gzip, which
provides GNU compression utilities.
