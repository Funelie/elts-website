---
title: "ELA-594-1 xz-utils security update"
package: xz-utils
version: 5.1.1alpha+20120614-2+deb8u1
distribution: "Debian 8 jessie"
description: "arbitrary-file-write vulnerability"
date: 2022-04-10T18:46:15+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-1271

---

An arbitrary-file-write vulnerability was discovered in xz-utils,
which provides XZ-format compression utilities.
