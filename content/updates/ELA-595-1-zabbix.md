---
title: "ELA-595-1 zabbix security update"
package: zabbix
version: 1:2.2.23+dfsg-0+deb8u4
distribution: "Debian 8 jessie"
description: "reflected XSS"
date: 2022-04-11T00:26:02+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-24349
  - CVE-2022-24919

---

Several security vulnerabilities have been discovered in zabbix, a network
monitoring solution. An authenticated user can create a link with reflected
Javascript code inside it for graphs and actions pages and send it to other
users. The payload can be executed only with a known CSRF token value of the
victim, which is changed periodically and is difficult to predict.

