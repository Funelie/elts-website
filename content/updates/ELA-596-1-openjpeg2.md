---
title: "ELA-596-1 openjpeg2 security update"
package: openjpeg2
version: 2.1.0-2+deb8u13
distribution: "Debian 8 jessie"
description: "null pointer dereference, out-of-bounds reads, integer overflow"
date: 2022-04-12T21:48:44+02:00
draft: false
type: updates
cvelist:
  - CVE-2020-27842
  - CVE-2020-27843
  - CVE-2021-29338

---

Multiple vulnerabilities have been discovered in openjpeg2, the open-source
JPEG 2000 codec.

CVE-2020-27842

    Null pointer dereference through specially crafted input. The highest impact
    of this flaw is to application availability.


CVE-2020-27843

    The flaw allows an attacker to provide specially crafted input to the
    conversion or encoding functionality, causing an out-of-bounds read. The
    highest threat from this vulnerability is system availability.


CVE-2021-29338

    Integer overflow allows remote attackers to crash the application, causing a
    denial of service. This occurs when the attacker uses the command line
    option "-ImgDir" on a directory that contains 1048576 files.