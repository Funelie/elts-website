---
title: "ELA-597-1 lrzip security update"
package: lrzip
version: 0.616-1+deb8u1
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-04-13T15:00:12+02:00
draft: false
type: updates
cvelist:
  - CVE-2017-8842
  - CVE-2017-8843
  - CVE-2017-8844
  - CVE-2017-8845
  - CVE-2017-8846
  - CVE-2017-8847
  - CVE-2017-9928
  - CVE-2017-9929
  - CVE-2018-5650
  - CVE-2018-5747
  - CVE-2018-5786
  - CVE-2018-9058
  - CVE-2018-10685
  - CVE-2018-11496
  - CVE-2020-25467
  - CVE-2021-27345
  - CVE-2021-27347
  - CVE-2022-26291

---

Several security vulnerabilities have been discovered in lrzip, a
compression program. Heap-based and stack buffer overflows,
use-after-free and infinite loops would allow attackers to cause a
denial of service or possibly other unspecified impact via a crafted
compressed file.

* CVE-2017-8842

    The bufRead::get() function in libzpaq/libzpaq.h allows remote
    attackers to cause a denial of service (divide-by-zero error and
    application crash) via a crafted archive.

* CVE-2017-8843

    The join_pthread function in stream.c allows remote attackers to
    cause a denial of service (NULL pointer dereference and
    application crash) via a crafted archive.

* CVE-2017-8844

    The read_1g function in stream.c allows remote attackers to cause
    a denial of service (heap-based buffer overflow and application
    crash) or possibly have unspecified other impact via a crafted
    archive.

* CVE-2017-8845

    The lzo1x_decompress function in lzo1x_d.ch in LZO, as used in
    lrzip, allows remote attackers to cause a denial of service
    (invalid memory read and application crash) via a crafted archive.

* CVE-2017-8846

    The read_stream function in stream.c allows remote attackers to
    cause a denial of service (use-after-free and application crash)
    via a crafted archive.

* CVE-2017-8847

    The bufRead::get() function in libzpaq/libzpaq.h allows remote
    attackers to cause a denial of service (NULL pointer dereference
    and application crash) via a crafted archive.

* CVE-2017-9928

    A stack buffer overflow was found in the function get_fileinfo,
    which allows attackers to cause a denial of service via a crafted
    file.

* CVE-2017-9929

    A stack buffer overflow was found in the function get_fileinfo,
    which allows attackers to cause a denial of service via a crafted
    file.

* CVE-2018-5650

    There is an infinite loop and application hang in the unzip_match
    function in runzip.c. Remote attackers could leverage this
    vulnerability to cause a denial of service via a crafted lrz file.

* CVE-2018-5747

    There is a use-after-free in the ucompthread function
    (stream.c). Remote attackers could leverage this vulnerability to
    cause a denial of service via a crafted lrz file.

* CVE-2018-5786

    There is an infinite loop and application hang in the get_fileinfo
    function (lrzip.c). Remote attackers could leverage this
    vulnerability to cause a denial of service via a crafted lrz file.

* CVE-2018-9058

    There is an infinite loop in the runzip_fd function of
    runzip.c. Remote attackers could leverage this vulnerability to
    cause a denial of service via a crafted lrz file.

* CVE-2018-10685

    There is a use-after-free in the lzma_decompress_buf function of
    stream.c, which allows remote attackers to cause a denial of
    service (application crash) or possibly have unspecified other
    impact.

* CVE-2018-11496

    There is a use-after-free in read_stream in stream.c, because
    decompress_file in lrzip.c lacks certain size validation.

* CVE-2020-25467

    A null pointer dereference was discovered lzo_decompress_buf in
    stream.c which allows an attacker to cause a denial of service
    (DOS) via a crafted compressed file.

* CVE-2021-27345

    A null pointer dereference was discovered in ucompthread in
    stream.c which allows attackers to cause a denial of service (DOS)
    via a crafted compressed file.

* CVE-2021-27347

    Use after free in lzma_decompress_buf function in stream.c in
    allows attackers to cause Denial of Service (DoS) via a crafted
    compressed file.

* CVE-2022-26291

    lrzip was discovered to contain a multiple concurrency
    use-after-free between the functions zpaq_decompress_buf() and
    clear_rulist(). This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted lrz file.
