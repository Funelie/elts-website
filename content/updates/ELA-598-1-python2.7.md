---
title: "ELA-598-1 python2.7 security update"
package: python2.7
version: 2.7.9-2-ds1-1+deb8u9
distribution: "Debian 8 jessie"
description: "multiple vulnerabilities"
date: 2022-04-14T20:28:05+05:30
draft: false
type: updates
cvelist:
  - CVE-2019-16935
  - CVE-2021-3177
  - CVE-2021-4189

---

Multiple vulnerabilities were found in src:python2.7, the Python interpreter.


CVE-2019-16935

    The documentation XML-RPC server in Python has XSS via the server_title
    field. This occurs in Lib/DocXMLRPCServer.py. If set_server_title is called
    with untrusted input, arbitrary JavaScript can be delivered to clients that
    visit the http URL for this server.

CVE-2021-3177

    Python has a buffer overflow in PyCArg_repr in _ctypes/callproc.c, which
    may lead to remote code execution in certain Python applications that
    accept floating-point numbers as untrusted input.

CVE-2021-4189

    A flaw was found in Python, specifically in the FTP (File Transfer Protocol)
    client library when using it in PASV (passive) mode. The flaw lies in how
    the FTP client trusts the host from PASV response by default. An attacker
    could use this flaw to setup a malicious FTP server that can trick FTP
    clients into connecting back to a given IP address and port. This could lead
    to FTP client scanning ports which otherwise would not have been possible.
    .
    Instead of using the returned address, ftplib now uses the IP address we're
    already connected to. For the rare user who wants an old behavior, set a
    `trust_server_pasv_ipv4_address` attribute on your `ftplib.FTP` instance to
    True.
