---
title: "ELA-6-1 ghostscript security update"
package: ghostscript
version: 9.05~dfsg-6.3+deb7u9
distribution: "Debian 7 Wheezy"
description: "potential information disclosure"
date: 2018-06-23T15:22:58Z
draft: false
type: updates
cvelist:
  - CVE-2018-11645
---

A vulnerability was discovered in Ghostscript, the GPL PostScript/PDF
interpreter, which may lead to the potential information disclosure
about files for which read permissions are not available.
