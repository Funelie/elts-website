---
title: "ELA-60-1 pixman security update"
package: pixman
version: 0.26.0-4+deb7u3
distribution: "Debian 7 Wheezy"
description: "numerical overflow in pointer arithmetic"
date: 2018-11-22T19:51:25+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-5297
---

An integer overflow issue has been reported in pixman prior to version 0.32.8. An attacker could cause an application using pixman to crash or, potentially, execute arbitrary code.

