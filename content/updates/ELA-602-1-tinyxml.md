---
title: "ELA-602-1 tinyxml security update"
package: tinyxml
version: 2.6.2-2+deb8u1
distribution: "Debian 8 jessie"
description: "infinite loop"
date: 2022-04-30T16:46:17+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-42260

---

An issue has been found in tinyxml, a C++ XML parsing library.
Crafted XML messages could lead to an infinite loop in
TiXmlParsingData::Stamp(), which results in a denial of service.
