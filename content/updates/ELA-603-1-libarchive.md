---
title: "ELA-603-1 libarchive security update"
package: libarchive
version: 3.1.2-11+deb8u9
distribution: "Debian 8 jessie"
description: "out-of-bounds read and incorrect symlink handling"
date: 2022-04-30T16:50:21+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-19221
  - CVE-2021-23177
  - CVE-2021-31566

---

Three issues have been found in libarchive, a multi-format archive and compression library.

CVE-2021-31566
     symbolic links incorrectly followed when changing modes, times, ACL
     and flags of a file while extracting an archive

CVE-2021-23177
     extracting a symlink with ACLs modifies ACLs of target

CVE-2019-19221
     out-of-bounds read because of an incorrect mbrtowc or mbtowc call
