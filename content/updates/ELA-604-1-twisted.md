---
title: "ELA-604-1 twisted security update"
package: twisted
version: 14.0.2-3+deb8u5
distribution: "Debian 8 jessie"
description: "HTTP request smuggling"
date: 2022-05-01T15:35:14+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-24801

---

Twisted is an event-based Python framework for internet applications. The
Twisted Web HTTP 1.1 server parsed several HTTP request constructs more
leniently than permitted by RFC 7230. This non-conformant parsing can lead to
desync if requests pass through multiple HTTP parsers, potentially resulting in
HTTP request smuggling.

