---
title: "ELA-607-1 mutt security update"
package: mutt
version: 1.5.23-3+deb8u6
distribution: "Debian 8 jessie"
description: "buffer overflow"
date: 2022-05-13T04:56:35+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-1328

---

It was discovered that Mutt, a text-based mailreader supporting MIME,
GPG, PGP and threading, incorrectly handled certain input. An attacker
could possibly use this issue to cause a crash, or expose sensitive
information.
