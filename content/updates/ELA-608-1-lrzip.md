---
title: "ELA-608-1 lrzip security update"
package: lrzip
version: 0.616-1+deb8u2
distribution: "Debian 8 jessie"
description: "heap memory corruption"
date: 2022-05-13T20:49:28+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-28044

---

src:lrzip, a compression program with a very high compression
ratio, was discovered to contain a heap memory corruption via
the component lrzip.c:initialise_control.
