---
title: "ELA-609-1 ruby-nokogiri security update"
package: ruby-nokogiri
version: 1.6.3.1+ds-1+deb8u3
distribution: "Debian 8 Jessie"
description: "denial of service vulnerability"
date: 2022-05-13T10:39:16-07:00
draft: false
type: updates
cvelist:
  - CVE-2022-24836
---

It was discovered that there was a potential denial of service attack in
ruby-nokogiri, a HTML, XML, SAX etc. parser written in/for the Ruby programming
language. This was caused by the use of inefficient regular expressions that
were susceptible to excessive backtracking.
