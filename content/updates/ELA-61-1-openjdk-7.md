---
title: "ELA-61-1 openjdk-7 security update"
package: openjdk-7
version: 7u181-2.6.14-2~deb7u1
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-11-22T23:17:42+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-2952
  - CVE-2018-3136
  - CVE-2018-3139
  - CVE-2018-3149
  - CVE-2018-3169
  - CVE-2018-3180
  - CVE-2018-3214
---

Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, incomplete TLS identity verification,
information disclosure or the execution of arbitrary code.
