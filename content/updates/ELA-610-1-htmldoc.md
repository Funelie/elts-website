---
title: "ELA-610-1 htmldoc security update"
package: htmldoc
version: 1.8.27-8+deb8u4
distribution: "Debian 8 Jessie"
description: "integer overflow vulnerability"
date: 2022-05-13T12:59:19-07:00
draft: false
type: updates
cvelist:
  - CVE-2022-27114
---

It was discovered that there was an integer overflow vulnerability in htmldoc,
a HTML processor that generates indexed HTML, PS and PDF files. This was caused
by a programming error in the `image_load_jpeg` function due to a conflation or
confusion of declared/expected/observed image dimensions.
