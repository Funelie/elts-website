---
title: "ELA-611-1 libgoogle-gson-java security update"
package: libgoogle-gson-java
version: 2.2.4-1+deb8u1
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-05-14T03:12:59+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-25647

---

src:libgoogle-gson-java, which helps convert Java objects into their JSON
representation, is vulnerable to Deserialization of Untrusted Data via
the writeReplace() method in internal classes, which may lead to DoS attacks.
