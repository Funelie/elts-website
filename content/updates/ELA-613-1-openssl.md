---
title: "ELA-613-1 openssl security update"
package: openssl
version: 1.0.1t-1+deb8u18
distribution: "Debian 8 jessie"
description: "shell command injection"
date: 2022-05-16T08:08:29-04:00
draft: false
type: updates
cvelist:
  - CVE-2022-1292

---

The `c_rehash` script does not properly sanitise shell metacharacters to
prevent command injection. This script is executed by
`update-ca-certificates`, from `ca-certificates`, to re-hash
certificates in `/etc/ssl/certs/`. An attacker able to place files in
this directory could execute arbitrary commands with the privileges of
the script.
