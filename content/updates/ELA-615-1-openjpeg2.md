---
title: "ELA-615-1 openjpeg2 security update"
package: openjpeg2
version: 2.1.0-2+deb8u14
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-05-17T21:39:18-04:00
draft: false
type: updates
cvelist:
  - CVE-2022-1122

---

A flaw was found in the `opj2_decompress` program in `openjpeg2` in the
way it handles an input directory with a large number of files.
When it fails to allocate a buffer to store the filenames of the input
directory, it calls `free()` on an uninitialized pointer, leading to a
segmentation fault and a denial of service.
