---
title: "ELA-618-1 openldap security update"
package: openldap
version: 2.4.40+dfsg-1+deb8u11
distribution: "Debian 8 jessie"
description: "SQL injection vulnerability"
date: 2022-05-27T01:15:19+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-29155

---

Jacek Konieczny discovered a SQL injection vulnerability in the back-sql
backend to slapd in OpenLDAP, a free implementation of the Lightweight
Directory Access Protocol, allowing an attacker to alter the database
during an LDAP search operations when a specially crafted search filter
is processed.
