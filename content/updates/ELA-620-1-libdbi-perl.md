---
title: "ELA-620-1 libdbi-perl security update"
package: libdbi-perl
version: 1.631-3+deb8u2
distribution: "Debian 8 jessie"
description: "information disclosure"
date: 2022-05-30T21:50:39+02:00
draft: false
type: updates
cvelist:
  - CVE-2014-10402

---

It was discovered that CVE-2014-10401 was fixed incompletely in the
Perl5 Database Interface (DBI).  An attacker could trigger information
disclosure through a different vector.

* CVE-2014-10401

    DBD::File drivers can open files from folders other than those
    specifically passed via the f_dir attribute.

* CVE-2014-10402

    DBD::File drivers can open files from folders other than those
    specifically passed via the f_dir attribute in the data source
    name (DSN). NOTE: this issue exists because of an incomplete fix
    for CVE-2014-10401.
