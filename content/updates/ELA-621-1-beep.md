---
title: "ELA-621-1 beep security update"
package: beep
version: 1.3-3+deb8u2
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-06-04T02:08:01+05:30
draft: false
type: updates
cvelist:
  - CVE-2018-1000532

---

beep, an advanced PC-speaker beeper, contains a External Control of
File Name or Path vulnerability in --device option that can result
in Local unprivileged user can inhibit execution of arbitrary programs
by other users, allowing DoS. This attack appear to be exploitable
via The system must allow local users to run beep.
