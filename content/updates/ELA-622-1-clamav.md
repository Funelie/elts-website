---
title: "ELA-622-1 clamav security update"
package: clamav
version: 0.103.6+dfsg-0+deb8u1
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2022-06-06T17:11:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-20770
  - CVE-2022-20771
  - CVE-2022-20785
  - CVE-2022-20792
  - CVE-2022-20796

---

Several vulnerabilities have been found in the ClamAV antivirus toolkit,
that could result in denial of service or other unspecified impact.
