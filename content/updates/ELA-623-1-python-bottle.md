---
title: "ELA-623-1 python-bottle security update"
package: python-bottle
version: 0.12.7-1+deb8u4
distribution: "Debian 8 jessie"
description: "mishandling errors"
date: 2022-06-09T01:18:08+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-31799

---

Bottle, which is a fast, simple and lightweight WSGI micro
web-framework for Pytho, mishandles errors during early request
binding.
