---
title: "ELA-624-1 avahi security update"
package: avahi
version: 0.6.31-5+deb8u1
distribution: "Debian 8 jessie"
description: "denial of service"
date: 2022-06-14T13:35:18+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3468
  - CVE-2021-26720

---

It was discovered that the Debian package of Avahi, a framework for Multicast
DNS Service Discovery, executed the script avahi-daemon-check-dns.sh with root
privileges which would allow a local attacker to cause a denial of service or
create arbitrary empty files via a symlink attack on files under
/var/run/avahi-daemon. This script is now executed with the privileges of user
and group avahi and requires sudo in order to achieve that.

Furthermore it was found (CVE-2021-3468) that the event used to signal the
termination of the client connection on the avahi Unix socket is not correctly
handled in the client_work function, allowing a local attacker to trigger an
infinite loop.
