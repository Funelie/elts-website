---
title: "ELA-625-1 glib2.0 security update"
package: glib2.0
version: 2.42.1-1+deb8u4
distribution: "Debian 8 jessie"
description: "several vulnerabilities"
date: 2022-06-14T13:39:09+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-27218
  - CVE-2021-27219
  - CVE-2021-28153

---

Several security vulnerabilities were found in glib2.0, a general-purpose
utility library for the GNOME environment.

CVE-2021-27218

    If g_byte_array_new_take() was called with a buffer of 4GB or more on a
    64-bit platform, the length would be truncated modulo 2**32, causing
    unintended length truncation.

CVE-2021-27219

    The function g_bytes_new has an integer overflow on 64-bit platforms due to
    an implicit cast from 64 bits to 32 bits. The overflow could potentially
    lead to memory corruption.

CVE-2021-28153

    When g_file_replace() is used with G_FILE_CREATE_REPLACE_DESTINATION to
    replace a path that is a dangling symlink, it incorrectly also creates the
    target of the symlink as an empty file, which could conceivably have
    security relevance if the symlink is attacker-controlled. (If the path is
    a symlink to a file that already exists, then the contents of that file
    correctly remain unchanged.)
