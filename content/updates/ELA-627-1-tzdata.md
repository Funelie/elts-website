---
title: "ELA-627-1 tzdata new timezone database"
package: tzdata
version: 2021a-0+deb8u4
distribution: "Debian 8 jessie"
description: "new timezone database"
date: 2022-06-16T11:32:19+02:00
draft: false
type: updates
cvelist:

---

This update includes the latest changes to the leap second list,
including an update to its expiry date, which was set for the end of
June.
