---
title: "ELA-628-1 vim security update"
package: vim
version: 2:7.4.488-7+deb8u7 (jessie)
version_map: {"8 jessie": "2:7.4.488-7+deb8u7"}
description: "denial of service"
date: 2022-06-22T10:45:41+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3903
  - CVE-2022-0417
  - CVE-2022-0943
  - CVE-2022-1851
  - CVE-2022-1898
  - CVE-2022-1968
  - CVE-2022-2124
  - CVE-2022-2126

---

Multiple security vulnerabilities have been discovered in vim, an enhanced vi
editor. Buffer overflows, out-of-bounds reads and use-after-free may lead to a
denial-of-service (application crash) or other unspecified impact.

