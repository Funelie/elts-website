---
title: "ELA-629-1 libsndfile security update"
package: libsndfile
version: 1.0.25-9.1+deb8u7 (jessie)
version_map: {"8 jessie": "1.0.25-9.1+deb8u7"}
description: "heap out of bounds read"
date: 2022-06-26T13:29:56+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-4156

---

An issue has been found in libsndfile, a library for reading/writing
audio files.
Using a crafted FLAC file, an attacker could trigger an out-of-bounds
read that would most likely cause a crash but could potentially leak
memory information.

