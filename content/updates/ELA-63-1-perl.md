---
title: "ELA-63-1 perl security update"
package: perl
version: 5.14.2-21+deb7u8
distribution: "Debian 7 Wheezy"
description: "integer overflow"
date: 2018-12-03T13:33:51+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-18311
---

Jayakrishna Menon and Christophe Hauser discovered an integer
overflow vulnerability in Perl_my_setenv leading to a heap-based
buffer overflow with attacker-controlled input.
