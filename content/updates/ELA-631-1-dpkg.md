---
title: "ELA-631-1 dpkg security update"
package: dpkg
version: 1.17.28 (jessie)
version_map: {"8 jessie": "1.17.28"}
description: "directory traversal vulnerability"
date: 2022-07-03T17:20:53-04:00
draft: false
type: updates
cvelist:
  - CVE-2022-1664

---

Max Justicz reported a directory traversal vulnerability in
Dpkg::Source::Archive in dpkg, the Debian package management system.
This affects extracting untrusted source packages in the v2 and v3
source package formats that include a debian.tar.
