---
title: "ELA-632-1 apache2 security update"
package: apache2
version: 2.4.10-10+deb8u23 (jessie), 2.4.25-3+deb9u14 (stretch)
version_map: {"8 jessie": "2.4.10-10+deb8u23", "9 stretch": "2.4.25-3+deb9u14"}
description: "several vulnerabilities"
date: 2022-07-04T15:10:29-04:00
draft: false
type: updates
cvelist:
  - CVE-2022-26377
  - CVE-2022-28614
  - CVE-2022-28615
  - CVE-2022-29404
  - CVE-2022-30522
  - CVE-2022-30556
  - CVE-2022-31813

---

Several security vulnerabilities were found in the Apache HTTP server:

CVE-2022-26377

    Inconsistent Interpretation of HTTP Requests ('HTTP Request
    Smuggling') vulnerability in mod_proxy_ajp of Apache HTTP Server
    allows an attacker to smuggle requests to the AJP server it forwards
    requests to.

CVE-2022-28614

    The ap_rwrite() function read unintended memory if an attacker can
    cause the server to reflect very large input using ap_rwrite() or
    ap_rputs(), such as with mod_luas r:puts() function. Modules
    compiled and distributed separately from Apache HTTP Server that use
    the 'ap_rputs' function and may pass it a very large (INT_MAX or
    larger) string must be compiled against current headers to resolve
    the issue.

CVE-2022-28615

    Apache HTTP Server may crash or disclose information due to a read
    beyond bounds in ap_strcmp_match() when provided with an extremely
    large input buffer.

CVE-2022-29404

    In Apache HTTP Server, a malicious request to a lua script that
    calls r:parsebody(0) may cause a denial of service due to no default
    limit on possible input size.

CVE-2022-30522

    If Apache HTTP Server is configured to do transformations with
    mod_sed in contexts where the input to mod_sed may be very large,
    mod_sed may make excessively large memory allocations and trigger an
    abort.

CVE-2022-30556

    Apache HTTP Server may return lengths to applications calling
    r:wsread() that point past the end of the storage allocated for the
    buffer.

CVE-2022-31813

    Apache HTTP Server may not send the X-Forwarded-* headers to the
    origin server based on client side Connection header hop-by-hop
    mechanism. This may be used to bypass IP based authentication on the
    origin server/application.

