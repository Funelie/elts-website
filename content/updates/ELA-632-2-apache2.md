---
title: "ELA-632-2 apache2 regression update"
package: apache2
version: 2.4.10-10+deb8u24 (jessie)
version_map: {"8 jessie": "2.4.10-10+deb8u24"}
description: "loss of request parameters"
date: 2022-07-23T13:38:05-04:00
draft: false
type: updates
cvelist:

---

The patch for CVE-2022-31813 caused a regression in the apache2 package for
Debian 8 jessie, which resulted in some request parameters being lost in
`modproxy` and `modproxy_http` configurations.  This version corrects the
regression and implements the intended fix without request parameters being
lost.

Note that this regression only affects the apache2 package for Debian 8 jessie.
The apache2 package for Debian 9 stretch which was published under the original
advisory ELA-632-1 is not affected by this regression.
