---
title: "ELA-633-1 linux-4.19 new kernel version"
package: linux-4.19
version: 4.19.232-1~deb8u1 (jessie)
version_map: {"8 jessie": "4.19.232-1~deb8u1"}
description: "new linux kernel backport"
date: 2022-07-11T12:51:10+02:00
draft: false
type: updates
cvelist:

---

This update introduces Linux kernel 4.19 to Debian 8 jessie. Previous
kernels 3.16 and 4.9 are no longer supported. Instructions on how to
update to 4.19 can be found [in the kernel backports page](../../docs/kernel-backport).
