---
title: "ELA-634-1 linux-5.10 new kernel version"
package: linux-5.10
version: 5.10.120-1~deb9u1 (stretch)
version_map: {"9 stretch": "5.10.120-1~deb9u1"}
description: "new linux kernel backport"
date: 2022-07-11T13:44:41+02:00
draft: false
type: updates
cvelist:

---

This update introduces Linux kernel 5.10 to Debian 9 stretch. This kernel will be
supported along with 4.19, but for a longer period. Linux 4.9 is no longer supported.
Instructions on how to update to 5.10 and support periods can be found
[in the kernel backports page](../../docs/kernel-backport).
