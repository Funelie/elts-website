---
title: "ELA-635-1 wireless-regdb new wireless regulatory database"
package: wireless-regdb
version: 2022.04.08-1~deb9u1 (stretch)
version_map: {"9 stretch": "2022.04.08-1~deb9u1"}
description: "updates wireless regulatory database"
date: 2022-07-11T14:12:33+02:00
draft: false
type: updates
cvelist:

---

This update includes the latest changes to the wireless regulatory database.
In addition, it allows the Linux 5.10 kernel to verify and autoload it.
