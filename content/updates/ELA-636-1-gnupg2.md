---
title: "ELA-636-1 gnupg2 security update"
package: gnupg2
version: 2.0.26-6+deb8u3 (jessie), 2.1.18-8~deb9u5 (stretch)
version_map: {"8 jessie": "2.0.26-6+deb8u3", "9 stretch": "2.1.18-8~deb9u5"}
description: "signature spoofing"
date: 2022-07-11T16:09:11+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-9234
  - CVE-2022-34903

---

CVE-2022-34903

    Demi Marie Obenour discovered a flaw in GnuPG, allowing for signature
    spoofing via arbitrary injection into the status line. An attacker who
    controls the secret part of any signing-capable key or subkey in the
    victim's keyring, can take advantage of this flaw to provide a
    correctly-formed signature that some software, including gpgme, will
    accept to have validity and signer fingerprint chosen from the attacker.

CVE-2018-9234

    GnuPG does not enforce a configuration in which key certification requires an
    offline master Certify key, which results in apparently valid certifications
    that occurred only with access to a signing subkey.
