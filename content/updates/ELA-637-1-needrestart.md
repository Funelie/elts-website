---
title: "ELA-637-1 needrestart security update"
package: needrestart
version: 1.2-8+deb8u2 (jessie)
version_map: {"8 jessie": "1.2-8+deb8u2"}
description: "privilege escalation"
date: 2022-07-12T10:02:52+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-30688

---

Jakub Wilk discovered a local privilege escalation in needrestart, a utility to
check which daemons need to be restarted after library upgrades. Regular
expressions to detect the Perl, Python, and Ruby interpreters are not anchored,
allowing a local user to escalate privileges when needrestart tries to detect
if interpreters are using old source files.
