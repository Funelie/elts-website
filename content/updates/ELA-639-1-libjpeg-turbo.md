---
title: "ELA-639-1 libjpeg-turbo security update"
package: libjpeg-turbo
version: 1:1.5.1-2+deb9u3 (stretch)
version_map: {"9 stretch": "1:1.5.1-2+deb9u3"}
description: "heap-based buffer overflow"
date: 2022-07-12T19:05:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-46822

---

A heap-based buffer overflow vulnerability was found in the libjpeg-turbo image
library in the get_word_rgb_row() function in rdppm.c. This flaw allows a
remote attacker to persuade a victim to open a specially-crafted file, causing
the application to crash.
