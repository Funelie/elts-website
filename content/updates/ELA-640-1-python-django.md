---
title: "ELA-640-1 python-django security update"
package: python-django
version: 1:1.10.7-2+deb9u18 (stretch)
version_map: {"9 stretch": "1:1.10.7-2+deb9u18"}
description: "SQL injection vulnerability vulnerability"
date: 2022-07-13T15:23:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-34265
---

A SQL injection vulnerability was discovered in Django, the popular web
development framework.

The `Trunc()` and `Extract()` database functions were subject to SQL injection
if untrusted data is used as a `kind` or `lookup_name` value. Applications that
constrained the lookup name and kind choice to a "known", fixed or otherwise
safe list were unaffected.
