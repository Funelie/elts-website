---
title: "ELA-641-1 strongswan security update"
package: strongswan
version: 5.2.1-6+deb8u9 (jessie)
version_map: {"8 jessie": "5.2.1-6+deb8u9"}
description: "authentication bypass"
date: 2022-07-14T00:22:25+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-41991
  - CVE-2021-45079

---

CVE-2021-45079

    Zhuowei Zhang discovered a bug in the EAP authentication client code of
    strongSwan, an IKE/IPsec suite, that may allow to bypass the client and in
    some scenarios even the server authentication, or could lead to a
    denial-of-service attack.

CVE-2021-41991

    Researchers at the United States of America National Security Agency (NSA)
    identified a denial of service vulnerability in strongSwan.
    Once the in-memory certificate cache is full it tries to randomly replace
    lesser used entries. Depending on the generated random value, this could
    lead to an integer overflow that results in a double-dereference and a call
    using out-of-bounds memory that most likely leads to a segmentation fault.
    Remote code execution can't be ruled out completely, but attackers have no
    control over the dereferenced memory, so it seems unlikely at this point.
