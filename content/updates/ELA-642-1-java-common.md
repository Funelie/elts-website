---
title: "ELA-642-1 java-common new default java version"
package: java-common
version: 0.52+deb8u1 (jessie)
version_map: {"8 jessie": "0.52+deb8u1"}
description: "default to Java 8"
date: 2022-07-14T13:22:10+02:00
draft: false
type: updates
cvelist:

---

This update changes the default Java packages to Java 8, with Java 7
no longer being supported. When both versions are installed, the
java binary still defaults to Java 7. That will be changed in the
next openjdk-8 update, and in the meanwhile can be changed using the
`update-alternatives --config java` command, or by uninstalling the
Java 7 packages.
