---
title: "ELA-643-1 ncurses security update"
package: ncurses
version: 5.9+20140913-1+deb8u4 (jessie), 6.0+20161126-1+deb9u3 (stretch)
version_map: {"8 jessie": "5.9+20140913-1+deb8u4", "9 stretch": "6.0+20161126-1+deb9u3"}
description: "out-of-bounds read, missing checks, NULL pointer dereferencing"
date: 2022-07-14T23:34:57+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-19211
  - CVE-2019-17594
  - CVE-2019-17595
  - CVE-2022-29458

---

Several issues have been found in ncurses, a shared libraries for terminal handling.
These issues are about out-of-bounds reads, missing checks for strange input and NULL pointer dereferencing in different parts of the library.
