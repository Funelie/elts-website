---
title: "ELA-644-1 python-pysaml2 security update"
package: python-pysaml2
version: 2.0.0-1+deb8u4 (jessie)
version_map: {"8 jessie": "2.0.0-1+deb8u4"}
description: "certificate verification bypass vulnerability"
date: 2022-07-15T10:14:49+01:00
draft: false
type: updates
cvelist:
  - CVE-2021-21239
---

A certificate verification bypass vulnerability was discovered in
`python-pysaml2`, a library for exchanging SAML authentication tokens.

The default `CryptoBackendXmlSec1` backend used the `xmlsec1` binary to verify the
signature of signed SAML documents but, by default, `xmlsec1` accepted any type
of key found within the given document; `xmlsec1` actually needs to be
configured explicitly to only use only x509 certificates for the verification
process of the SAML document signature.
