---
title: "ELA-645-1 pjproject security update"
package: pjproject
version: 2.5.5~dfsg-6+deb9u6 (stretch)
version_map: {"9 stretch": "2.5.5~dfsg-6+deb9u6"}
description: "stack overflow vulnerability"
date: 2022-07-15T13:08:20+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-31031
---

There was a stack buffer overflow vulnerability in `pjproject`, a multimedia
communication library used in various VOIP frameworks. `pjproject` now
maintains a maximum attribute count to prevent this from happening.
