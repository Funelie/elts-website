---
title: "ELA-647-1 request-tracker4 security update"
package: request-tracker4
version: 4.2.8-3+deb8u4 (jessie), 4.4.1-3+deb9u5 (stretch)
version_map: {"8 jessie": "4.2.8-3+deb8u4", "9 stretch": "4.4.1-3+deb9u5"}
description: "multiple vulnerabilities"
date: 2022-07-18T08:15:13+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-38562
  - CVE-2022-25802

---

Multiple vulnerabilities have been discovered in Request Tracker, an
extensible trouble-ticket tracking system.

CVE-2022-25802

    It was discovered that Request Tracker is vulnerable to a cross-site
    scripting (XSS) attack when displaying attachment content with fraudulent
    content types.

Additionally it was discovered that Request Tracker did not perform full
rights checks on accesses to file or image type custom fields, possibly
allowing access to these custom fields by users without rights to access
to the associated objects, resulting in information disclosure.

Furthermore the following vulnerability was addressed in Debian 8.

CVE-2021-38562

     Sensitive information could have been revealed by way of a timing attack
     on the authentication system.
