---
title: "ELA-648-1 ruby-rails-html-sanitizer security update"
package: ruby-rails-html-sanitizer
version: 1.0.3-2+deb9u1 (stretch)
version_map: {"9 stretch": "1.0.3-2+deb9u1"}
description: "XSS vulnerability"
date: 2022-07-20T08:54:52+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-32209
---

A potential cross-site scripting (XSS) vulnerability was discovered in
`ruby-rails-html-sanitizer`, a library to clean (or "sanitize") HTML for
rendering within Ruby on Rails web applications.
