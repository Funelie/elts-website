---
title: "ELA-65-1 jasper security update"
package: jasper
version: 1.900.1-13+deb7u7
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2018-12-06T17:11:39+01:00
draft: false
type: updates
cvelist:
  - CVE-2015-5203
  - CVE-2015-5221
  - CVE-2016-1867
  - CVE-2016-8690
  - CVE-2017-13748
  - CVE-2017-14132
  - CVE-2018-18873
  - CVE-2018-19539
  - CVE-2018-19542

---

Multiple issues were found in the JasPer JPEG-2000 library.

CVE-2015-5203

    Gustavo Grieco discovered an integer overflow vulnerability that allows
    remote attackers to cause a denial of service or may have other unspecified
    impact via a crafted JPEG 2000 image file.

CVE-2015-5221

    Josselin Feist found a double-free vulnerability that allows remote
    attackers to cause a denial-of-service (application crash) by processing a
    malformed image file.

CVE-2016-8690

    Gustavo Grieco discovered a NULL pointer dereference vulnerability that can
    cause a denial-of-service via a crafted BMP image file. The update also
    includes the fixes for the related issues CVE-2016-8884 and CVE-2016-8885
    which complete the patch for CVE-2016-8690.

CVE-2017-13748

    It was discovered that jasper does not properly release memory used to
    store image tile data when image decoding fails which may lead to a
    denial-of-service.

CVE-2017-14132

    A heap-based buffer over-read was found related to the jas_image_ishomosamp
    function that could be triggered via a crafted image file and may cause a
    denial-of-service (application crash) or have other unspecified impact.

CVE-2018-18873

    NULL pointer dereference in the function ras_putdatastd in ras/ras_enc.c.

CVE-2018-19539 and CVE-2018-19542

    Several NULL pointer dereferences were discovered that may lead to a
    denial-of-service (application crash).
