---
title: "ELA-650-1 jetty8 security update"
package: jetty8
version: 8.1.16-4+deb8u1 (jessie)
version_map: {"8 jessie": "8.1.16-4+deb8u1"}
description: "multiple vulnerabilities"
date: 2022-07-22T21:21:46+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-10247
  - CVE-2020-27216
  - CVE-2021-28169

---

Several security vulnerabilities have been discovered in Jetty 8, a Java
webserver and servlet engine.

CVE-2019-10247

    The server running on any OS and Jetty version combination will reveal the
    configured fully qualified directory base resource location on the output
    of the 404 error for not finding a Context that matches the requested path.
    The default server behavior on jetty-distribution and jetty-home will
    include at the end of the Handler tree a DefaultHandler, which is
    responsible for reporting this 404 error, it presents the various
    configured contexts as HTML for users to click through to. This produced
    HTML includes output that contains the configured fully qualified directory
    base resource location for each context.

CVE-2020-27216

    On Unix like systems, the system's temporary directory is shared between
    all users on that system. A collocated user can observe the process of
    creating a temporary sub directory in the shared temporary directory and
    race to complete the creation of the temporary subdirectory. If the
    attacker wins the race then they will have read and write permission to the
    subdirectory used to unpack web applications, including their WEB-INF/lib
    jar files and JSP files. If any code is ever executed out of this temporary
    directory, this can lead to a local privilege escalation vulnerability.

CVE-2021-28169

    It is possible for requests to the ConcatServlet with a doubly encoded path
    to access protected resources within the WEB-INF directory. For example a
    request to `/concat?/%2557EB-INF/web.xml` can retrieve the web.xml file.
    This can reveal sensitive information regarding the implementation of a web
    application.
