---
title: "ELA-651-1 gsasl security update"
package: gsasl
version: 1.8.0-6+deb8u1 (jessie), 1.8.0-8+deb9u1 (stretch)
version_map: {"8 jessie": "1.8.0-6+deb8u1", "9 stretch": "1.8.0-8+deb9u1"}
description: "out-of-bounds read vulnerability"
date: 2022-07-23T15:00:53+01:00
draft: false
type: updates
cvelist:
  - CVE-2022-2469
---

Prevent a potential read-out-of-bounds vulnerability was discovered in gsasl, a
library for performing SASL authentication. The attack could have been
performed by a malicious (authenticated) GSS-API client.
