---
title: "ELA-653-1 openjdk-8 security update"
package: openjdk-8
version: 8u342-b07-1~deb8u1 (jessie), 8u342-b07-1~deb9u1 (stretch)
version_map: {"8 jessie": "8u342-b07-1~deb8u1", "9 stretch": "8u342-b07-1~deb9u1"}
description: "multiple vulnerabilities"
date: 2022-07-26T14:52:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-21540
  - CVE-2022-21541
  - CVE-2022-34169

---

Several vulnerabilities have been discovered in the OpenJDK Java
runtime, which may result in information disclosure, unauthorized access
or code execution.
