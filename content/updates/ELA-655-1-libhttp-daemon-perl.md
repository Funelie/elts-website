---
title: "ELA-655-1 libhttp-daemon-perl security update"
package: libhttp-daemon-perl
version: 6.01-1+deb8u1 (jessie), 6.01-1+deb9u1 (stretch)
version_map: {"8 jessie": "6.01-1+deb8u1", "9 stretch": "6.01-1+deb9u1"}
description: "bad Content-Length: handling"
date: 2022-08-01T11:25:42+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-31081

---

An issue has been found in libhttp-daemon-perl, a simple http server class.
Due to insufficient Content-Length: handling in HTTP-header an attacker could gain privileged access to APIs or poison intermediate caches. 
