---
title: "ELA-656-1 libxml2 security update"
package: libxml2
version: 2.9.4+dfsg1-2.2+deb9u8 (stretch)
version_map: {"9 stretch": "2.9.4+dfsg1-2.2+deb9u8"}
description: "cross-site scripting vulnerability"
date: 2022-08-03T08:25:21-07:00
draft: false
type: updates
cvelist:
  - CVE-2016-3709
---

A cross-site scripting vulnerability was discovered in libxml2, a widely used
XML parsing library.
