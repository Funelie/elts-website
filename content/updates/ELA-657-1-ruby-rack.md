---
title: "ELA-657-1 ruby-rack security update"
package: ruby-rack
version: 1.6.4-4+deb9u3 (stretch)
version_map: {"9 stretch": "1.6.4-4+deb9u3"}
description: "multiple vulnerabilities"
date: 2022-08-03T10:57:34-07:00
draft: false
type: updates
cvelist:
  - CVE-2022-30122
  - CVE-2022-30123
---

Two vulnerabilities were discovered in `ruby-rack`, a popular Ruby webserver:

* CVE-2022-30122: Prevent a Denial of Service (DoS) vulnerability in the HTTP
  multipart parsing.

* CVE-2022-30123: Prevent a potential shell escape sequence injection
  vulnerability that could be triggered through the logging system.
