---
title: "ELA-658-1 libxslt security update"
package: libxslt
version: 1.1.28-2+deb8u7 (jessie), 1.1.29-2.1+deb9u3 (stretch)
version_map: {"8 jessie": "1.1.28-2+deb8u7", "9 stretch": "1.1.29-2.1+deb9u3"}
description: "multiple vulnerabilities"
date: 2022-08-05T08:21:49-07:00
draft: false
type: updates
cvelist:
  - CVE-2019-5815
  - CVE-2021-30560
---

Two vulnerabilities were discovered in libxslt, an XML processing library:

* CVE-2019-5815: Type confusion in xsltNumberFormatGetMultipleLevel prior to
  libxslt 1.1.33 could allow attackers to potentially exploit heap corruption
  via crafted XML data.

* CVE-2021-30560: Use after free in Blink XSLT in Google Chrome prior to
  91.0.4472.164 allowed a remote attacker to potentially exploit heap
  corruption via a crafted HTML page.
