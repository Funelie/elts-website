---
title: "ELA-66-1 samba security update"
package: samba
version: 2:3.6.6-6+deb7u18
distribution: "Debian 7 Wheezy"
description: "denial of service"
date: 2018-12-15T10:54:09+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-16851

---
Garming Sam of the Samba Team and Catalyst discovered a NULL pointer
dereference vulnerability in the Samba AD DC LDAP server allowing a
user able to read more than 256MB of LDAP entries to crash the Samba
AD DC's LDAP server.
