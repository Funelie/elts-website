---
title: "ELA-663-1 libdatetime-timezone-perl new timezone database"
package: libdatetime-timezone-perl
version: 1:1.75-2+2022c (jessie), 1:2.09-1+2022c (stretch)
version_map: {"8 jessie": "1:1.75-2+2022c", "9 stretch": "1:2.09-1+2022c"}
description: "update to tzdata 2022c"
date: 2022-08-19T11:41:21+02:00
draft: false
type: updates
cvelist:

---

This update includes the changes in tzdata 2022c for the
Perl bindings.
