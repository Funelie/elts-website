---
title: "ELA-665-1 vlc security update"
package: vlc
version: 3.0.17.4-0+deb9u1 (stretch)
version_map: {"9 stretch": "3.0.17.4-0+deb9u1"}
description: "several vulnerabilities"
date: 2022-08-21T12:12:27+02:00
draft: false
type: updates
cvelist:

---

Multiple vulnerabilities were discovered in the VLC media player, which
could result in the execution of arbitrary code or denial of service if
a malformed file is opened.
