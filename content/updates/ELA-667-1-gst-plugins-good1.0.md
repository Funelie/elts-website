---
title: "ELA-667-1 gst-plugins-good1.0 security update"
package: gst-plugins-good1.0
version: 1.4.4-2+deb8u5 (jessie), 1.10.4-1+deb9u2 (stretch)
version_map: {"8 jessie": "1.4.4-2+deb8u5", "9 stretch": "1.10.4-1+deb9u2"}
description: "denial of service or execution of code"
date: 2022-08-27T00:52:59+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-1920
  - CVE-2022-1921
  - CVE-2022-1922
  - CVE-2022-1923
  - CVE-2022-1924
  - CVE-2022-1925
  - CVE-2022-2122

---

Adam Doupe discovered multiple vulnerabilities in package gst-plugins-good1.0,
which contains Gstreamer plugins from the "good" set.
The issues are within the plugins to demux Mastroska and AVI files, which could
result in denial of service or the execution of arbitrary code.
