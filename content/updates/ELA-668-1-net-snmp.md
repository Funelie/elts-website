---
title: "ELA-668-1 net-snmp security update"
package: net-snmp
version: 5.7.2.1+dfsg-1+deb8u5 (jessie), 5.7.3+dfsg-1.7+deb9u4 (stretch)
version_map: {"8 jessie": "5.7.2.1+dfsg-1+deb8u5", "9 stretch": "5.7.3+dfsg-1.7+deb9u4"}
description: "denial of service or code execution"
date: 2022-08-30T16:18:45+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-24805
  - CVE-2022-24806
  - CVE-2022-24807
  - CVE-2022-24808
  - CVE-2022-24809
  - CVE-2022-24810

---

Yu Zhang and Nanyu Zhong discovered several vulnerabilities in net-snmp,
a suite of Simple Network Management Protocol applications, which could
result in denial of service or the execution of arbitrary code
