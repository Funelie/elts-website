---
title: "ELA-67-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u17
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2018-12-17T02:34:31Z
draft: false
type: updates
cvelist:
  - CVE-2018-19518
  - CVE-2018-19935

---

Vulnerabilities have been discovered in php5, a server-side,
HTML-embedded scripting language.  Note that this update includes a
change to the default behavior for IMAP connections.  See below for
details.

CVE-2018-19518

    An argument injection vulnerability in imap_open() may allow a
    remote attacker to execute arbitrary OS commands on the IMAP server.

    The fix to the CVE-2018-19518 vulnerability included this
    additional note from the upstream developers:

    Starting with 5.6.38, rsh/ssh logins are disabled by default. Use
    imap.enable_insecure_rsh if you want to enable them. Note that the
    IMAP library does not filter mailbox names before passing them to
    rsh/ssh command, thus passing untrusted data to this function with
    rsh/ssh enabled is insecure.

CVE-2018-19935

    A NULL pointer dereference leads to an application crash and a
    denial of service via an empty string in the message argument to the
    imap_mail function of ext/imap/php_imap.c.
