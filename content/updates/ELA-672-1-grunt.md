---
title: "ELA-672-1 grunt security update"
package: grunt
version: 1.0.1-5+deb9u2 (stretch)
version_map: {"9 stretch": "1.0.1-5+deb9u2"}
description: "path traversal"
date: 2022-09-04T02:41:12+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-0436

---

Grunt is a JavaScript task runner, a tool used to automatically perform
frequent tasks such as minification, compilation, unit testing, and linting.
In GruntJS, file.copy operations in GruntJS are not protected against
symlink traversal for both source and destination directories.
