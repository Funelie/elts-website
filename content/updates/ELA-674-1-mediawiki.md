---
title: "ELA-674-1 mediawiki security update"
package: mediawiki
version: 1:1.27.7-1+deb9u12 (stretch)
version_map: {"9 stretch": "1:1.27.7-1+deb9u12"}
description: "cross-site-scripting"
date: 2022-09-05T22:00:02+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-28201
  - CVE-2022-28202
  - CVE-2022-34911
  - CVE-2022-34912

---

Several security vulnerabilities were discovered in mediawiki, a website engine
for collaborative work. Insufficiently escaped input text may allow a malicious
user to perform cross-site-scripting (XSS) attacks.
