---
title: "ELA-675-1 systemd security update"
package: systemd
version: 232-25+deb9u15 (stretch)
version_map: {"9 stretch": "232-25+deb9u15"}
description: "use-after-free"
date: 2022-09-06T17:25:56+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-2526

---

A use-after-free vulnerability was found in systemd, a system and service
manager. This issue occurs due to the on_stream_io() function and
dns_stream_complete() function in 'resolved-dns-stream.c' not incrementing the
reference counting for the DnsStream object. Therefore, other functions and
callbacks called can dereference the DNSStream object, causing the
use-after-free when the reference is still used later.

