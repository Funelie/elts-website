---
title: "ELA-678-1 sqlite3 security update"
package: sqlite3
version: 3.8.7.1-1+deb8u8 (jessie), 3.16.2-5+deb9u4 (stretch)
version_map: {"8 jessie": "3.8.7.1-1+deb8u8", "9 stretch": "3.16.2-5+deb9u4"}
description: "null pointer dereference vulnerability"
date: 2022-09-15T08:48:59+01:00
draft: false
type: updates
cvelist:
  - CVE-2020-35525
---


A potential null pointer dereference vulnerability was discovered in the
popular embedded database engine SQLite related to INTERSEC query processing.
