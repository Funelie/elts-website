---
title: "ELA-679-1 glib2.0 security update"
package: glib2.0
version: 2.42.1-1+deb8u5 (jessie), 2.50.3-2+deb9u4 (stretch)
version_map: {"8 jessie": "2.42.1-1+deb8u5", "9 stretch": "2.50.3-2+deb9u4"}
description: "information disclosure"
date: 2022-09-15T17:23:24+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-3800

---

It was found that GLib, a general-purpose portable utility library,
could be used to print partial contents from arbitrary files. This
could be exploited from setuid binaries linking to GLib for information
disclosure of files with a specific format.
