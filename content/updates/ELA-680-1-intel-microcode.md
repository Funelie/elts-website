---
title: "ELA-680-1 intel-microcode security update"
package: intel-microcode
version: 3.20220510.1~deb8u1 (jessie), 3.20220510.1~deb9u1 (stretch)
version_map: {"8 jessie": "3.20220510.1~deb8u1", "9 stretch": "3.20220510.1~deb9u1"}
description: "multiple vulnerabilities"
date: 2022-09-19T12:13:26+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-0127
  - CVE-2021-0145
  - CVE-2021-33117
  - CVE-2021-33120
  - CVE-2022-21123
  - CVE-2022-21125
  - CVE-2022-21127
  - CVE-2022-21151
  - CVE-2022-21166

---

This update ships updated CPU microcode for some types of Intel CPUs
and provides mitigations for security vulnerabilities which could
result in information disclosure or denial or service.
