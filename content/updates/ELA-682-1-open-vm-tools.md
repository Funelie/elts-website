---
title: "ELA-682-1 open-vm-tools security update"
package: open-vm-tools
version: 2:10.1.5-5055683-4+deb9u3 (stretch)
version_map: {"9 stretch": "2:10.1.5-5055683-4+deb9u3"}
description: "privilege escalation vulnerability"
date: 2022-09-21T23:47:28+05:30
draft: false
type: updates
cvelist:
  - CVE-2022-31676

---

A vulnerability was discovered in open-vm-tools, an open source
implementation of VMware Tools, allowing an unprivileged local
guest user to escalate their privileges as root user in the virtual
machine.
