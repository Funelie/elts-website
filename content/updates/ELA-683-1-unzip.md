---
title: "ELA-683-1 unzip security update"
package: unzip
version: 6.0-16+deb8u7 (jessie), 6.0-21+deb9u3 (stretch)
version_map: {"8 jessie": "6.0-16+deb8u7", "9 stretch": "6.0-21+deb9u3"}
description: "arbitrary code execution"
date: 2022-09-22T18:55:50+02:00
draft: false
type: updates
cvelist:
  - CVE-2022-0529
  - CVE-2022-0530

---

Sandipan Roy discovered two vulnerabilities in InfoZIP's unzip program,
a de-archiver for .zip files, which could result in denial of service
or potentially the execution of arbitrary code.
