---
title: "ELA-685-1 ntfs-3g security update"
package: ntfs-3g
version: 1:2014.2.15AR.2-1+deb8u6 (jessie)
version_map: {"8 jessie": "1:2014.2.15AR.2-1+deb8u6"}
description: "multiple vulnerabilities"
date: 2022-09-24T00:36:07+02:00
draft: false
type: updates
cvelist:
  - CVE-2021-46790
  - CVE-2022-30783
  - CVE-2022-30784
  - CVE-2022-30785
  - CVE-2022-30786
  - CVE-2022-30787
  - CVE-2022-30788
  - CVE-2022-30789

---

Several vulnerabilities were discovered in NTFS-3G, a read-write NTFS driver for FUSE. A local user can take advantage of these flaws for local root privilege escalation.

CVE-2022-30783

    An invalid return code in fuse_kern_mount enables intercepting of libfuse-lite protocol traffic between NTFS-3G and the kernel when using libfuse-lite.

CVE-2022-30784

    A crafted NTFS image can cause heap exhaustion in ntfs_get_attribute_value.

CVE-2022-30785

    A file handle created in fuse_lib_opendir, and later used in fuse_lib_readdir, enables arbitrary memory read and write operations when using libfuse-lite.

CVE-2022-30786

    A crafted NTFS image can cause a heap-based buffer overflow in ntfs_names_full_collate.

CVE-2022-30787

    An integer underflow in fuse_lib_readdir enables arbitrary memory read operations when using libfuse-lite.

CVE-2022-30788

    A crafted NTFS image can cause a heap-based buffer overflow in ntfs_mft_rec_alloc.

CVE-2022-30789

    A crafted NTFS image can cause a heap-based buffer overflow in ntfs_check_log_client_array.

CVE-2021-46790

    A crafted NTFS image can cause a heap-based buffer overflow in ntfsck.

