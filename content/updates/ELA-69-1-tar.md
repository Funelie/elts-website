---
title: "ELA-69-1 tar security update"
package: tar
version: 1.26+dfsg-0.1+deb7u2
distribution: "Debian 7 Wheezy"
description: "Denial of service vulnerability"
date: 2018-12-31T10:23:12+00:00
draft: false
type: updates
cvelist:
  - CVE-2018-20482
---

A denial of service vulnerability was discovered in tar,
the GNU version of the tar UNIX archiving utility.

The --sparse argument looped endlessly if the file shrank
whilst it was being read. Tar would only break out of this
endless loop if the file grew again to (or beyond) its
original end of file.
