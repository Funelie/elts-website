---
title: "ELA-7-1 perl security update"
package: perl
version: 5.14.2-21+deb7u7
distribution: "Debian 7 Wheezy"
description: "directory traversal flaw"
date: 2018-06-23T15:23:06Z
draft: false
type: updates
cvelist:
  - CVE-2018-12015
---

Jakub Wilk discovered a directory traversal flaw in the Archive::Tar
module, allowing an attacker to overwrite any file writable by the
extracting user via a specially crafted tar archive.
