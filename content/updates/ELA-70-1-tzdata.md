---
title: "ELA-70-1 tzdata new upstream version"
package: tzdata
version: 2018i-0+deb7u1
distribution: "Debian 7 Wheezy"
description: "New version update"
date: 2019-01-02T18:34:30+01:00
draft: false
type: updates
cvelist:

---

This update brings the timezone changes from the upstream 2018i release.
