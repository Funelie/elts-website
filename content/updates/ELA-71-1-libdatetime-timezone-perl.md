---
title: "ELA-71-1 libdatetime-timezone-perl new upstream version"
package: libdatetime-timezone-perl
version: 1:1.58-1+2018i
distribution: "Debian 7 Wheezy"
description: "New version update"
date: 2019-01-02T18:37:55+01:00
draft: false
type: updates
cvelist:

---

This update brings the Olson database changes from the 2018i version to
the Perl bindings.
