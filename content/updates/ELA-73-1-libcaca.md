---
title: "ELA-73-1 libcaca security update"
package: libcaca
version: 0.99.beta18-1+deb7u1
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2019-01-05T19:01:51+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-20544
  - CVE-2018-20546
  - CVE-2018-20547
  - CVE-2018-20549

---

Several vulnerabilities were discovered in libcaca, a graphics library that
outputs text: integer overflows, floating point exceptions or invalid memory
reads may lead to a denial-of-service (application crash) if a malformed image
file is processed.
