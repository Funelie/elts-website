---
title: "ELA-77-1 krb5 security update"
package: krb5
version: 1.10.1+dfsg-5+deb7u10
distribution: "Debian 7 Wheezy"
description: "fix for LDAP issues and other authorization flaws"
date: 2019-01-25T19:57:51+01:00
draft: false
type: updates
cvelist:
  - CVE-2015-2694
  - CVE-2018-5729
  - CVE-2018-5730
  - CVE-2018-20217

---

krb5, a MIT Kerberos implementation had several flaws in LDAP DN checking, which could be used to circumvent a DN containership check by supplying special parameters to some calls. Further an attacker could crash the KDC by making S4U2Self requests.
