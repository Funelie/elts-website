---
title: "ELA-78-1 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u14
distribution: "Debian 7 Wheezy"
description: "issues in several dissectors"
date: 2019-01-28T22:54:45+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-5716
  - CVE-2019-5717
  - CVE-2019-5719

---

Several issues in wireshark, a network traffic analyzer, have been found.
Dissectors of
  - ISAKMP, a Internet Security Association and Key Management Protocol
  - P_MUL, a reliable multicast transfer protocol
  - 6LoWPAN, IPv6 over Low power Wireless Personal Area Network
are affected.

CVE-2019-5719
   Mateusz Jurczyk found that a missing encryption block in a packet could crash the ISAKMP dissector.

CVE-2019-5717
  It was found that the P_MUL dissector could crash when a malformed packet contains an illegal Data PDU sequence number of 0.  Such a packet may not be analysed.

CVE-2019-5716
  It was found that the 6LoWPAN dissector could crash when a malformed packet does not contain IPHC information though the header says it should.

