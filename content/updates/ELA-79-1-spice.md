---
title: "ELA-79-1 spice security update"
package: spice
version: 0.11.0-1+deb7u6
distribution: "Debian 7 Wheezy"
description: "out of bounds read"
date: 2019-01-30T17:30:19+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-3813

---

Christophe Fergeau of RedHat found an off-by-one error in spice, a
SPICE protocol client and server library, that leads to an out of
bounds read, which can be exploited by a malicious client to cause
denial of service or arbitrary code execution.
