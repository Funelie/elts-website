---
title: "ELA-8-1 ruby-passenger security update"
package: ruby-passenger
version: 3.0.13debian-1+deb7u3
distribution: "Debian 7 Wheezy"
description: "CHOWN race vulnerability"
date: 2018-06-25T18:53:40+02:00
draft: false
type: updates
cvelist:
  - CVE-2018-12029
---
 A vulnerability was discovered by the Pulse Security team.
 It was exploitable only when running a non-standard
 passenger_instance_registry_dir, via a race condition where after a file
 was created, there was a window in which it could be replaced with a
 symlink before it was chowned via the path and not the file descriptor.
 If the symlink target was to a file which would be executed by root such as
 root's crontab file, then privilege escalation was possible.
 This is now mitigated by using fchown().

