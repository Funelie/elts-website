---
title: "ELA-80-1 libsndfile security update"
package: libsndfile
version: 1.0.25-9.1+deb7u5
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-01-31T16:41:07+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-14245
  - CVE-2017-14246
  - CVE-2017-14634
  - CVE-2017-17456
  - CVE-2017-17457
  - CVE-2018-13139
  - CVE-2018-19661
  - CVE-2018-19662
  - CVE-2018-19758

---

Several vulnerabilities were found in libsndfile, a library for
reading and writing files containing sampled sound, that could
cause denial of service or other unspecified impact via crafted
input files.
