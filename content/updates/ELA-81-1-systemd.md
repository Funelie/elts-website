---
title: "ELA-81-1 systemd security update"
package: systemd
version: 44-11+deb7u6
distribution: "Debian 7 Wheezy"
description: "privilege escalation and denial-of-service"
date: 2019-02-01T00:56:00+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-1049
  - CVE-2018-15686

---

CVE-2018-15686

    Jann Horn of Google discovered a vulnerability in unit_deserialize of
    systemd that allows a local attacker to supply arbitrary state across
    systemd re-execution via NotifyAccess. This can be used to improperly
    influence systemd execution and possibly lead to root privilege escalation.

CVE-2018-1049

    In systemd exists a race condition between .mount and .automount units such
    that automount requests from kernel may not be serviced by systemd
    resulting in kernel holding the mountpoint and any processes that try to
    use said mount will hang. A race condition like this may lead to denial of
    service, until mount points are unmounted.
