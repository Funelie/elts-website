---
title: "ELA-84-1 gnutls26 security update"
package: gnutls26
version: 2.12.20-8+deb7u6
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2019-02-25T20:02:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-7869
  - CVE-2017-5335
  - CVE-2017-5336
  - CVE-2017-5337

---

    GNUTLS-SA-2017-2: CVE-2017-5335, CVE-2017-5336, CVE-2017-5337

    It was found that decoding a specially crafted OpenPGP certificate could
    lead to heap and stack overflows. This may cause a denial-of-service
    (out-of-memory error and crash) or lead to other unspecified impact by
    remote attackers. This affects only applications which utilize the OpenPGP
    certificate functionality of GnuTLS.

    CVE-2017-7869

    It was found that decoding a specially crafted OpenPGP certificate could
    lead to (A) an integer overflow, resulting in an invalid memory write, (B)
    a null pointer dereference resulting in a server crash, and (C) a large
    allocation, resulting in a server out-of-memory condition. These affect
    only applications which utilize the OpenPGP certificate functionality of
    GnuTLS.


