---
title: "ELA-85-1 elfutils security update"
package: elfutils
version: 0.152-1+wheezy2
distribution: "Debian 7 Wheezy"
description: "several issues found by fuzzing or with sanatizers"
date: 2019-02-25T20:24:02+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-7608
  - CVE-2017-7610
  - CVE-2017-7611
  - CVE-2017-7612
  - CVE-2017-7613
  - CVE-2018-16062
  - CVE-2018-18310
  - CVE-2018-18520
  - CVE-2018-18521
  - CVE-2019-7149
  - CVE-2019-7150
  - CVE-2019-7665

---

Several issues in elfutils, a collection of utilities to handle ELF objects, have been found either by fuzzing or by using an AddressSanitizer.

CVE-2019-7665
     Due to a heap-buffer-overflow problem in function elf32_xlatetom()
     a crafted ELF input can cause segmentation faults.

CVE-2019-7150
     Add sanity check for partial core file dynamic data read.

CVE-2019-7149
     Due to a heap-buffer-overflow problem in function read_srclines()
     a crafted ELF input can cause segmentation faults.

CVE-2018-18521
     By using a crafted ELF file, containing a zero sh_entsize, a
     divide-by-zero vulnerability could allow remote attackers to
     cause a denial of service (application crash).

CVE-2018-18520
     By fuzzing an Invalid Address Deference problem in function elf_end
     has been found.

CVE-2018-18310
     By fuzzing an Invalid Address Read problem in eu-stack has been found.

CVE-2018-16062
     By using an AddressSanitizer a heap-buffer-overflow has been found.

CVE-2017-7613
     By using fuzzing it was found that an allocation failure was not
     handled properly.

CVE-2017-7612
     By using a crafted ELF file, containing an invalid sh_entsize, a
     remote attackers could cause a denial of service (application crash).

CVE-2017-7611
     By using a crafted ELF file a remote attackers could cause a denial
     of service (application crash).

CVE-2017-7610
     By using a crafted ELF file a remote attackers could cause a denial
     of service (application crash).

CVE-2017-7608
     By fuzzing a heap based buffer overflow has been detected.
