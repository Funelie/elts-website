---
title: "ELA-86-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u19
distribution: "Debian 7 Wheezy"
description: "prohibit access to illegal memory"
date: 2019-02-27T22:15:21+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-20783
  - CVE-2018-1000888
  - CVE-2019-9022

---

Several issues in php5 have been fixed to avoid access to illegal memory. 

CVE-2019-9022:
     An issue during parsing of DNS responses allows a hostile DNS server
     to misuse memcpy, which leads to a read operation past an allocated
     buffer.

CVE-2018-1000888:
     Fix for a PHP object injection vulnerability in the PEAR Archive_tar
     code, potentially allowing a remote attacker to execute arbitrary code.

CVE-2018-20783:
     buffer over-read in PHAR reading functions may give an attacker access
     to memory past the actual data when trying to parse a .phar file
