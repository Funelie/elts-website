---
title: "ELA-87-1 bind9 security update"
package: bind9
version: 1:9.8.4.dfsg.P1-6+nmu2+deb7u22
distribution: "Debian 7 Wheezy"
description: "fix issues in zone transfer and key handling"
date: 2019-02-28T20:30:36+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-5745
  - CVE-2019-6465

---

Two issues have been found in bind9, the Internet Domain Name Server.

CVE-2019-6465: Zone transfer for DLZs are executed though not permitted by ACLs.

CVE-2018-5745: Avoid assertion and thus causing named to deliberately exit when a trust anchor's key is replaced with a key which uses an unsupported algorithm.
