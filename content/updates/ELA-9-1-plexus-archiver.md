---
title: "ELA-9-1 plexus-archiver security update"
package: plexus-archiver
version: 1.0~alpha12-3+deb7u1
distribution: "Debian 7 Wheezy"
description: "Arbitrary file write vulnerability"
date: 2018-06-26T19:10:29+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-1002200
---

An arbitrary file write vulnerability was discovered in plexus-archiver,
the archiver plugin for the Plexus modular compiler system.

A specially-crafted `.zip` file could overwrite any file on disk, leading
to a privilege esclation.
