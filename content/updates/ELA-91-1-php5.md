---
title: "ELA-91-1 php5 security update"
package: php5
version: 5.4.45-0+deb7u20
distribution: "Debian 7 Wheezy"
description: "multiple vulnerabilities"
date: 2019-03-10T04:05:35Z
draft: false
type: updates
cvelist:
  - CVE-2019-9637
  - CVE-2019-9638
  - CVE-2019-9639
  - CVE-2019-9640
  - CVE-2019-9641

---

Vulnerabilities have been discovered in php5, a server-side,
HTML-embedded scripting language.  Note that this update includes a
change to the default behavior for IMAP connections.  See below for
details.

CVE-2019-9637

    rename() across the device may allow unwanted access during processing

CVE-2019-9638 CVE-2019-9639

    Uninitialized read in exif_process_IFD_in_MAKERNOTE

CVE-2019-9640

    Invalid Read on exif_process_SOFn

CVE-2019-9641

    Uninitialized read in exif_process_IFD_in_TIFF
