---
title: "ELA-92-1 xmltooling security update"
package: xmltooling
version: 1.4.2-5+deb7u4
distribution: "Debian 7 Wheezy"
description: "denial-of-service"
date: 2019-03-13T13:22:46+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-9628

---

Ross Geerlings discovered that the XMLTooling library did not correctly handle
exceptions for malformed XML declarations, which could result in denial of
service against the application using XMLTooling.
