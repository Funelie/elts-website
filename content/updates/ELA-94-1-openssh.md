---
title: "ELA-94-1 openssh security update"
package: openssh
version: 1:6.0p1-4+deb7u11
distribution: "Debian 7 Wheezy"
description: "scp vulnerabilities"
date: 2019-03-20T14:33:33+01:00
draft: false
type: updates
cvelist:
  - CVE-2018-20685
  - CVE-2019-6109
  - CVE-2019-6111

---

Harry Sintonen from F-Secure Corporation discovered multiple vulnerabilities in
OpenSSH, an implementation of the SSH protocol suite. All the vulnerabilities
are found in the scp client implementing the SCP protocol.

    CVE-2018-20685

    Due to improper directory name validation, the scp client allows servers to
    modify permissions of the target directory by using empty or dot directory
    name.

    CVE-2019-6109

    Due to missing character encoding in the progress display, the object name
    can be used to manipulate the client output, for example to employ ANSI
    codes to hide additional files being transferred.

    CVE-2019-6111

    Due to scp client insufficient input validation in path names sent by
    the server, a malicious server can do arbitrary file overwrites in target
    directory. If the recursive (-r) option is provided, the server can also
    manipulate subdirectories as well.

    The check added in this version can lead to a regression if the client and
    the server behave differently in wildcard expansion rules. If the server is
    trusted for that purpose, the check can be disabled with a new -T option to
    the scp client.
