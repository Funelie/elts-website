---
title: "ELA-97-1 wireshark security update"
package: wireshark
version: 1.12.1+g01b65bf-4+deb8u6~deb7u15
distribution: "Debian 7 Wheezy"
description: "several vulnerabilities"
date: 2019-03-25T20:27:41+01:00
draft: false
type: updates
cvelist:
  - CVE-2017-9344
  - CVE-2017-9349
  - CVE-2019-9209

---

Several vulnerabilities have been found in wireshark, a network traffic analyzer.

CVE-2019-9209:
     Preventing the crash of the ASN.1 BER and related dissectors by
     avoiding a buffer overflow associated with excessive digits in
     time values.

CVE-2017-9349:
     Fixing an infinite loop in the DICOM dissector by validationg
     a length value.

CVE-2017-9344:
     Avoid a divide by zero, by validating an interval value in the
     Bluetooth L2CAP dissector.

