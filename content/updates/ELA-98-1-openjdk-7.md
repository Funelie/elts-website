---
title: "ELA-98-1 openjdk-7 security update"
package: openjdk-7
version: 7u211-2.6.17-1~deb7u1
distribution: "Debian 7 Wheezy"
description: "sandbox bypass"
date: 2019-03-27T10:44:53+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-2422

---

A memory disclosure vulnerability was discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in information
disclosure or bypass of sandbox restrictions.
