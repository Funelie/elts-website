---
title: "ELA-99-1 libssh2 security update"
package: libssh2
version: 1.4.2-1.1+deb7u3
distribution: "Debian 7 Wheezy"
description: "several issues"
date: 2019-03-27T20:26:32+01:00
draft: false
type: updates
cvelist:
  - CVE-2019-3855
  - CVE-2019-3856
  - CVE-2019-3857
  - CVE-2019-3858
  - CVE-2019-3859
  - CVE-2019-3860
  - CVE-2019-3861
  - CVE-2019-3862
  - CVE-2019-3863

---

Several vulnerabilities have recently been discovered in libssh2, a
client-side C library implementing the SSH2 protocol 

CVE-2019-3855:
    An integer overflow flaw which could have lead to an out of bounds
    write was discovered in libssh2 in the way packets were read from the
    server. A remote attacker who compromised an SSH server could have
    been able to execute code on the client system when a user connected
    to the server.

CVE-2019-3856:
    An integer overflow flaw, which could have lead to an out of bounds
    write, was discovered in libssh2 in the way keyboard prompt requests
    were parsed. A remote attacker who compromised an SSH server could have
    been able to execute code on the client system when a user connected
    to the server.

CVE-2019-3857:
    An integer overflow flaw which could have lead to an out of bounds
    write was discovered in libssh2 in the way SSH_MSG_CHANNEL_REQUEST
    packets with an exit signal were parsed. A remote attacker who
    compromises an SSH server could have been able to execute code on the
    client system when a user connected to the server.

CVE-2019-3858:
    An out of bounds read flaw was discovered in libssh2 when a specially
    crafted SFTP packet was received from the server. A remote attacker
    who compromised an SSH server could have been able to cause a Denial
    of Service or read data in the client memory.

CVE-2019-3859:
    An out of bounds read flaw was discovered in libssh2's
    _libssh2_packet_require and _libssh2_packet_requirev functions. A
    remote attacker who compromised an SSH server could have be able to
    cause a Denial of Service or read data in the client memory.

CVE-2019-3860:
    An out of bounds read flaw was discovered in libssh2 in the way SFTP
    packets with empty payloads were parsed. A remote attacker who
    compromised an SSH server could have be able to cause a Denial of
    Service or read data in the client memory.

CVE-2019-3861:
    An out of bounds read flaw was discovered in libssh2 in the way SSH
    packets with a padding length value greater than the packet length
    were parsed. A remote attacker who compromised a SSH server could
    have been able to cause a Denial of Service or read data in the
    client memory.

CVE-2019-3862:
    An out of bounds read flaw was discovered in libssh2 in the way
    SSH_MSG_CHANNEL_REQUEST packets with an exit status message and no
    payload were parsed. A remote attacker who compromised an SSH server
    could have been able to cause a Denial of Service or read data in the
    client memory.

CVE-2019-3863:
    A server could have sent multiple keyboard interactive response
    messages whose total length were greater than unsigned char max
    characters. This value was used as an index to copy memory causing
    an out of bounds memory write error.
