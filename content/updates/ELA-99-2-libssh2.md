---
title: "ELA-99-2 libssh2 regression update"
package: libssh2
version: 1.4.2-1.1+deb7u4
distribution: "Debian 7 Wheezy"
description: "regression update"
date: 2019-04-06T20:12:36+02:00
draft: false
type: updates
cvelist:
  - CVE-2019-3859

---

This regression update follows up on an upstream regression update [1] regarding CVE-2019-3859.

With the previous libssh2 package revision, it was observed that user authentication with private/public key pairs would fail under certain circumstances.


[1] https://github.com/libssh2/libssh2/pull/327
